/*
 * Copyright (c) 2019-2024 Thierry Wilmot
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


class SiteMap {

   constructor(params) {
      let planLayer = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
         attribution: 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a>',
         minZoom: 2,
         maxZoom: 18
      });

      let satelliteLayer = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
         attribution: 'Tiles &copy; <a href="https://www.esri.com"> Esri</a>',
         minZoom: 2,
         maxZoom: 18
      });

      this.map_ = L.map(params.map_id, {
         zoomControl: false,
         center: [0.0, 0.0],
         zoom: 2,
         layers: [satelliteLayer, planLayer]
      });
      if(params.zIndex)
         this.map_.zoomControl.setZIndex(zIndex)
      L.control.scale({ metric: true, imperial: true }).addTo(this.map_);

      let baseMaps = {
         Satellite: satelliteLayer,
         Plan: planLayer
      };
      L.control.layers(baseMaps).addTo(this.map_);
   }

   addMarker(params) {
      if (!params.data.fly_site) {
         console.warn(`Site empty for contest_id=${params.data.id} club_id=${params.data.club_id}`);
         return;
      }

      if ((!params.data.fly_site.gps) || (params.data.fly_site.gps == "") || (params.data.fly_site.gps == "0.0,0.0")) {
         if (params.data.id && params.data.club_id)
            console.warn(`GPS empty for contest_id=${params.data.id} club_id=${params.data.club_id}`);
         else if (params.data.club_id)
            console.warn(`GPS empty for club_id=${params.data.club_id}`);
         else
            console.warn("GPS empty");
         return;
      }

      let latLng = params.data.fly_site.gps.split(',');
      let isDuplicate = false;
      let duplicateMarker;

      this.#markers_.eachLayer((marker) => {
         let latLng2 = marker.getLatLng()
         if ((latLng[0] === latLng2.lat) &&
            (latLng[1] === latLng2.lng)) {
            isDuplicate = true;
            duplicateMarker = marker;
            return;
         }
      });

      if (isDuplicate && params.duplicate == "no")
         return;

      let popupText = "";
      if ((params.duplicate == "merge") && (duplicateMarker != undefined)) {
         popupText = duplicateMarker.getPopup().getContent();
      }

      if (useOpenAPI)
         popupText += `<img src="json/countries/data/${params.data.fly_site.country.toLowerCase()}.svg" style="width:16px;height:8px;margin-bottom:5px;">&nbsp;${stringTruncate(params.data.name, 25)}<br>`;
      else {
         if (params.content == "both") {
            if (!popupText.includes("<img")) {
               popupText += `<img src="json/countries/data/${params.data.fly_site.country.toLowerCase()}.svg" style="width:16px;height:8px;margin-bottom:5px;"> `;
               popupText += `<a href="club-mg.html?club_id=${params.data.fly_site.club_id}"><i class="fas fa-shield-alt"></i></a>&nbsp;${stringTruncate(params.data.name, 25)}<br>`;
            }
            popupText += `<span class="badge text-xs ${contestStatusClass(params.data.status)}">${contestStatusChar(params.data.status)}</span>&nbsp;`;
            popupText += `<span class="text-monospace">${formatCategories(params.data.categories)}</span> `;
            if (params.data.start_datetime && params.data.start_datetime.includes(" "))
               popupText += `<span><i class="far fa-calendar-alt"></i> <a href="contest.html?contest_id=${params.data.id}">${params.data.start_datetime.split(" ")[0]}</a></span><br>`;
         }
         else {
            popupText += `<img src="json/countries/data/${params.data.fly_site.country.toLowerCase()}.svg" style="width:16px;height:8px;margin-bottom:5px;">`;
            popupText += `<a href="club-mg.html?club_id=${params.data.fly_site.club_id}"><i class="fas fa-shield-alt"></i></a>&nbsp;${params.data.fly_site.club_name}<br>`;
         }
      }

      if ((params.duplicate == "merge") && (duplicateMarker != undefined)) {
         duplicateMarker.getPopup().setContent(popupText);
      } else {
         let marker = L.marker(latLng);
         marker.bindPopup(popupText);
         this.#markers_.addLayer(marker);
      }
   }

   map_;
   #markers_;
}

function addMarkerMap(params) {
   if (!params.data.fly_site) {
      console.warn(`Site empty for contest_id=${params.data.id} club_id=${params.data.club_id}`);
      return;
   }

   if ((!params.data.fly_site.gps) || (params.data.fly_site.gps == "") || (params.data.fly_site.gps == "0.0,0.0")) {
      if (params.data.id && params.data.club_id)
         console.warn(`GPS empty for contest_id=${params.data.id} club_id=${params.data.club_id}`);
      else if (params.data.club_id)
         console.warn(`GPS empty for club_id=${params.data.club_id}`);
      else
         console.warn("GPS empty");
      return;
   }

   let latLng = params.data.fly_site.gps.split(',');
   let isDuplicate = false;
   let duplicateMarker;

   markers.eachLayer((marker) => {
      let latLng2 = marker.getLatLng()
      if ((latLng[0] === latLng2.lat) &&
         (latLng[1] === latLng2.lng)) {
         isDuplicate = true;
         duplicateMarker = marker;
         return;
      }
   });

   if (isDuplicate && params.duplicate == 'no')
      return;

   var popupText = '';
   if ((params.duplicate == 'merge') && (duplicateMarker != undefined)) {
      popupText = duplicateMarker.getPopup().getContent();
   }

   if (useOpenAPI)
      popupText += '<img src="json/countries/data/' + params.data.fly_site.country.toLowerCase() + '.svg" style="width:16px;height:8px;margin-bottom:5px;">&nbsp;' + stringTruncate(params.data.name, 25) + '<br>';
   else {
      if (params.content == 'both') {
         if (!popupText.includes('<img')) {
            popupText += '<img src="json/countries/data/' + params.data.fly_site.country.toLowerCase() + '.svg" style="width:16px;height:8px;margin-bottom:5px;"> ';
            popupText += '<a href="club-mg.html?club_id=' + params.data.fly_site.club_id + '"><i class="fas fa-shield-alt"></i></a>&nbsp;' + stringTruncate(params.data.name, 25) + '<br>';
         }
         popupText += `<span class="badge text-xs ${contestStatusClass(params.data.status)}">${contestStatusChar(params.data.status)}</span>&nbsp;`;
         popupText += '<span class="text-monospace">' + formatCategories(params.data.categories) + '</span> ';
         if (params.data.start_datetime && params.data.start_datetime.includes(" "))
            popupText += '<span><i class="far fa-calendar-alt"></i> <a href="contest.html?contest_id=' + params.data.id + '">' + params.data.start_datetime.split(" ")[0] + '</a></span><br>';
      }
      else {
         popupText += '<img src="json/countries/data/' + params.data.fly_site.country.toLowerCase() + '.svg" style="width:16px;height:8px;margin-bottom:5px;">';
         popupText += '<a href="club-mg.html?club_id=' + params.data.fly_site.club_id + '"><i class="fas fa-shield-alt"></i></a>&nbsp;' + params.data.fly_site.club_name + '<br>';
      }
   }

   if ((params.duplicate == 'merge') && (duplicateMarker != undefined)) {
      duplicateMarker.getPopup().setContent(popupText);
   } else {
      let marker = L.marker(latLng);
      marker.bindPopup(popupText);
      markers.addLayer(marker);
   }
}

function createMap() {

   if (mymap != undefined)
      return;

   let planLayer = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
      attribution: 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a>',
      minZoom: 2,
      maxZoom: 18
   });

   let satelliteLayer = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      attribution: 'Tiles &copy; <a href="https://www.esri.com"> Esri</a>',
      minZoom: 2,
      maxZoom: 18
   });

   mymap = L.map('mapid', {
      zoomControl: false,
      center: [0.0, 0.0],
      zoom: 2,
      layers: [satelliteLayer, planLayer]
   });
   //mymap.zoomControl.setZIndex(500)

   let baseMaps = {
      Satellite: satelliteLayer,
      Plan: planLayer
   };

   L.control.scale({ metric: true, imperial: true }).addTo(mymap);
   L.control.layers(baseMaps).addTo(mymap);
}
