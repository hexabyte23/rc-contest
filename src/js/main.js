/*
RC-Contest (c) 2019-2024 Thierry Wilmot - All rights reserved
*/

/*
$(document).ready(function () {
   init();
});*/

Array.prototype.insert = function (index, item) {
   this.splice(index, 0, item);
};

async function handleErrors(response) {
   if (response.ok == false) {
      let text = await response.text();
      let errorObj = new Error(text);
      errorObj.type = response.status;
      errorObj.url = response.url;
      try {
         if (text.startsWith("{"))
            errorObj.json = JSON.parse(text);
      } catch (error) {
         console.error(error);
      }

      throw errorObj;
   }

   const contentType = response.headers.get("Content-Type");
   if (contentType && contentType.indexOf("application/json") !== -1)
      return response.json();
   else
      return response;
}

function displayErrorMsg(error) {
   if (error.type == 401) {
      goAuth("");
   }
   else {
      $('#errTitle').html(`{{ lang en="Operation failed" fr="Échec de l'opération" de="Operation fehlgeschlagen" es="Operación fallida"}}`);
      if (error.json != undefined)
         $('#errMsg').html(error.json.msg);
      else
         $('#errMsg').html(error.message);
      $('#errStackMsg').html(error.url);

      $('#errorModal').modal({ backdrop: 'static', show: true });
   }
}

function displayModalMsg(params) {
   if (params.large_modal)
      $("#msgModalDlg").addClass("modal-lg");
   else
      $("#msgModalDlg").removeClass("modal-lg");
   $("#msgModalLabel").attr("class", "modal-title");
   $("#msgModalOkButton").attr("class", "btn");
   if (params.ok_no_dismiss === undefined || params.ok_no_dismiss === false)
      $("#msgModalOkButton").attr("data-dismiss", "modal");
   $("#msgModalOkButton").off("click");
   $("#msgModalOkButton").prop("disabled", false);
   $("#msgModalOkButton").html("Ok");
   $("#msgModalCancelButton").off("click");
   $("#msgModalBody").removeClass("bg-gradient-danger");
   $("#msgModalBody").removeClass("text-gray-100");
   if (params.title == undefined) {
      $("#msgModalTitle").prop("hidden", true);
      $("#msgModalFooter").prop("hidden", true);
   } else {
      $("#msgModalTitle").prop("hidden", false);
      $("#msgModalFooter").prop("hidden", false);
   }

   if (params.scrollable)
      $("#msgModalDlg").addClass("modal-dialog-scrollable");
   else
      $("#msgModalDlg").removeClass("modal-dialog-scrollable");

   if (params.type == "info") {
      $("#msgModalLabel").addClass("title-info");
      $("#msgModalOkButton").addClass("btn-info");
      if (params.title) {
         let title = `<i class="fa-solid fa-circle-info" style="font-size: 1.5em;vertical-align:middle;"></i> <span>${params.title}</span>`;
         $("#msgModalLabel").html(title);
      }
   }
   else if (params.type == "error") {
      $("#msgModalLabel").addClass("title-error");
      $("#msgModalOkButton").addClass("btn-danger");
      if (params.title) {
         let title = `<i class="fas fa-exclamation-circle" style="font-size: 1.5em;vertical-align:middle;"></i> <span>${params.title}</span>`;
         $("#msgModalLabel").html(title);
      }
   }
   else if (params.type == "success") {
      $("#msgModalLabel").addClass("title-success");
      $("#msgModalOkButton").addClass("btn-success");
      if (params.title) {
         let title = `<i class="fas fa-exclamation-circle" style="font-size: 1.5em;vertical-align:middle;"></i> <span>${params.title}</span>`;
         $("#msgModalLabel").html(title);
      }
   }
   else if (params.type == "success-question") {
      $("#msgModalLabel").addClass("title-success");
      $("#msgModalOkButton").addClass("btn-success");
      if (params.title) {
         let title = `<i class="fas fa-question-circle" style="font-size: 1.5em;vertical-align:middle;"></i> <span>${params.title}</span>`;
         $("#msgModalLabel").html(title);
      }
   }
   else if (params.type == "error-question") {
      $("#msgModalLabel").addClass("title-error");
      $("#msgModalOkButton").addClass("btn-danger");
      if (params.title) {
         let title = `<i class="fas fa-question-circle" style="font-size: 1.5em;vertical-align:middle;"></i> <span>${params.title}</span>`;
         $("#msgModalLabel").html(title);
      }
   }
   else if (params.type == "critical-question") {
      $("#msgModalLabel").addClass("title-error");
      $("#msgModalOkButton").addClass("btn-danger");
      $("#msgModalBody").addClass("bg-gradient-danger");
      $("#msgModalBody").addClass("text-gray-100");
      if (params.title) {
         let title = `<i class="fas fa-question-circle" style="font-size: 1.5em;vertical-align:middle;"></i> <span>${params.title}</span>`;
         $("#msgModalLabel").html(title);
      }
   } else {
      $("#msgModalLabel").addClass("info-question");
      $("#msgModalOkButton").addClass("btn-primary");
      if (params.title) {
         let title = `<i class="far fa-question-circle" style="font-size: 1.5em;vertical-align:middle;"></i> <span>${params.title}</span>`;
         $("#msgModalLabel").html(title);
      }
   }
   if (params.message) {
      $("#msgModalBody").html(`<span id="msgModalMsg"></span>`);
      $("#msgModalMsg").html(params.message);
   }
   else if (params.body) {
      $("#msgModalBody").html(params.body);
      $("#msgModalMsg").html("");
   }

   let modalElt = $("#msgModal").modal({ backdrop: "static", show: true });
   if (params.post_display_callback) {
      try {
         params.post_display_callback(modalElt, params.cb_params);
      } catch (error) {
         displayErrorMsg(error);
      }
   }
   if (params.showCancel) {
      $("#msgModalCancelButton").prop("hidden", false);
      if (params.cancel_callback) {
         $("#msgModalCancelButton").click(function (event) {
            try {
               params.cancel_callback(event, params.cb_params);
            } catch (error) {
               displayErrorMsg(error);
            }
            // we remove click handler for the next modal usage
            $("#msgModalCancelButton").off("click");
         });
      }
   } else
      $("#msgModalCancelButton").prop("hidden", true);

   if (params.hideOK == true)
      $("#msgModalOkButton").prop("hidden", true);
   else
      $("#msgModalOkButton").prop("hidden", false);

   if (params.ok_label)
      $("#msgModalOkButton").html(params.ok_label);

   $("#msgModalOkButton").click(function (event) {
      if (params.reload)
         window.location.reload();
      if (params.ok_callback) {
         try {
            params.ok_callback(event, params.cb_params);
         } catch (error) {
            displayErrorMsg(error);
         }
         // we remove click handler for the next modal usage
         $("#msgModalOkButton").off("click");
      }
   });

   return modalElt;
}

function displayNotYetImpl() {
   displayModalMsg({
      type: "error",
      title: `{{ lang en="For future use" fr="Pour une utilisation future" de="Für die künftige Verwendung" es="For future use" }}`,
      message: `{{ lang 
      en="This function is not yet implemented" 
      fr="Cette fonction n'est pas encore developpée" 
      de="Diese Funktion ist noch nicht implementiert" 
      es="Esta función aún no está implementada" }}`,
   });
}

function displayAlert(params) {
   let alertList = (params.containerElt) ? params.containerElt : document.getElementById("alertList");
   if (!alertList)
      return;

   let icon;
   let alertElt = document.createElement("div");

   if (params.type == "success") {
      alertElt.className = "alert-success";
      icon = params.icon ? params.icon : '<i class="fas fa-thumbs-up"></i>&nbsp;';
   } else if (params.type == "warning") {
      alertElt.className = "alert-warning";
      icon = '<i class="fas fa-exclamation-triangle"></i>&nbsp;';
   } else {
      alertElt.className = "alert-danger";
      icon = '<i class="fa-solid fa-bomb"></i>&nbsp;';
   }

   alertElt.style = "font-size:1.2rem;";
   alertElt.style.visibility = "visible";
   alertElt.classList.add("alert");
   alertElt.classList.add("fade");
   alertElt.classList.add("show");
   alertElt.classList.add("row");

   let spanElt = document.createElement("span");
   spanElt.innerHTML = `${icon} ${params.msg}`;
   alertElt.appendChild(spanElt);
   alertList.appendChild(alertElt);

   if (params.autoHide) {
      const timeout = params.timeout | 3000;
      setTimeout(function (alertElt) {
         if (alertElt.style.visibility == "visible") {
            $(alertElt).fadeOut("slow");
            //alertList.removeChild(alertElt);
         }

         if (params.callbackOnHide != undefined)
            params.callbackOnHide();
      }, timeout, alertElt, alertList);
   }

   return alertElt;
}

function encodeHTML(str) {
   return document.createElement("a").appendChild(document.createTextNode(str)).parentNode.innerHTML;
}

function decodeHTML(str) {
   let element = document.createElement("a");
   element.innerHTML = str;
   return element.textContent;
}

function removeChilds(parent) {
   while (parent.lastChild)
      parent.removeChild(parent.lastChild);
}

function fillupMessages(msg) {
   $("#msgCount").text(msg.length);
   if (msg.length > 0) {
      let content = "";
      let count = 0;
      content += '<h6 class="dropdown-header">{{ lang en="Notification Center" fr="Centre de notification" de="Nachrichtencenter" es="Centro de notificaciones"}}</h6>';
      msg.forEach(function (item) {
         if (count < 5) {
            if (item.url.length === 0)
               content += '<a class="dropdown-item d-flex align-items-center" href="#">';
            else
               content += '<a class="dropdown-item d-flex align-items-center" href="' + item.url + '">';
            content += '  <div class="dropdown-list-image mr-3">';
            content += '    <img class="rounded-circle" src="/image/avatar/av' + item.from_user_avatar_id + '.png" alt="">';
            if (item.from_user_login_count > 0)
               content += '    <div class="status-indicator bg-success"></div>';
            content += '  </div>';
            if (item.status == "new")
               content += '  <div class="font-weight-bold">';
            else
               content += '  <div>';
            content += '    <div class="text-wrap">' + item.msg + '</div>';
            content += '    <div class="small text-gray-500">' + item.from_user_login + '</div>';
            content += '  </div>';
            content += '</a>';
            count++;
         }
      });

      if (count >= 5)
         content += '<a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>';
      $('#msgDD').html(content);
   }
}

function getLocaleDetail(localeStr) {
   let ret = [];
   let sep = "-";

   if (!localeStr.includes("-")) {
      if (!localeStr.includes("_"))
         return ret;
      else
         sep = "_";
   }

   ret = localeStr.split(sep);
   ret.insert(1, sep);

   return ret;
}

async function getGlobalInformations() {
   let ret = {};
   await fetch('/api/v1/op/getGlobalInformations')
      .then(handleErrors)
      .then(function (result) {
         ret = result;
      });
   return ret;
}

function init() {
   if (getUserPrefsValue('sidebarToggle', true) == false)
      $("#sidebarToggle").click();
   $("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
      setUserPrefsValue('sidebarToggle', $(".sidebar").hasClass("toggled"));
   });

   window.onerror = function (message, source, lineno, colno, error) {
      //console.error(message, source, lineno, colno, error);
      displayModalMsg({
         type: "error",
         title: "{{ lang en='Error' fr='Erreur' de='Error' es='Error' }}",
         message: message + source
      });
   };
}

var currentUser = { role: "pi" };
async function getSessionUserParams(params) {
   //{{ if_master }}
   let countInfo = await getGlobalInformations();
   $("#clubs_count").text(countInfo.clubs_count);
   $("#contests_count").text(countInfo.contests_count);
   //{{ endif }}

   let paramsAPI = "";
   if (params) {
      if (params.contestID != undefined)
         paramsAPI = `?contest_id=${params.contestID}`;
      else if (params.clubID != undefined)
         paramsAPI = `?club_id=${params.clubID}`;
   }

   return await fetch(`/api/v1/getSessionUserParams${paramsAPI}`)
      .then(handleErrors)
      .then(function (result) {
         currentUser = result;

         if (!currentUser.modules)
            currentUser.modules = [];

         $("#userName").text(currentUser.login);
         $("#userAvatar").prop("src", `/image/avatar/av${currentUser.avatar_id}.png`);
         //$("#nav-db").prop("href", "index2.html");

         /*
         if (currentUser.is_site_administrator == true)
            $(".card-header").css("backgroundColor", "#e7edff");
         else if (currentUser.is_site_manager == true)
            $(".card-header").css("backgroundColor", "#e7edff");
         else if (currentUser.is_club_manager == true)
            $(".card-header").css("backgroundColor", "#d0e9fb");
         else if (currentUser.is_country_manager == true)
            $(".card-header").css("backgroundColor", "#e6ffe3");
         else if (currentUser.is_contest_director == true)
            $(".card-header").css("backgroundColor", "#fae4ab");
         */

         if (params) {
            if (params.contestID != undefined) {
               if (currentUser.contest_role.includes("ph"))
                  phoneIsAllow(params.contestID);

               if (currentUser.role.includes("cd"))
                  contestAdminIsAllow(params.contestID);
            }
         }

         //{{ if_master }}
         if (currentUser.is_contest_director) {
            myContestIsAllow();
         }
         //{{ endif }}


         fillupMessages(result.msg);

         return currentUser;
      });
}

async function getLocalServerInfo() {
   return await fetch("/api/v1/op/getLocalServerInfo")
      .then(handleErrors);
}

async function updateLocalServerInfo(data) {
   return await fetch("/api/v1/updateLocalServerInfo", {
      headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(data)
   })
      .then(handleErrors);
}

async function setDefaultSiteSpeaker(name, gender) {
   return await fetch(`/api/v1/setDefaultSiteSpeaker?name=${name}&gender=${gender}`)
      .then(handleErrors);
}

async function getDevicePortList() {
   return await fetch("/api/v1/op/getDevicePortList")
      .then(handleErrors);
}

async function getDisplayProtocolList() {
   return await fetch("/api/v1/op/getDisplayProtocolList")
      .then(handleErrors);
}

async function getExternalDeviceList() {
   return await fetch("/api/v1/getExternalDeviceList")
      .then(handleErrors);
}

async function setExternalDeviceList(deviceList) {
   return await fetch("/api/v1/setExternalDeviceList?device_list=" + JSON.stringify(deviceList))
      .then(handleErrors);
}

async function testExternalDisplay(startStop, curRound, curGroup, maxRound, maxGroup) {
   return await fetch(`/api/v1/testExternalDisplay?ss=${startStop}&cur_round=${curRound}&cur_group=${curGroup}&max_round=${encodeHTML(maxRound)}&max_group=${encodeHTML(maxGroup)}`)
      .then(handleErrors);
}

async function initializeContest(contest_id) {
   return await fetch(`/api/v1/initializeContest?contest_id=${contest_id}`)
      .then(handleErrors);
}

async function getContestParameters(contest_id) {
   return await fetch(`/api/v1/getContestParameters?contest_id=${contest_id}`)
      .then(handleErrors);
}

async function computeScores(contest_id, isFlyoff, isJunior, pilotID) {
   return await fetch(`/api/v1/computeScores?contest_id=${contest_id}&is_flyoff=${isFlyoff}&is_junior=${isJunior}&pilot_id=${pilotID}`)
      .then(handleErrors);
}

async function modifyContestParameters(contest_id, data) {
   return await fetch(`/api/v1/modifyContestParameters?contest_id=${contest_id}`, {
      headers: {
         "Accept": "application/json",
         "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(data)
   })
      .then(handleErrors);
}

async function modifyContest(contestID, data, callback) {
   await fetch(`/api/v1/modifyContest?contest_id=${contestID}`, {
      headers: {
         "Accept": "application/json",
         "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(data)
   })
      .then(handleErrors)
      .then(function () {
         if (callback)
            callback();
      })
      .catch(function (error) {
         displayErrorMsg(error);
      });
}


async function modifyUserLanguage(lang) {
   await fetch(`/api/v1/modifyUserLanguage?lang=${lang}`)
      .then(handleErrors)
      .then(function (result) { })
      .catch(function (error) {
         displayErrorMsg(error);
      });
}

async function removeContest(contestID) {
   return await fetch(`/api/v1/removeContest?contest_id=${contestID}`)
      .then(handleErrors);
}

async function getOrganizationList() {
   return await fetch("/api/v1/op/getOrganizationList")
      .then(handleErrors);
}

async function getCategoryList(scope) {
   return await fetch(`/api/v1/op/getCategoryList?scope=${scope}`)
      .then(handleErrors);
}

async function getActiveContestParameters(contestID) {
   return await fetch(`/api/v1/getActiveContestParameters?contest_id=${contestID}`)
      .then(handleErrors);
}

async function getContest(contestID) {
   return await fetch(`/api/v1/getContest?contest_id=${contestID}`)
      .then(handleErrors);
}

async function getContestCategoryList() {
   return await fetch("/api/v1/op/getCategoryList?scope=contest")
      .then(handleErrors);
}

async function getContestSpeakerLocaleList(contestID) {
   return await fetch(`/api/v1/op/getContestSpeakerLocaleList?contest_id=${contestID}`)
      .then(handleErrors);
}

async function getContestList() {
   return await fetch("/api/v1/getContestList")
      .then(handleErrors);
}

async function isContestRegistrationStopped(contestID) {
   return await fetch(`/api/v1/isContestRegistrationStopped?contest_id=${contestID}`)
      .then(handleErrors);
}

async function closeContestRegistration(contestID) {
   await fetch(`/api/v1/closeContestRegistration?contest_id=${contestID}`)
      .then(handleErrors)
      .then(function () {
      })
      .catch(function (error) {
         displayErrorMsg(error);
      });
}

async function updateLocalServer(version) {
   return await fetch(`/api/v1/updateLocalServer?version=${version}`)
      .then(handleErrors);
}

function askCloseRegistration(contestID, extraCB) {
   displayModalMsg({
      type: "error-question",
      title: `{{ lang en="Closing date for registration ?" 
         fr="Clôture de l'inscription ?" 
         de="Abschluss der Anmeldung?" 
         es="¿Fecha límite de inscripción?" }}`,
      message: `{{ lang en="Closing the competition registration?"
         fr="Clôturer l’inscription au concours ?"
         de="Die Anmeldung zum Auswahlverfahren abschließen?"
         es="¿Cierre de la inscripción al concurso?"
         }}`,
      ok_callback: async function () {
         await closeContestRegistration(contestID);
         if (extraCB)
            extraCB();
      },
      showCancel: true
   });
}

function replacer(key, value) {
   if (value instanceof Map)
      return { dataType: "Map", value: Array.from(value.entries()) };
   else
      return value;
}

function reviver(key, value) {
   if (typeof value === 'object' && value !== null) {
      if (value.dataType === 'Map')
         return new Map(value.value);
   }
   return value;
}

function getUserPrefs() {
   return JSON.parse(localStorage.getItem("rcs"), reviver);
}

function setUserPrefs(value) {
   localStorage.setItem("rcs", JSON.stringify(value, replacer));
}

function deleteUserPrefs() {
   localStorage.removeItem("rcs");
   localStorage.removeItem("club-table.bs.table.pageList");
   localStorage.removeItem("wich-list-contest-table.bs.table.sortName");
   localStorage.removeItem("club-contest-table.bs.table.columns");
   localStorage.removeItem("contest-pilot-table.bs.table.columns");
   localStorage.removeItem("club-members-table.bs.table.columns");
   localStorage.removeItem("wich-list-contest-table.bs.table.sortOrder");
   localStorage.removeItem("club-table.bs.table.pageNumber");
   localStorage.removeItem("wich-list-contest-table.bs.table.searchText");
   localStorage.removeItem("wich-list-contest-table.bs.table.pageNumber");
   localStorage.removeItem("club-table.bs.table.searchText");
   localStorage.removeItem("club-members-table.bs.table.searchText");
   localStorage.removeItem("club-members-table.bs.table.pageNumber");
   localStorage.removeItem("club-contest-table.bs.table.searchText");
   localStorage.removeItem("club-contest-table.bs.table.pageNumber");
   localStorage.removeItem("club-access-table.bs.table.searchText");
   localStorage.removeItem("club-access-table.bs.table.pageNumber");
}

function setUserPrefsValue(key, value) {
   let conf = getUserPrefs();
   if (conf == undefined)
      conf = {};
   conf[key] = value;
   setUserPrefs(conf);
}

function getUserPrefsValue(key, opt) {
   let conf = getUserPrefs();
   if (!conf) {
      if (opt) {
         setUserPrefsValue(key, opt);
         return opt;
      }
      else
         return "";
   }

   // looking for
   for (const elt in conf) {
      if (elt == key)
         return conf[key];
   }

   // not found
   if (opt) {
      setUserPrefsValue(key, opt);
      return opt;
   }
   else
      return "";
}

function phoneNotAllowMsg() {
   $('#infoModal').modal();
   if (window.location.pathname == "/contest.html") {
      $('#infoMsg').html(
         [
            'Ask to contest director to allow you !'
         ].join(''));
   }
   else {
      $('#infoMsg').html(
         [
            'Choose a contest from dashboard page first<br>',
            'Then contest director will have to allow you !'
         ].join(''));
   }
   return true;
}

function phoneIsAllow(contestID) {
   let pn = document.getElementById("phone-nav-link");
   if (pn) {
      pn.removeAttribute("hidden");

      $("#phone-nav-link").prop("href", `/module/f5j/phone.html?contest_id=${contestID}`);
      $("#phone-nav-link").prop("onclick", null).off('click');
   }
}

function phoneIsNotAllow() {
   document.getElementById("phone-nav-link").setAttribute("hidden", "true");
   $("#phone-nav-link").prop("href", "/module/default/phone.html");
   //$("#phone-nav-link").prop("onclick", null).off("click");
}

function myContestIsAllow() {
   document.getElementById("my-contests-nav-link").removeAttribute("hidden");
}

function contestAdminIsAllow(contestID) {
   document.getElementById("contest-admin-nav-link").removeAttribute("hidden");
   $("#contest-admin-nav-link").prop("href", "contest-mg.html?contest_id=" + contestID);
   $("#contest-admin-nav-link").prop("onclick", null).off("click");
}

function contestAdminIsNotAllow() {
   document.getElementById('contest-admin-nav-link').setAttribute("hidden", "true");
   $('#contest-admin-nav-link').prop("href", "#");
   //$('#phone-nav-link').prop("onclick", null).off("click");
}

function createNotAllowMsg() {
   $('#infoModal').modal();
   $('#infoMsg').html(
      [
         'For create contest, 3 cases are possible:',
         '<ul><li>First of your country <i class="fas fa-arrow-circle-right"></i> Site admin must allow you first !</li>',
         '<li>First of your club <i class="fas fa-arrow-circle-right"></i> <a href="club.html">Country admin</a> must allow you first !</li>',
         '<li>Already organized contests <i class="fas fa-arrow-circle-right"></i> Ask to your club manager </li></ul>'
      ].join(''));
   return true;
}

function canDisplayContest(contest, user) {
   if ((user.role == 'ad') || (user.role == 'sm'))
      return true;
   else if (user.role == 'rv')
      return false;
   else
      return (contest.status != 'X');
}

function canAccesToContest(contest, user) {
   if ((user.role == 'ad') || (user.role == 'sm'))
      return true;
   else if (user.role == 'rv')
      return false;
   else
      return (contest.status != 'X');
}

function initLogin() {
   if (localStorage.getItem("rcs") != undefined) {
      $("#inputEmail").val(getUserPrefsValue("login"));
      $("#customCheck").prop('checked', getUserPrefsValue("rm"));
      $("#inputPassword").focus();
   } else {
      $("#inputEmail").focus();
   }
}

function onLogin(targetURL) {
   var login = $("#inputEmail").val();
   var password = $("#inputPassword").val();
   var hash = sha3_512(login + password);
   //console.log(hash);

   fetch(`/api/v1/login?hash=${hash}&login=${encodeURIComponent(login)}`)
      .then(handleErrors)
      .then(function (/*response*/) {
         //    if(response.status == 200) {
         if ($("#customCheck").prop("checked")) {
            setUserPrefsValue("login", $("#inputEmail").val());
            setUserPrefsValue("rm", $("#customCheck").prop("checked"));
         }
         $("#loginModal").modal("hide");
         if (targetURL == "")
            window.location.reload();
         else
            window.location.href = targetURL;
         /*    }
             else
               throw new RCSServerError(response.status, response.statusText);*/
      })
      .catch(function (error) {
         displayAlert({
            type: "error",
            msg: error.message,
            containerElt: document.getElementById("alertListLoginFail"),
            divElt: document.getElementById("loginFadeMsg"),
            spanElt: document.getElementById("loginMsg"),
            autoHide: true,
            timeout: 4000
         });
      });
}

function onLogout() {
   fetch("/api/v1/logout")
      .then(function (/*response*/) {
         $("#logoutModal").modal("hide");
         window.location.href = "/login.html";
      })
      .catch(function (error) {
         displayErrorMsg(error);
      });
}

function goAuth(url) {
   $('#loginModal').on('show.bs.modal', function () { initLogin(); });
   $('#loginModalBut').attr("onclick", `onLogin("${url}")`);
   $('#loginModal').modal({ backdrop: 'static', show: true });
}

function stringTruncate(str, length) {
   let dots = str.length > length ? '...' : '';
   return str.substring(0, length) + dots;
}

function removeAudioDuplicate(result) {
   let ret = {};
   ret.disableTimerSpeech = (result.active_speech_device == "none" || result.active_speech_device == "both" || result.active_speech_device == "server");
   ret.disableTimerSound = (result.active_sound_device == "none" || result.active_sound_device == "both" || result.active_sound_device == "server");

   return ret;
}

function jsonToCSV(data, attributes, separ = ";", removeQuote = false) {

   function replacer(key, value) {
      if (value === null)
         return "";

      if ((typeof value === "number") && (Math.abs(value % 1) > 0))
         return Number.parseFloat(value).toLocaleString();

      return value;
   }

   if (!data[0])
      return {};

   let csv = [];

   if (attributes) {
      csv = data.map(row => attributes.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(separ));
      csv.unshift(attributes.join(separ));
   }
   else {
      const header = Object.keys(data[0]);
      csv = data.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(separ));
      csv.unshift(header.join(separ));
   }

   csv = csv.join("\r\n");
   if (removeQuote)
      csv = csv.replace(/"/gi, "");

   return csv;
}

function csvToJSON(data, strict = true) {
   let result = [];
   let lines = data.split("\r\n");
   if (lines.length == 1)
      lines = data.split("\n");
   let headDelimiter;
   let lineDelimiter;

   // looking for the good delimiter
   if (lines[0].indexOf(";") != -1) {
      headDelimiter = ";";
      lineDelimiter = ";";
   }
   else if (lines[0].indexOf(",") != -1) {
      headDelimiter = ",";
      lineDelimiter = "\",\"";
   }
   else if (lines[0].indexOf("\t") != -1) {
      headDelimiter = "\t";
      lineDelimiter = "\t";
   }
   else {
      if (strict)
         return result;
      else
         return lines;
   }

   let headers = lines[0].split(headDelimiter);

   for (let i = 1; i < lines.length; i++) {
      let obj = {};
      let currentline = lines[i].split(lineDelimiter);

      if (headers.length >= currentline.length) {
         for (let j = 0; j < headers.length; j++) {
            if (currentline[j]) {
               let attrName = headers[j].trim().replace(/"/gi, "");
               let attrValue = currentline[j].replace(/"/gi, "");
               obj[attrName] = (attrValue == "null") ? null : attrValue;
            }
         }
         result.push(obj);
      }
      else
         console.error(`bad line format (${i}) ${currentline}`);
   }

   return result;
}

function uploadJsonFromCSV(callback) {
   let link = document.createElement("input");
   link.setAttribute("type", "file");
   link.addEventListener("change", callback, false);
   link.click();
}

function downloadCSV(filename, csv) {
   let link = document.createElement("a");
   link.setAttribute("href", 'data:text/csv;charset=utf-8,%EF%BB%BF' + encodeURIComponent(csv));
   link.setAttribute("download", filename);
   link.style.visibility = "hidden";
   document.body.appendChild(link);
   link.click();
   document.body.removeChild(link);
}

function downloadCSVFromJson(filename, data, attributes, separ, removeQuote, formulas = "") {
   let csv = jsonToCSV(data, attributes, separ, removeQuote);
   if (formulas != "") {
      let lines = csv.split("\r\n");
      for (const item of formulas) {
         lines[0] += `${separ}${item.field}`;
         let group = 0;
         for (let i = 1; i < lines.length; i++) {
            let matchResult = item.value.match(/\$\{(\d+)\}/);
            if (matchResult && matchResult.length > 1) {
               let str = "";
               for (let j = 1; j < matchResult.length; j++) {
                  let initialNum = parseInt(matchResult[j], 10) + i - 1;
                  if (j == 1)
                     str = item.value.replaceAll(/\$\{(\d+)\}/g, initialNum);
                  else
                     str = str.replaceAll(/\$\{(\d+)\}/g, initialNum);
               }
               lines[i] += separ + str;
            }
            else
               lines[i] += `${separ}${item.value}`;
         }
      }
      csv = lines.join("\r\n");
      csv = `sep=${separ}\r\n` + csv;
   }
   if (csv.length > 0)
      downloadCSV(filename, csv);
}

function getCurrentLocale() {
   return "{{ lang en='en-US' fr='fr-FR' de='de-DE' es='es-ES' }}";
}

function getDatetimeFormatted(now = new Date(), format = "ISO8601") {
   //let now = (date == undefined) ? new Date() : date;
   if (format == "ISO8601")
      return `${now.getFullYear()}-${formatDigit(now.getMonth() + 1)}-${formatDigit(now.getDate())}T${formatDigit(now.getHours())}:${formatDigit(now.getMinutes())}:${formatDigit(now.getSeconds())}`;
   else if (format == "sqlite")
      return `${now.getFullYear()}-${formatDigit(now.getMonth() + 1)}-${formatDigit(now.getDate())} ${formatDigit(now.getHours())}:${formatDigit(now.getMinutes())}:${formatDigit(now.getSeconds())}`;
   else if (format == "date-only")
      return now.toLocaleDateString("{{ lang en='en-US' fr='fr-FR' de='de-DE' es='es-ES' }}");
   if (format == "date-only-us")
      return `${now.getFullYear()}-${formatDigit(now.getMonth() + 1)}-${formatDigit(now.getDate())}`;
   else
      return now.toLocaleString("{{ lang en='en-US' fr='fr-FR' de='de-DE' es='es-ES' }}");
}

function formatCategories(categories) {
   let ret = "???";

   if (categories.length != 0) {
      let list = categories.split(",");
      let first = true;

      for (const cat of list) {
         if (first) {
            ret = cat;
            first = false;
         } else {
            let newCat;
            if (cat.startsWith("S"))
               newCat = cat.substring(1, 3);
            else
               newCat = cat.substring(2, 3);

            if (!ret.includes(newCat, 1))
               ret += newCat;
         }
      }

      if (list.length > 1) {
         if (ret.length > 5)
            ret = ret.substring(0, 2) + "*";
      } else if (list.length == 1) {
         if (list[0] == "Electro7")
            ret = "EL7";
         else if (list[0] == "FF2000")
            ret = "FF2";
      }
   }

   return ret;
}

function changeChronoState(value) {
   switch (value) {
      case 0:
         $("#chronoRow").css("background", "#ac4949");
         $("#chronoDisplay").css("background", "#ac4949");
         $("#chronoDisplay").css("color", "#000000");
         break;
      case 1:
         $("#chronoRow").css("background", "#aaac49");
         $("#chronoDisplay").css("background", "#aaac49");
         $("#chronoDisplay").css("color", "#000000");
         break;
      case 2:
         $("#chronoRow").css("background", "#5dac49");
         $("#chronoDisplay").css("background", "#5dac49");
         $("#chronoDisplay").css("color", "#000000");
         break;
      case 3:
         $("#chronoRow").css("background", "#000000");
         $("#chronoDisplay").css("background", "#000000");
         $("#chronoDisplay").css("color", "#abe881");
         break;
   }
}

function contestStatusChar(status) {
   const statusList = ["F", "A", "R", "C", "O", "P", "X"];
   if (statusList.includes(status))
      return status;
   else
      return "?";
}

function contestStatusText(status) {
   switch (status) {
      case 'A':
         return '&nbsp;{{ lang en="Registration open" fr="Inscription ouverte" de="Registrierung offen" es="Inscripción abierta" }}&nbsp;';
      case 'F':
         return '&nbsp;{{ lang en="Forecasting" fr="En prévision" de="Prognose" es="Pronóstico" }}&nbsp;';
      case 'R':
         return '&nbsp;{{ lang en="In progress" fr="En cours" de="In Bearbeitung" es="En progreso" }}&nbsp;';
      case 'C':
         return '&nbsp;{{ lang en="Cancel" fr="Annulé" de="Stornieren" es="Cancelar" }}&nbsp;';
      case 'O':
         return '&nbsp;{{ lang en="Complete" fr="Achevé" de="Komplett" es="Completo" }}&nbsp;';
      case 'X':
         return '&nbsp;{{ lang en="Future" fr="Future" de="Future" es="Future" }}&nbsp;';
      case 'P':
         return '&nbsp;{{ lang en="Postponed" fr="Reporté" de="Aufgeschoben" es="Aplazado" }}&nbsp;';
      default: return '?';
   }
}


function contestStatusClass(status) {
   switch (status) {
      case 'A':
         return 'event-registration-details';
      case 'F':
         return 'event-forecast-details';
      case 'R':
         return 'event-in-progress-details';
      case 'C':
         return 'event-cancel-details';
      case 'O':
         return 'event-complete-details';
      case 'P':
         return 'event-postponed-details';
      default:
         return 'event-regular-details';
   }
}

function contestTypeClass(type) {
   switch (type) {
      case 'wcs':
         return "calendar-bg-wcs";
      case 'ccs':
         return "calendar-bg-ccs";
      case 'wcc':
         return "calendar-bg-wcc";
      case 'ccc':
         return "calendar-bg-ccc";
      case 'nat':
         return "calendar-bg-nat";
      case 'loc':
         return "calendar-bg-loc";
      case 'met':
         return "calendar-bg-met";
      case 'eve':
         return "calendar-bg-eve";
      case 'tra':
         return "calendar-bg-tra";
      case 'exa':
         return "calendar-bg-exa";

   }
}

function formatDigit(v) {
   if (v < 10)
      return "0" + v;
   else
      return v;
}

function timeFormat(time, isFlyoff) {
   if ((time < 0) || (typeof time == "undefined") || (time === "") || (time === null)) {
      return (!isFlyoff ? "-:--" : "--:--");
   }
   if (time == "+")
      return time;

   let sec = (time % 60).toFixed(0);
   let min = Math.floor(time / 60);
   return `${!isFlyoff ? min : formatDigit(min)}:${formatDigit(sec)}`;
}

var colorIndex = 0;
function getNextColor() {
   let COLORS = [
      "#115f9a", "#1984c5", "#22a7f0", "#48b5c4", "#76c68f", "#a6d75b", "#c9e52f", "#d0ee11", "#d0f400",
      "#003f5c",
      "#2f4b7c",
      "#665191",
      "#a05195",
      "#d45087",
      "#f95d6a",
      "#ff7c43",
      "#ffa600",
      "#4dc9f6",
      "#f67019",
      "#f53794",
      "#537bc4",
      "#acc236",
      "#166a8f",
      "#00a950",
      "#58595b",
      "#8549ba",
   ];
   if (colorIndex == COLORS.length)
      colorIndex = 0;

   return COLORS[colorIndex++];
}

function DD2DMS(deg, lat) {
   let absolute = Math.abs(deg);
   let degrees = Math.floor(absolute);
   let minutesNotTruncated = (absolute - degrees) * 60;
   let minutes = Math.floor(minutesNotTruncated);
   let seconds = ((minutesNotTruncated - minutes) * 60).toFixed(2);
   let direction;

   if (lat)
      direction = deg >= 0 ? 'N' : 'S';
   else
      direction = deg >= 0 ? 'E' : 'W';

   return `${degrees}°${minutes}'${seconds}"${direction}`;
}

function daysBetween(startDate, endDate) {
   // The number of milliseconds in all UTC days (no DST)
   const oneDay = 1000 * 60 * 60 * 24;

   // A day in UTC always lasts 24 hours (unlike in other time formats)
   const start = Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());
   const end = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());

   // so it's safe to divide by 24 hours
   return (start - end) / oneDay;
}

function createElementRCS(params) {
   let element;

   if ("tagName" in params) {
      element = document.createElement(params["tagName"]);

      if ("parent" in params)
         params["parent"].appendChild(element);

      if ("className" in params)
         element.className = params["className"];

      if ("style" in params)
         element.style = params["style"];

      if ("type" in params)
         element.type = params["type"];

      if ("checked" in params)
         element.checked = params["checked"];

      if ("onchange" in params)
         element.onchange = params["onchange"];
   }

   return element;
}

function initWebSocket(isSSL, wsPort) {
   try {
      if (typeof MozWebSocket == 'function')
         WebSocket = MozWebSocket;

      if (websocket && websocket.readyState == 1)
         return;

      let wsProtocol = (isSSL ? "wss" : "ws");

      websocket = new WebSocket(`${wsProtocol}://${window.location.hostname}:${wsPort}`);
      //websocket.onopen = function (evt)    { console.log("CONNECTED"); };
      //websocket.onclose = function (evt)   { console.log("DISCONNECTED"); };
      websocket.onmessage = onWebSocketMessage;
      websocket.onerror = function (evt) { console.error('WebSocket ERROR: ', evt.data); };
   }
   catch (exception) {
      console.error('WebSocket ERROR: ', exception);
   }
}

function displayGeneralMessage(data, isHtml) {
   if (isHtml)
      $("#genModalMsg").html(data);
   else
      $("#genModalMsg").text(data);
   $("#generalModal").modal("show");
}

function closeGeneralMessage() {
   $("#generalModal").modal("hide");
}

function isoToEmoji(code) {
   return code
      .split('')
      .map(letter => letter.charCodeAt(0) % 32 + 0x1F1E5)
      .map(emojiCode => String.fromCodePoint(emojiCode))
      .join('')
}

async function resetContestData(initSetup) {
   await fetch(`/api/v1/resetLocalContestData?contest_id=0`, {
      headers: {
         "Accept": "application/json",
         "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(initSetup)
   })
      .then(handleErrors)
      .catch(function (error) {
         displayErrorMsg(error);
      });
}

function resetLocalServerDefaultVoices(defaultLocale = "") {
   fetch(`/api/v1/resetLocalServerDefaultVoices?default_locale=${defaultLocale}`)
      .then(handleErrors)
      .then(function (response) {
         window.location.reload();
      })
      .catch(function (error) {
         displayErrorMsg(error);
      });

}


(function ($) {
   "use strict";

   /* For Progressive Web App
   if ("serviceWorker" in navigator) {
      navigator.serviceWorker.register("/serviceWorker.js");
   }*/

   var ip = document.getElementById('inputPassword');
   if (ip != null) {
      ip.addEventListener('keypress', function (event) {
         if (event.code == 'Enter') {
            if (ip.parentElement.id == "index-html")
               onLogin("/module/f5j/contest.html");
            else
               onLogin(""); // just refresh current page
         }
      });
   }

   /*==================================================================
   [ Focus input ]*/
   $('.input100').each(function () {
      $(this).on('blur', function () {
         if ($(this).val().trim() != "") {
            $(this).addClass('has-val');
         }
         else {
            $(this).removeClass('has-val');
         }
      });
   });

   /*==================================================================
   [ Validate ]*/
   var input = $('.validate-input .input100');

   $('.validate-form').on('submit', function () {
      let check = true;

      for (var i = 0; i < input.length; i++) {
         if (validate(input[i]) == false) {
            showValidate(input[i]);
            check = false;
         }
      }

      return check;
   });


   $('.validate-form .input100').each(function () {
      $(this).focus(function () {
         hideValidate(this);
      });
   });

   function validate(input) {
      if ($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
         if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
            return false;
         }
      }
      else {
         if ($(input).val().trim() == '') {
            return false;
         }
      }
   }

   function showValidate(input) {
      let thisAlert = $(input).parent();

      $(thisAlert).addClass('alert-validate');
   }

   function hideValidate(input) {
      let thisAlert = $(input).parent();

      $(thisAlert).removeClass('alert-validate');
   }


})(jQuery);
