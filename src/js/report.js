/*
RC-Contest (c) 2019-2024 Thierry Wilmot - All rights reserved
*/

const { jsPDF } = window.jspdf;

function formatNames(firstName, lastName, maxLen = 22) {
   let fl = `${firstName} ${lastName}`;
   if ((fl.length > maxLen) && (maxLen > 0))
      fl = fl.substring(0, maxLen);
   return fl;
}

function printCardRoundPilot(pdf, x, y, roundNum, groupNum, data) {
   pdf.setTextColor("#000000");
   pdf.setFont(undefined, "bold");
   pdf.setFontSize(11);
   if (roundNum % 2) {
      pdf.setFillColor("#F4F4F4");
      pdf.rect(x + 5, y + 5, 48, data.length * 5 + 2, "F");
   }

   x += 8;
   let txt1 = `{{ lang en="Round" fr="Manche" de="Ruden" es="Manga" }} ${roundNum}`;
   pdf.text(txt1, x, y + 10);

   pdf.setFont(undefined, "normal");
   let txt2 = `{{ lang en="Group" fr="Groupe" de="Gruppe" es="Grupo"}} ${groupNum}`;
   pdf.text(txt2, x + 22, y + 10);

   pdf.setFontSize(8);

   y += 12;
   let target = 1;
   for (const row of data) {
      let width = (target - 1) * 4;
      if (row.is_free_type == false) {
         pdf.rect(x, y + width, 42, 4);
         pdf.text(`${(target >= 10) ? target : " " + target}  ${formatNames(row.first_name, row.last_name)}`, x + 2, y + 3 + width);
      } else {
         pdf.setFillColor("#D0D0D0");
         pdf.rect(x, y + width, 42, 4, "F");
         pdf.setFillColor("#FFFFFF");
         pdf.rect(x, y + width, 42, 4);
         pdf.text(`${(target >= 10) ? target : " " + target}  {{ lang en="Free" fr="Libre" de="Leer" es="Vacío" }}`, x + 2, y + 3 + width);
      }

      target++;
   }
}

function checkNewPage(point, height, pdf, contest, footer) {
   if ((point.y + height) > (210 - 5)) {
      pdf.addPage("a4", "l");
      point.x = 0;
      point.y = 11;

      pdf.setFontSize(14);
      let category = contest.categories[0].category.toUpperCase();
      pdf.text(`${category}   ${contest.name}`, point.x + 8, point.y);
      pdf.setFontSize(9);
      point.y += 4;
      pdf.text(`${contest.start_datetime}`, point.x + 8, point.y);
      pdf.text(footer, point.x + 8, point.y + 190);

      return true;
   }

   return false;
}

async function generateDrawMatrix(params, contest, data) {
   document.getElementById(params.spinID).style.visibility = "visible";
   document.getElementById(params.iframeID).style.visibility = "hidden";

   let pdf = new jsPDF({
      orientation: "l",
      unit: "mm",
      format: "a4",
      putOnlyUsedFonts: true,
      compress: true,
      floatPrecision: 16 // or "smart", default is 16
   });
   pdf.setFont(params.fontName);

   pdf.setDocumentProperties({
      title: "{{ lang en='Draw' fr='Tirage' de='Zeichnen' es='Sorteo'}}",
      subject: "",
      author: "Radio Control System",
      keywords: "F5J, radio control contest",
      creator: "RC-Contest.com"
   });

   let pageNum = 1;
   let footer = `www.rc-contest.com     v: ${params.serverVersion}`;

   if (data.matrix.length == 0) {
      pdf.setFontSize(30);
      pdf.text(`{{ lang en="Rounds draw is not yet saved" fr="Le tirage au sort n'est pas encore enregistré" de="Die Auslosung der Runden ist noch nicht gespeichert" es="El sorteo de Mangas aún no se ha guardado" }}`, 30, 30);
   } else {
      let point = { x: 0, y: 11 };

      // header
      let img = new Image();
      img.src = '/image/20 noir.png';
      pdf.addImage(img, 'PNG', 6, 3, 15, 15);
      pdf.setTextColor("#000000");
      pdf.setFont(undefined, "normal");
      pdf.setFontSize(14);
      let category = contest.categories[0].category.toUpperCase();

      pdf.text(`${category}   ${contest.name}`, 17 + 8, point.y);
      pdf.setFontSize(9);
      point.y += 4;
      let height = ((data.matrix[0][0].length + 1) * 4.8);
      let width = 48;
      let date = contest.start_datetime;
      pdf.text(`${date}`, 17 + 8, point.y);
      pdf.text(footer, point.x + 8, point.y + 190);

      //let interval = false;
      //let tick = document.getElementById("drawQualifProgress");
      let roundNum = 1;
      for (const round of data.matrix) {
         if (checkNewPage(point, height, pdf, contest, footer))
            pageNum++;

         let groupNum = 1;
         for (const group of round) {
            printCardRoundPilot(pdf, point.x, point.y, roundNum, groupNum, group);
            point.x += width;
            if ((point.x + width) > 297) {
               point.x = 0;
               point.y += height;

               if (checkNewPage(point, height, pdf, contest, footer))
                  pageNum++;
            }
            groupNum++;
         }

         roundNum++;

         /*let value = 100.0 * (roundNum - 1) / pilotMatrix.length;

         // eslint-disable-next-line no-inner-declarations
         function updateTicker(value) {
            tick.innerHTML = value + "%";
            tick.style.width = value + "%";
         }
         interval = window.setTimeout(() => updateTicker(value.toFixed(1)), 100);
         console.log(value);*/
      }
      //window.clearInterval(interval);
   }

   pdf.setFont(undefined, "normal");
   pdf.setFontSize(11);
   for (let p = 1; p <= pageNum; p++) {
      pdf.setPage(p);
      pdf.text(`${p}/${pageNum}`, 297 - 15, 205);
   }

   if (params.filename !== "")
      pdf.save(params.filename);

   if (params.iframeID !== "") {
      let iframe = document.getElementById(params.iframeID);
      if (iframe)
         iframe.src = pdf.output("datauristring", { filename: "draw-qualif.pdf" });
   }

   document.getElementById(params.iframeID).style.visibility = "visible";
   document.getElementById(params.spinID).style.visibility = "hidden";

   return pdf;
}

function printPilotCard(params, pdf, img, x, y, data, url) {
   pdf.setTextColor("#000000");

   pdf.setFont(undefined, "bold");
   pdf.setFontSize(11);

   pdf.setFillColor("#F5F5F5");
   pdf.setLineDashPattern([1, 3], 0);
   pdf.line(x, y + 210 / 2, x + 297 / 4, y + 210 / 2);   // cut lines
   pdf.line(x + 297 / 4, y, x + 297 / 4, y + 210 / 2);   // cut lines

   pdf.setLineDashPattern();
   pdf.rect(x + 5, y + 41, 37, 13, "F");     // fly time
   pdf.rect(x + 5, y + 48 + 6, 37, 18, "F"); // Cible
   pdf.rect(x + 5, y + 48 + 24, 37, 6, "F"); // Altitude
   pdf.rect(x + 5, y + 48 + 30, 37, 6, "F"); // penalité
   pdf.rect(x + 5, y + 41, 64, 59);          // general frame horizontal
   pdf.rect(x + 42, y + 41, 27, 43);         // general frame vertical
   pdf.rect(x + 5, y + 41, 37, 13);          // fly time
   pdf.rect(x + 42, y + 41 + 7, 27, 6);      // < 11 min    > 11 min
   pdf.line(x + 55, y + 48, x + 55, y + 48 + 6);

   pdf.rect(x + 42, y + 48 + 6, 27, 6);      // jusqu'a 10 m
   pdf.rect(x + 42, y + 48 + 12, 27, 6);     // au dela 10 m
   pdf.rect(x + 42, y + 48 + 18, 27, 6);     // au dela 10 m
   pdf.rect(x + 5, y + 48 + 24, 37, 6);      // Altitude
   pdf.rect(x + 42, y + 48 + 24, 27, 6);     // altitude
   pdf.rect(x + 5, y + 48 + 36, 32, 16);     // signature pilote
   pdf.rect(x + 37, y + 48 + 36, 32, 16);    // signature juge
   //pdf.setDrawColor("#FF0000");
   //pdf.setDrawColor("#000000");

   pdf.setFillColor("#A0A0A0");
   pdf.rect(x + 5, y + 6, 64, 6, (data) ? "F" : "S");
   pdf.text("{{ lang en='Round\nGroup\nSpot' fr='Manche\nGroupe\nCible' de='Runden\nGruppe\nStelle' es='Manga\nGrupo\nLugar'}}", x + 37, y + 18);
   pdf.setFillColor("#FFFFFF");

   pdf.setTextColor("#FFFFFF");
   if (data) {
      let str = formatNames(data.first_name, data.last_name);
      let marginX = (64 - str.length) / 2.5;
      pdf.text(x + marginX, y + 10, str);

      pdf.setTextColor("#000000");
      pdf.text(`${data.round_id + 1}\n${data.group_id + 1}\n${data.slot_id + 1}`, x + 69, y + 18, "right");
   }
   else
      pdf.setTextColor("#000000");

   if (img)
      pdf.addImage(img, "PNG", x + 5, y + 15, 20, 20);

   pdf.text("{{ lang en='Fly time' fr='Temps vol' de='Flugzeit' es='Tiempo vuelo'}}", x + 6, y + 45);
   pdf.text("{{ lang en='Spot' fr='Cible' de='Stelle' es='Lugar'}}", x + 6, y + 53 + 5);
   pdf.text("{{ lang en='Start height' fr='Altitude' de='Starthöhe' es='Altura'}}", x + 6, y + 52 + 24);
   pdf.text("{{ lang en='Penalty' fr='Pénalité' de='Elfer' es='Penaliz.'}}", x + 6, y + 53 + 29);

   if (url) {
      pdf.setFont(undefined, "normal");
      pdf.setFontSize(5);
      pdf.text(url, x + 5, y + 37, { maxWidth: 65 });
   }

   pdf.setFont(undefined, "normal");
   pdf.setFontSize(8);
   if (data && data.is_flyoff)
      pdf.text("HT < 16 min  |  HT > 16 min", x + 41, y + 51, "right");
   else
      pdf.text("HT < 11 min  |  HT > 11 min", x + 41, y + 51, "right");
   pdf.text(`{{ lang en="Until 10 meters" fr="Jusqu'à 10 m" de="bis 10 Meter" es="hasta 10 metros" }}`, x + 40, y + 58, "right");
   pdf.text("{{ lang en='Btw 10,1 m and 74,9 m' fr='Entre 10,1 m et 74,9 m' de='Btw 10,1 m und 74,9 m' es='Entre 10,1 m y 74,9 m' }}", x + 40, y + 64, "right");
   pdf.text("{{ lang en='75 m and beyond' fr='A 75 m et au delà' de='75 m und mehr' es='75 m y más allá'}}", x + 40, y + 70, "right");
   pdf.text("{{ lang en='Pilot                              Time keeper' fr='Pilote                             Chronométreur' de='Pilot                                  Zeithalter' es='Piloto                            Cronometrador' }}", x + 17, y + 54 + 44);
   //pdf.text("{{ lang en='Motor cut-off' fr='Coupure moteur' de='Motorabschaltung' es='Corte del motor' }}", x + 21, y + 52 + 24);
   pdf.text("{{ lang en='Motor restarted' fr='Remise moteur' de='Motor gestartet' es='Motor reiniciado' }}", x + 22, y + 52 + 30);
   pdf.text("www.rc-contest.com", x + 2, y + 72, -90);

   pdf.setTextColor("#909090");
   pdf.text("MM:SS", x + 51, y + 45);
   pdf.text(`{{ lang en="To check   To check" fr="A cocher   A cocher" de="Cheken   Checken" es="   Tildar      Tildar" }}`, x + 43, y + 52);
   if (params.distanceUnit == "m")
      pdf.text("{{ lang en='In meter' fr='En mètre' de='Im Meter' es='En metros'}}", x + 51, y + 53 + 5);
   else if (params.distanceUnit == "cm")
      pdf.text("{{ lang en='In centimeter' fr='En centimètre' de='In Zentimer' es='En centimero'}}", x + 47, y + 53 + 5);
   else
      pdf.text("{{ lang en='In point' fr='En point' de='In Punkt' es='En punto'}}", x + 51, y + 53 + 5);
   pdf.text(`{{ lang en="To check" fr="A cocher" de="Cheken" es="Tildar" }}`, x + 51, y + 53 + 11);
   pdf.text(`{{ lang en="To check" fr="A cocher" de="Cheken" es="Tildar" }}`, x + 51, y + 53 + 17);
   pdf.text("{{ lang en='In meter' fr='En mètre' de='Im Meter' es='En metros'}}", x + 51, y + 53 + 23);
   pdf.text(`{{ lang en="To check" fr="A cocher" de="Cheken" es="Tildar" }}`, x + 51, y + 53 + 29);
   pdf.text("{{ lang en='Signature' fr='Signature' de='Unterschrift' es='Firma'}}", x + 15, y + 54 + 38);
   pdf.text("{{ lang en='Signature' fr='Signature' de='Unterschrift' es='Firma'}}", x + 45, y + 54 + 38);
}

async function generateLeaflet(params, scores, entrantList) {
   document.getElementById(params.spinID).style.visibility = "visible";
   document.getElementById(params.iframeID).style.visibility = "hidden";

   params.distanceUnit = params.cache.contest.categories[0].distance_unit;
   let pdf = new jsPDF({
      orientation: "l",
      unit: "mm",
      format: "a4",
      putOnlyUsedFonts: true,
      compress: true,
      floatPrecision: 16 // or "smart", default is 16
   });
   pdf.setFont(params.fontName);

   pdf.setDocumentProperties({
      title: '{{ lang en="Flight leaflet" fr="Feuillet de vol" de="Flugblatt" es="Folleto de vuelo"}}',
      subject: '',
      author: "Radio Control System",
      keywords: "F5J, radio control contest",
      creator: "RC-Contest.com"
   });

   removeChilds(document.getElementById("qrcode-for-report"));
   let qrcode = new QRCode("qrcode-for-report", {
      text: "",
      width: 128,
      height: 128,
      userSVG: true
      // colorDark : "#000000",
      // colorLight : "#ffffff",
      // correctLevel : QRCode.CorrectLevel.H
   });

   if (scores.length === 0) {
      pdf.setFontSize(30);
      pdf.text(`{{ lang en="Rounds draw is not yet saved" fr="Le tirage au sort n'est pas encore enregistré" de="Die Auslosung der Runden ist noch nicht gespeichert" es="El sorteo de Mangas aún no se ha guardado" }}`, 30, 30);
   } else {
      let y = -1;
      let isFirstpage = true;
      let index = 0;
      //let indexProgress = 0;

      if (params.uniqueEntrant == -1)
         entrantList = entrantList.sort((a, b) => a.last_name.localeCompare(b.last_name));
      else {
         const entrant = entrantList.find((entrant) => entrant.id == params.uniqueEntrant);
         entrantList = [];
         entrantList.push(entrant);
      }


      //let interval = false;
      //let tick = document.getElementById("leafQualifProgress");
      for (let entrant of entrantList) {
         let hash = String(entrant.license_list.split("=")[1]);
         let port = "";
         if (window.location.port)
            port = ":" + window.location.port;
         let url = `${window.location.protocol}//${params.serverIP}${port}/api/v1/login?login=${encodeURIComponent(entrant.login)}&hash=${hash}&first`;

         qrcode.clear();
         qrcode.makeCode(url);
         let dc = document.getElementById("qrcode-for-report");
         let canvas = dc.children[0];
         //let img = canvas.toDataURL("image/png");

         for (let score of scores) {
            //indexProgress++;
            if (((score.is_free_type == true) && (score.pilot_id == -1)) || score.pilot_id != entrant.id)
               continue;

            let x = (297.0 / 4) * (index % 4);

            if (index % 4 === 0) {
               if (y === 0) {
                  y = 210.0 / 2;
               } else {
                  y = 0;
                  if (isFirstpage)
                     isFirstpage = false;
                  else
                     pdf.addPage("a4");
               }
            }

            /*let entrant = entrantList.find(function (item) { return item.id === score.entrant_id; });
            if (entrant === undefined) {
               console.error("entrant not found");
               continue;
            }*/
            score.license_list = entrant.license_list;
            score.login = entrant.login;

            printPilotCard(params, pdf, canvas, x, y, score, url);

            index++;

            /*
            // eslint-disable-next-line no-inner-declarations
            function updateTicker(value) {
               tick.innerHTML = value.toFixed(1) + "%";
               tick.style.width = value + "%";
            }
            let value = 100 * (index - 1) / scores.length;
            interval = window.setTimeout(() => updateTicker(value), 10);*/
         }

         if (params.easyCut) {
            let remain = 8 - index % 8;
            if (remain != 0) {
               index += remain;
               y = -1;
            }
         }
      }
      //window.clearInterval(interval);

      if (params.freeCards) {
         pdf.addPage("a4");
         y = 0;

         for (let index = 0; index < 8; index++) {
            let x = (297.0 / 4) * (index % 4);
            printPilotCard(params, pdf, null, x, y, null, null);
            if (index % 4 === 0) {
               if (y === 0)
                  y = 210.0 / 2;
               else
                  y = 0;
            }
         }
      }
   }

   if (params.filename !== "")
      pdf.save(params.filename);

   if (params.iframeID !== "") {
      let iframe = document.getElementById(params.iframeID);
      if (iframe)
         iframe.src = pdf.output("datauristring", { filename: "leaflet-qualif.pdf" });
   }

   document.getElementById(params.iframeID).style.visibility = "visible";
   document.getElementById(params.spinID).style.visibility = "hidden";

   return pdf;
}

function printPilotCardCover(pdf, x, y, data, contest, scores) {
   pdf.setTextColor("#000000");

   pdf.setFont(undefined, "bold");

   pdf.setFontSize(18);
   pdf.text(formatNames(data.first_name, data.last_name), x + 5, y + 16);

   pdf.setFontSize(11);

   pdf.setFillColor("#D0D0D0");
   pdf.setLineDashPattern([1, 3], 0);
   pdf.line(x, y + 210 / 2, x + 297 / 2, y + 210 / 2);   // cut lines
   pdf.line(x + 297 / 2, y, x + 297 / 2, y + 210 / 2);   // cut lines

   pdf.setLineDashPattern();
   pdf.setDrawColor("#909090");
   pdf.line(x + 297 / 4, y + 15, x + 297 / 4, y + 210 / 2 - 10);

   // left tab
   pdf.rect(x + 5, y + 34, 66, 6, "F");
   pdf.rect(x + 5, y + 34, 66, 6);
   pdf.rect(x + 5, y + 34 + 6, 66, 6);
   pdf.rect(x + 5, y + 34 + 2 * 6, 66, 6);
   pdf.rect(x + 5, y + 34 + 3 * 6, 66, 6);
   pdf.rect(x + 5, y + 34 + 4 * 6, 66, 6);
   pdf.rect(x + 5, y + 34 + 5 * 6, 66, 6);
   pdf.rect(x + 5, y + 34 + 6 * 6, 66, 6);
   pdf.rect(x + 5, y + 34 + 7 * 6, 66, 6);
   pdf.rect(x + 5, y + 34 + 8 * 6, 66, 6);
   pdf.rect(x + 5, y + 34 + 9 * 6, 66, 6);
   pdf.rect(x + 5, y + 34 + 10 * 6, 66, 6);

   pdf.rect(x + 5, y + 34, 10, 66);
   pdf.rect(x + 5 + 10, y + 34, 8, 66);
   pdf.rect(x + 5 + 18, y + 34, 15, 66);
   pdf.rect(x + 5 + 18, y + 34, 15, 66);
   pdf.rect(x + 5 + 33, y + 34, 11, 66);
   pdf.rect(x + 5 + 44, y + 34, 11, 66);

   // right tab
   pdf.rect(x + 5 + 72, y + 46, 66, 6, "F");
   pdf.rect(x + 5 + 72, y + 46, 66, 6);
   pdf.rect(x + 5 + 72, y + 46 + 6, 66, 6);
   pdf.rect(x + 5 + 72, y + 46 + 2 * 6, 66, 6);
   pdf.rect(x + 5 + 72, y + 46 + 3 * 6, 66, 6);
   pdf.rect(x + 5 + 72, y + 46 + 4 * 6, 66, 6);
   pdf.rect(x + 5 + 72, y + 46 + 5 * 6, 66, 6);
   pdf.rect(x + 5 + 72, y + 46 + 6 * 6, 66, 6);
   pdf.rect(x + 5 + 72, y + 46 + 7 * 6, 66, 6);
   pdf.rect(x + 5 + 72, y + 46 + 8 * 6, 66, 6);

   pdf.rect(x + 5 + 72, y + 46, 10, 54);
   pdf.rect(x + 5 + 10 + 72, y + 46, 8, 54);
   pdf.rect(x + 5 + 18 + 72, y + 46, 15, 54);
   pdf.rect(x + 5 + 18 + 72, y + 46, 15, 54);
   pdf.rect(x + 5 + 33 + 72, y + 46, 11, 54);
   pdf.rect(x + 5 + 44 + 72, y + 46, 11, 54);

   pdf.setFontSize(11);
   pdf.text("Rnd   Grp  Temps   Dist.   Altit.   Pen.", x + 6, y + 38);
   //pdf.text("Rnd   Grp  Temps   Dist.   Altit.   Pen.", x + 78, y + 38);
   pdf.text("Rnd   Grp  Temps   Dist.   Altit.   Pen.", x + 78, y + 50);

   pdf.setFontSize(9);
   if (contest.categories.length == 1) {
      pdf.text(`${contest.categories[0].pen_model_contact_person}`, x + 130, y + 8);
      pdf.text(`${contest.categories[0].pen_safety_area_infringement}`, x + 130, y + 12);
      pdf.text(`${contest.categories[0].pen_model_part_in_corridor}`, x + 130, y + 16);
      pdf.text(`${contest.categories[0].pen_bad_launch_direction}`, x + 130, y + 20);
      pdf.text(`${contest.categories[0].pen_motor_run_before_signal}`, x + 130, y + 24);
      pdf.text(`${contest.categories[0].pen_launch_inside_corridor}`, x + 130, y + 28);
      pdf.text(`${contest.categories[0].pen_launch_straight_3_sec}`, x + 130, y + 32);
      pdf.text(`${contest.categories[0].pen_launch_before_signal}`, x + 130, y + 36);
      pdf.text(`${contest.categories[0].pen_outside_10m_rectangle}`, x + 130, y + 40);
      pdf.text(`${contest.categories[0].pen_bad_landing_direction}`, x + 130, y + 44);
   }

   pdf.setFont(undefined, "normal");

   pdf.text(`{{ lang en='Model hit a person' fr='Modèle touche une personne' de='Model schlägt eine Person' es='Modelo golpeó una persona'}}:`, x + 85, y + 8);
   pdf.text(`{{ lang en='Safety area infringement' fr='Violation zone de sécurité' de='Verstoß gegen den Sicherheitsbereich' es='Infracción área seguridad'}}:`, x + 85, y + 12);
   pdf.text(`{{ lang en='Parts of model in corridor' fr='Parties du modèle dans couloir' de='Teile des Modells im Korridor' es='Partes del modelo en pasillo'}}:`, x + 85, y + 16);
   pdf.text(`{{ lang en='Bad launch direction' fr='Lancement mauvaise direction' de='Schlechte Startrichtung' es='Mala dirección de lanzamiento'}}:`, x + 85, y + 20);
   pdf.text(`{{ lang en='Motor run before signal' fr='Moteur en marche avant signal' de='Motorlauf vor Signal' es='Motor marcha antes señal'}}:`, x + 85, y + 24);
   pdf.text(`{{ lang en='Launch outside corridor' fr='Lancement extérieur au couloir' de='Start außerhalb Korridors' es='Lanzamiento pasillo exterior'}}:`, x + 85, y + 28);
   pdf.text(`{{ lang en='Launch straight 3 sec' fr='Lancement ligne droite 3 sec' de='Start gerade 3 sec' es='Lanzamiento recto 3 seg'}}:`, x + 85, y + 32);
   pdf.text(`{{ lang en='Launch before signal' fr='Lancement avant le signal' de='Start vor Signal' es='Lanzamiento antes señal'}}:`, x + 85, y + 36);
   pdf.text(`{{ lang en='Move outside 10m rectangle' fr='Sortir du rectangle de 10 m' de='Bewegung außerhalb des 10m-Rechtecks' es='Desplazarse fuera rectángulo 10 m'}}:`, x + 85, y + 40);
   pdf.text(`{{ lang en='Bad landing direction' fr='Mauvaise direction atterrissage' de='Schlechte Landerichtung' es='Mala dirección de aterrizaje'}}:`, x + 85, y + 44);

   pdf.setFontSize(16);
   pdf.text("{{ lang en='Penalties' fr='Pénalités' de='Sanktionen' es='Sanciones'}}", x + 81, y + 37, { angle: 90 });

   pdf.setFontSize(11);
   pdf.text(`${contest.name.substring(0, 26)}  ${contest.start_datetime.split(" ")[0]}\n\n\n${(data.category == "s" ? "Senior" : "Junior")} ${data.tag}\n${data.license_list}\n${data.locale}`, x + 5, y + 8);

   // round
   pdf.text("1", x + 8, y + 38 + 6);
   pdf.text("2", x + 8, y + 38 + 2 * 6);
   pdf.text("3", x + 8, y + 38 + 3 * 6);
   pdf.text("4", x + 8, y + 38 + 4 * 6);
   pdf.text("5", x + 8, y + 38 + 5 * 6);
   pdf.text("6", x + 8, y + 38 + 6 * 6);
   pdf.text("7", x + 8, y + 38 + 7 * 6);
   pdf.text("8", x + 8, y + 38 + 8 * 6);
   pdf.text("9", x + 8, y + 38 + 9 * 6);
   pdf.text("10", x + 7, y + 38 + 10 * 6);

   pdf.text("11", x + 7 + 72, y + 50 + 6);
   pdf.text("12", x + 7 + 72, y + 50 + 2 * 6);
   pdf.text("13", x + 7 + 72, y + 50 + 3 * 6);
   pdf.text("14", x + 7 + 72, y + 50 + 4 * 6);
   pdf.text("15", x + 7 + 72, y + 50 + 5 * 6);
   pdf.text("16", x + 7 + 72, y + 50 + 6 * 6);
   pdf.text("17", x + 7 + 72, y + 50 + 7 * 6);
   pdf.text("18", x + 7 + 72, y + 50 + 8 * 6);

   // group
   let getGroup = function (array, roundID, pilotID) {
      let t = array.filter(function (object) {
         return ((object.pilot_id === pilotID) && (object.round_id === roundID));
      });
      if (t.length == 0)
         return "";
      return (t[0].group_id + 1).toString();
   };

   pdf.text(getGroup(scores, 0, data.id), x + 17, y + 38 + 6);
   pdf.text(getGroup(scores, 1, data.id), x + 17, y + 38 + 2 * 6);
   pdf.text(getGroup(scores, 2, data.id), x + 17, y + 38 + 3 * 6);
   pdf.text(getGroup(scores, 3, data.id), x + 17, y + 38 + 4 * 6);
   pdf.text(getGroup(scores, 4, data.id), x + 17, y + 38 + 5 * 6);
   pdf.text(getGroup(scores, 5, data.id), x + 17, y + 38 + 6 * 6);
   pdf.text(getGroup(scores, 6, data.id), x + 17, y + 38 + 7 * 6);
   pdf.text(getGroup(scores, 7, data.id), x + 17, y + 38 + 8 * 6);
   pdf.text(getGroup(scores, 8, data.id), x + 17, y + 38 + 9 * 6);
   pdf.text(getGroup(scores, 9, data.id), x + 17, y + 38 + 10 * 6);

   pdf.text(getGroup(scores, 10, data.id), x + 17 + 72, y + 50 + 6);
   pdf.text(getGroup(scores, 11, data.id), x + 17 + 72, y + 50 + 2 * 6);
   pdf.text(getGroup(scores, 12, data.id), x + 17 + 72, y + 50 + 3 * 6);
   pdf.text(getGroup(scores, 13, data.id), x + 17 + 72, y + 50 + 4 * 6);
   pdf.text(getGroup(scores, 14, data.id), x + 17 + 72, y + 50 + 5 * 6);
   pdf.text(getGroup(scores, 15, data.id), x + 17 + 72, y + 50 + 6 * 6);
   pdf.text(getGroup(scores, 16, data.id), x + 17 + 72, y + 50 + 7 * 6);
   pdf.text(getGroup(scores, 17, data.id), x + 17 + 72, y + 50 + 8 * 6);
   //pdf.text(getGroup(scores, 18, data.id), x + 17 + 72, y + 50 + 9 * 6);
   //pdf.text(getGroup(scores, 19, data.id), x + 17 + 72, y + 50 + 10 * 6);
}

function printHelperCardCover(pdf, x, y, data, contest) {
   pdf.setTextColor("#000000");

   pdf.setFont(undefined, "bold");
   pdf.setFontSize(11);

   pdf.setFillColor("#D0D0D0");
   pdf.setLineDashPattern([1, 3], 0);
   pdf.line(x, y + 210 / 2, x + 297 / 2, y + 210 / 2);   // cut lines
   pdf.line(x + 297 / 2, y, x + 297 / 2, y + 210 / 2);   // cut lines

   pdf.setLineDashPattern();
   pdf.setDrawColor("#909090");
   pdf.line(x + 297 / 4, y + 15, x + 297 / 4, y + 210 / 2 - 10);

   pdf.setFontSize(18);
   pdf.text(formatNames(data.first_name, data.last_name), x + 5, y + 16);

   pdf.setFontSize(11);
   pdf.setFont(undefined, "normal");

   let category = contest.categories[0].category.toUpperCase();
   pdf.text(`${category}   ${contest.name}     ${contest.start_datetime}\n\n\n${(data.category == "s" ? "Senior" : "Junior")} ${data.tag}\n${data.license_list}\n${data.locale}`, x + 5, y + 8);
}

async function generateLeafletCover(params, contest, scores, entrantList) {
   document.getElementById(params.spinID).style.visibility = "visible";
   document.getElementById(params.iframeID).style.visibility = "hidden";

   if (contest.categories.length > 1)
      throw "More than one category contest is not yet supported";

   let pdf = new jsPDF({
      orientation: "l",
      unit: "mm",
      format: "a4",
      putOnlyUsedFonts: true,
      compress: true,
      floatPrecision: 16 // or "smart", default is 16
   });
   pdf.setFont(params.fontName);
   pdf.setDocumentProperties({
      title: '{{ lang en="Cover" fr="Couverture" de="Startseite" es="Cubrir" }}',
      subject: '',
      author: "Radio Control System",
      keywords: "F5J, radio control contest",
      creator: "RC-Contest.com"
   });

   if (scores.length === 0) {
      pdf.setFontSize(30);
      pdf.text(`{{ lang en="Rounds draw is not yet saved" fr="Le tirage au sort n'est pas encore enregistré" de="Die Auslosung der Runden ist noch nicht gespeichert" es="El sorteo de Mangas aún no se ha guardado" }}`, 30, 30);
   } else {
      let y = -1;
      let isFirstpage = true;

      let selectedEntrantIDList = [];
      for (let score of scores) {
         if (!selectedEntrantIDList.includes(score.pilot_id))
            selectedEntrantIDList.push(score.pilot_id);
      }
      let restrictedEntrantList = [];
      for (const entrant of entrantList) {
         if (!selectedEntrantIDList.includes(entrant.id))
            continue;
         restrictedEntrantList.push(entrant);
      }

      let sortedEntrantList = restrictedEntrantList.sort((a, b) => a.last_name.localeCompare(b.last_name));

      sortedEntrantList.forEach(function (item, index) {
         const x = (297.0 / 2) * (index % 2);

         if (index % 2 == 0) {
            if (y == 0) {
               y = 210.0 / 2;
            } else {
               y = 0;
               if (isFirstpage)
                  isFirstpage = false;
               else
                  pdf.addPage("a4", "l");
            }
         }

         if (item.role.includes("pi"))
            printPilotCardCover(pdf, x, y, item, contest, scores);
         else if (item.role.includes("he") && !params.isFlyoff)
            printHelperCardCover(pdf, x, y, item, contest);
      });
   }

   if (params.filename !== "")
      pdf.save(params.filename);

   if (params.iframeID !== "") {
      let iframe = document.getElementById(params.iframeID);
      if (iframe)
         iframe.src = pdf.output("datauristring", { filename: "leaflet-cover-qualif.pdf" });
   }

   document.getElementById(params.iframeID).style.visibility = "visible";
   document.getElementById(params.spinID).style.visibility = "hidden";

   return pdf;
}

function penaltyFormat(pilot) {
   let ret = "";
   if (pilot.group_penalty != 0.0)
      ret += pilot.group_penalty + "p ";
   if (pilot.group_rm_penalty)
      ret += "{{ lang en='Rm' fr='Rm' de='Ms' es='Rm'}} ";
   if (pilot.group_overtime_less1m)
      ret += "Ov ";
   if (pilot.group_overtime_more1m)
      ret += "Ov+ ";
   if (pilot.group_cancel)
      ret += "{{ lang en='C' fr='A' de='A' es='C'}} ";
   return ret;
}

function timeContent(pilot) {
   let ret;

   if (pilot.group_overtime_more1m || pilot.group_rm_penalty)
      ret = "--:-- **";
   else
      ret = timeFormat(pilot.fly_time);

   return ret;
}

function distanceContent(pilot, distanceUnit) {
   let ret;

   if (pilot.group_overtime_less1m || pilot.group_overtime_more1m || pilot.group_rm_penalty)
      ret = `0 ${distanceUnit} **`;
   else {
      if (pilot.distance == null || pilot.distance == "0")
         ret = `0 ${distanceUnit}`;
      else
         ret = `${pilot.distance} ${distanceUnit}`;
   }

   return ret;
}

function startHeightContent(pilot) {
   let ret;

   if (pilot.group_overtime_more1m || pilot.group_rm_penalty)
      ret = `0 m **`;
   else
      ret = pilot.start_height !== null ? pilot.start_height + " m" : "  -";

   return ret;
}

function printPilotResult(pdf, x, y, rank, entrant, scoreList, displayHeader, firstRound, lastRound, ref, fractionDigitProfile, precisionProfile, distanceUnit, inputMode, csvFormat = false) {
   x += 4;

   if (!csvFormat) {
      let fullname = formatNames(entrant.first_name, entrant.last_name, 17);
      pdf.setFontSize(11);
      pdf.text(fullname, x + 7, y);
      pdf.text(entrant.locale.split("-")[1], x + 46, y);
   }

   let totalFractionDigit = fractionDigitProfile[precisionProfile].total.digit;
   let totalOffsetX = fractionDigitProfile[precisionProfile].total.offset;
   let scoreFractionDigit = fractionDigitProfile[precisionProfile].score.digit;
   let scoreOffsetX = fractionDigitProfile[precisionProfile].score.offset;
   let percentFractionDigit = fractionDigitProfile[precisionProfile].percent.digit;
   let percentOffsetX = fractionDigitProfile[precisionProfile].percent.offset;

   if (!inputMode) {
      let totalScore = -1;
      for (const round of scoreList) {
         rank = round.final_rank;
         if (round.removed || round.worse_score || !round.cd_validate)
            continue;

         if (round.total_score > totalScore)
            totalScore = (round.total_score === "" ? 0.0 : round.total_score);
      }

      if (!csvFormat)
         pdf.text(`${rank}`, x, y);
      else
         pdf += `${rank};${entrant.first_name} ${entrant.last_name};${entrant.locale.split("-")[1]};`;


      if (ref.score === -1)
         ref.score = totalScore;

      if (ref.score != 0) {
         const val = (100 * totalScore / ref.score).toFixed(percentFractionDigit);
         if (!csvFormat)
            pdf.text(val, x + 56 + percentOffsetX, y);
         else
            pdf += val + ";";
      }
      else {
         const val = (0).toFixed(percentFractionDigit);
         if (!csvFormat)
            pdf.text(val, x + 56 + percentOffsetX, y);
         else
            pdf += val + ";";
      }

      x += 74;
      let finalTotalScore = totalScore.toFixed(totalFractionDigit);
      let delta = Math.abs(parseFloat(finalTotalScore) - parseFloat(ref.totalScore));
      let deltaPrec = 5.0 / Math.pow(10, precisionProfile + 1);
      ref.totalScore = finalTotalScore;

      if (!csvFormat) {
         pdf.setFont(undefined, "bold");
         if ((finalTotalScore != 0.0) && (delta < deltaPrec))
            pdf.setTextColor("#FF0000");
         pdf.text(((finalTotalScore < 0) ? "0.0" : finalTotalScore), x + totalOffsetX, y);
         pdf.setTextColor("#000000");
         pdf.setFont(undefined, "normal");
      } else {
         pdf += ((finalTotalScore < 0) ? "0.0" : finalTotalScore) + ";";
      }
   } else {
      pdf.text(`${rank}`, x, y);
      x += 74;
   }

   if (!csvFormat) {
      pdf.setFontSize(9);
      pdf.text(entrant.tag, x - 74 + 5, y + 5);
      pdf.text(entrant.short_club_name ? entrant.short_club_name : "", x - 74 + 5, y + 9, { maxWidth: 25 });

      if (!inputMode)
         pdf.setFontSize(7);
      else
         y -= 5;
      pdf.text("{{ lang en='Time\nDistance\nHeight\nPenalty**' fr='Temps\nDistance\nAltitude\nPenalité**' de='Zeit\nDistanz\nHöhe\nStrafe**' es='Tiempo\nDistancia\nAltura\nPenalización**'}}", x, y + 3);
   } else {
      pdf += entrant.short_club_name ? entrant.short_club_name : "" + ";";
   }
   x += 15;
   let roundNum = 0;
   for (let idx = firstRound; idx <= lastRound; idx++) {
      const pilot = scoreList[idx];
      if (pilot === undefined)
         return pdf;
      if (pilot.removed || !pilot.cd_validate) {
         if (!pilot.cd_validate) {
            if (!csvFormat)
               pdf.text(`{{ lang en='Not validate\nby CD' fr='Pas validé\npar le DC' de='Nicht durch CD \nvalidiert' es='No validado\npor CD'}}`, x + scoreOffsetX + roundNum * 16, y + 8, { angle: 45 });
            else
               pdf += "NV;";
            x += 16;
         }
         continue;
      }

      let isRemoved = pilot.worse_score;
      let widthRound = roundNum * 16;

      if (isRemoved && !inputMode) {
         const val = Number(pilot.group_score).toFixed(scoreFractionDigit) + "*";
         if (!csvFormat) {
            pdf.setFillColor("#C0C0C0");
            let width = pdf.getStringUnitWidth(val) * 4.3;
            pdf.roundedRect(x - 1 + widthRound, y - 4, width, 5, 0.8, 0.8, "F");
            pdf.setFillColor("#FFFFFF");
         }
      }

      if (!csvFormat) {
         pdf.setFontSize(11);
         if (displayHeader)
            pdf.text(`{{ lang en="Rnd" fr="Ma" de="Ru" es="Re" }} ${idx + 1}`, x + widthRound + 2, y - 4 - (!inputMode ? 5 : 0));
      }
      if (!inputMode) {
         if (!csvFormat) {
            if (pilot.group_score === 1000)
               pdf.text(`1000${isRemoved ? " *" : ""}`, x + scoreOffsetX + widthRound + 1, y);
            else if (pilot.group_score === 0)
               pdf.text(`0${isRemoved ? " *" : ""}`, x + scoreOffsetX + widthRound + 2, y);
            else
               pdf.text(`${Number(pilot.group_score).toFixed(scoreFractionDigit)}${isRemoved ? "*" : ""}`, x + scoreOffsetX + widthRound, y);
            pdf.setFontSize(7);
         } else {
            if (pilot.group_score === 1000)
               pdf += `1000${isRemoved ? " *" : ""};`;
            else if (pilot.group_score === 0)
               pdf += `0${isRemoved ? " *" : ""};`;
            else
               pdf += `${Number(pilot.group_score).toFixed(scoreFractionDigit)}${isRemoved ? "*" : ""};`;
         }
      }

      let penalty = penaltyFormat(pilot);
      if (!csvFormat)
         pdf.text(`${timeContent(pilot)}\n${distanceContent(pilot, distanceUnit)}\n${startHeightContent(pilot)}\n${penalty !== "" ? penalty : "  -"}`, x + 3 + widthRound, y + 3);

      roundNum++;
   }

   return pdf;
}

async function generateResult(params, contest, bestPilotList, entrantList, scores, inputMode = false, csvFormat = false) {
   if (params.spinID)
      document.getElementById(params.spinID).style.visibility = "visible";
   if (params.iframeID)
      document.getElementById(params.iframeID).style.visibility = "hidden";

   let pdf;
   let props = {
      subject: "",
      author: "Radio Control System",
      keywords: "F5J, radio control contest",
      creator: "RC-Contest.com"
   };
   let csv;

   if (csvFormat) {
      csv = "rank;name;country;percent;score;club;";
      for (let round = 0; round <= params.afterRound; round++)
         csv += `{{ lang en="Rnd" fr="Ma" de="Ru" es="Re" }} ${round + 1};`;
      csv += "\n";
   } else {
      pdf = new jsPDF({
         orientation: "l",
         unit: "mm",
         format: "a4",
         putOnlyUsedFonts: true,
         compress: true,
         floatPrecision: 16
      });
      pdf.setFont(params.fontName);
   }

   if (!inputMode) {
      if (params.isFlyoff)
         props.title = '{{ lang en="Flyoff results" fr="Résultats du Flyoff" de="Flyoff-Ergebnis" es="Resultado del Flyoff" }}';
      else
         props.title = '{{ lang en="Qualification results" fr="Résultats des qualifications" de="Ergebnis der Qualifizierung" es="Resultado de la calificación" }}';
   }
   else {
      props.title = '{{ lang en="Inputs" fr="Entrées" de="Eingaben" es="Entradas" }}';

      let sortedEntrantList = entrantList.sort((a, b) => a.last_name.localeCompare(b.last_name));
      let bestSortedByNamePilotList = [];
      for (const entrant of sortedEntrantList) {
         if (entrant.role.includes("pi") && bestPilotList.includes(entrant.id))
            bestSortedByNamePilotList.push(entrant.id);
      }
      bestPilotList = bestSortedByNamePilotList;
   }

   if (!csvFormat)
      pdf.setDocumentProperties(props);

   let pageNum = 1;

   if (scores.length === 0) {
      let msg = `{{ lang en="Rounds draw is not yet saved" fr="Le tirage au sort n'est pas encore enregistré" de="Die Auslosung der Runden ist noch nicht gespeichert" es="El sorteo de Mangas aún no se ha guardado" }}`;
      if (!csvFormat) {
         pdf.setFontSize(30);
         pdf.text(msg, 30, 30);
      }
      else
         csv += msg;
   } else {
      let curRound = params.afterRound ? params.afterRound : contest.categories[0].cur_round;
      let distanceUnit = contest.categories[0].distance_unit;

      let fractionDigitProfile = [
         {
            total: { digit: 1, offset: -2 },
            score: { digit: 1, offset: 0 },
            percent: { digit: 2, offset: 0 }
         },
         {
            total: { digit: 2, offset: -3 },
            score: { digit: 2, offset: 0 },
            percent: { digit: 2, offset: -2 }
         },
         {
            total: { digit: 3, offset: -5 },
            score: { digit: 3, offset: 0 },
            percent: { digit: 3, offset: -2 }
         },
      ];
      let precisionProfile = params.precisionProfile;

      let x = 0;
      let y = 11;
      let rank = 1;

      if (!csvFormat) {
         // header
         let img = new Image();
         img.src = "/image/20 noir.png";
         pdf.addImage(img, "PNG", 6, 3, 15, 15);
         pdf.setTextColor("#000000");
         pdf.setFont(undefined, "normal");
         pdf.setFontSize(14);
         //let category = contest.categories[0].category.toUpperCase();

         pdf.text(`${contest.name} - ${props.title}`, 17 + 8, y);
         pdf.setFontSize(9);
      }
      y += 4;

      let header;
      if (!inputMode)
         header = `{{ lang
            en="#   Name                              Ctry         %           Score"
            fr="Rang       Nom                   Pays         %             Score"
            de="Rang       Name                  Land         %             Score"
            es="Rango      Nombre              País               %         Puntuación"
         }}`;
      else
         header = `{{ lang
            en="Rank        Name                 Ctry"
            fr="Rang       Nom                   Pays"
            de="Rang       Name                  Land"
            es="Rango      Nombre              País"
         }}`;
      let footer = `www.rc-contest.com     v: ${params.serverVersion}`;
      let legend = `{{ lang 
         en="(*) Worse removed    (**) Penalty  Ov=Overtime < 1mn, Ov+=Overtime > 1mn, C=Cancel, Rm=Restart motor"
         fr="(*) Pire retirée     (**) Pénalité Ov=Overtime < 1mn, Ov+=Overtime > 1mn, A=Annulé, Rm=Remise moteur"
         de="(*) Schlim. entfernt (**) Strafe   Ov=Overtime < 1mn, Ov+=Overtime > 1mn, A=Abbrechen, Ms=Motor neu starten"
         es="(*) Peor eliminado   (**) Penaliz. Ov=Overtime < 1mn, Ov+=Overtime > 1mn, C=Cancelar, Rm=Reiniciar motor"
      }}`;

      if (!csvFormat) {
         pdf.text(contest.start_datetime, 17 + 8, y);
         pdf.text(footer, x + 8, y + 190);
         pdf.text(legend, x + 120, y + 190);

         pdf.setTextColor("#000000");
         pdf.setFont(undefined, "bold");
         pdf.setFontSize(11);
         pdf.setFillColor("#D0D0D0");
         pdf.rect(x + 4, y + 3, 297 - 10, 7.5, "F");
         pdf.setFillColor("#FFFFFF");
         pdf.text(header, x + 6, y + 8);
         pdf.setFont(undefined, "normal");
      }

      x = 2;
      y += 17;

      let displayHeader = true;
      let pageCache = [];
      let firstRound = 0;
      let lastRound = (curRound < 11) ? curRound : 11;
      let ref = { score: -1, prevTotalScore: -1 };
      let firstPage = true;

      for (const pilotID of bestPilotList) {
         let entrant = entrantList.find((item) => item.id === pilotID);
         let scoresEntrant = scores.filter((item) => item.entrant_id === pilotID);
         scoresEntrant = scoresEntrant.filter((item) => item.removed != true);

         if (!csvFormat)
            pageCache.push({ pdf: pdf, x: x, y: y, rank: rank, entrant: entrant, scoresEntrant: scoresEntrant });

         let ret = printPilotResult(pdf ? pdf : "", x, y, rank, entrant, scoresEntrant, displayHeader, firstRound, lastRound, ref, fractionDigitProfile, precisionProfile, distanceUnit, inputMode, csvFormat);
         if (csvFormat)
            csv += ret + "\n";

         displayHeader = false;

         if (!csvFormat) {
            y += 18;
            if ((y + 18) > 210) {
               pdf.addPage("a4", "l");
               x = 0;
               y = 0;
               pageNum++;

               pdf.setFont(undefined, "bold");
               pdf.setFontSize(11);
               pdf.setFillColor("#D0D0D0");
               pdf.rect(x + 4, y + 3, 297 - 8, 7.5, "F");
               pdf.setFillColor("#FFFFFF");
               pdf.text(header, x + 6, y + 8);
               pdf.setFont(undefined, "normal");
               pdf.setFontSize(9);
               pdf.text(footer, x + 8, y + 205);
               pdf.text(legend, x + 120, y + 205);
               pdf.setFont(undefined, "normal");

               displayHeader = true;

               if (curRound > 11) {
                  firstRound = 12;
                  lastRound = curRound;

                  for (const item of pageCache) {
                     printPilotResult(item.pdf, item.x, firstPage ? item.y - 15 : item.y, item.rank, item.entrant, item.scoresEntrant, displayHeader, firstRound, lastRound, ref, fractionDigitProfile, precisionProfile, distanceUnit, inputMode);
                     displayHeader = false;
                  }

                  pdf.addPage("a4", "l");
                  firstPage = false;
                  x = 0;
                  y = 0;
                  pageNum++;

                  pdf.setFont(undefined, "bold");
                  pdf.setFontSize(11);
                  pdf.setFillColor("#D0D0D0");
                  pdf.rect(x + 4, y + 3, 297 - 8, 7.5, "F");
                  pdf.setFillColor("#FFFFFF");
                  pdf.text(header, x + 6, y + 8);
                  pdf.setFont(undefined, "normal");
                  pdf.setFontSize(9);
                  pdf.text(footer, x + 8, y + 205);
                  pdf.text(legend, x + 120, y + 205);
                  pdf.setFont(undefined, "normal");

                  x = 0;
                  y = 17;
                  pageCache = [];
                  firstRound = 0;
                  lastRound = (curRound < 11) ? curRound : 11;
                  displayHeader = true;
               } else {
                  x = 0;
                  y = 17;
               }
            }
            rank++;
         }
      }

      if (!csvFormat) {
         if (pageCache.length > 0) {
            if (curRound > 11) {
               pdf.addPage("a4", "l");
               x = 0;
               y = 0;
               pageNum++;

               pdf.setFont(undefined, "bold");
               pdf.setFontSize(11);
               pdf.setFillColor("#D0D0D0");
               pdf.rect(x + 4, y + 3, 297 - 8, 7.5, "F");
               pdf.setFillColor("#FFFFFF");
               pdf.text(header, x + 6, y + 8);
               pdf.setFont(undefined, "normal");
               pdf.setFontSize(9);
               pdf.text(footer, x + 8, y + 205);
               pdf.text(legend, x + 160, y + 205);
               pdf.setFont(undefined, "normal");

               firstRound = 12;
               lastRound = curRound;

               for (const item of pageCache) {
                  printPilotResult(item.pdf, item.x, firstPage ? item.y - 15 : item.y, item.rank, item.entrant, item.scoresEntrant, displayHeader, firstRound, lastRound, ref, fractionDigitProfile, precisionProfile, distanceUnit, inputMode);
                  displayHeader = false;
               }

               pageCache = [];
            }
         }
      }
   }

   if (csvFormat) {

      return csv;
   }
   else {
      pdf.setFont(undefined, "normal");
      pdf.setFontSize(11);
      for (let p = 1; p <= pageNum; p++) {
         pdf.setPage(p);
         pdf.text(`${p}/${pageNum}`, 297 - 15, 205);
      }

      pdf.setLanguage('{{ lang en="en-US" fr="fr-FR" de="de-DE" es="es-ES"}}');

      if (params.filename !== "")
         pdf.save(params.filename);

      if (params.iframeID !== "") {
         let iframe = document.getElementById(params.iframeID);
         if (iframe)
            iframe.src = pdf.output("datauristring", { filename: "results-qualif.pdf" });
      }

      document.getElementById(params.iframeID).style.visibility = "visible";
      document.getElementById(params.spinID).style.visibility = "hidden";

      return pdf;
   }
}

async function generateReports(params) {
   // https://fonts.google.com/specimen/Roboto?preview.text=Frąk,%20Tomasz&preview.text_type=custom
   jsPDF.API.events.push(["addFonts", function () {
      this.addFileToVFS("Roboto-Regular-normal.ttf", RobotoRegularFont);
      this.addFont("Roboto-Regular-normal.ttf", "Roboto-Regular", "normal");
      this.addFileToVFS("Roboto-Bold-normal.ttf", RobotoBoldRegularFont);
      this.addFont("Roboto-Bold-normal.ttf", "Roboto-Regular", "bold");
      //this.addFileToVFS("Poppins-Regular-normal.ttf", PoppinsRegularFont);
      //this.addFont("Poppins-Regular-normal.ttf", "Poppins-Regular", "normal");
   }]);
   //params.fontName = "helvetica";
   //params.fontName = "Poppins-Regular";
   params.fontName = "Roboto-Regular";

   await fetch(`/api/v1/recomputeAll?contest_id=${params.contestID}&category_id=${params.categoryID}&is_flyoff=${params.isFlyoff}&is_junior=${params.isJunior}&round_id=${params.afterRound}`)
      .then(handleErrors)
      .then(result => {
      })
      .catch(function (error) {
         displayErrorMsg(error);
      });

   let scores;
   if (params.isFlyoff) {
      //if (params.cache.flyoff.scores == undefined || params.cache.flyoff.scores == 0) {
      await fetch(`/api/v1/getScoreList?contest_id=${params.contestID}&is_flyoff=true&is_junior=${params.isJunior}`)
         .then(handleErrors)
         .then(result => {
            scores = params.cache.flyoff.scores = result;
         })
         .catch(function (error) {
            displayErrorMsg(error);
         });
      /*} else {
         scores = params.cache.flyoff.scores;
      }*/
   } else {
      //if (params.cache.qualif.scores == undefined || params.cache.qualif.scores.length == 0 || params.afterRound != undefined) {
      let query = `/api/v1/getScoreList?contest_id=${params.contestID}&is_flyoff=false&is_junior=${params.isJunior}`;
      if (params.afterRound != undefined)
         query += `&round_id=${params.afterRound}&scope=lessOrEqualRound`;
      await fetch(query)
         .then(handleErrors)
         .then(result => {
            if (params.reflyRound != undefined && params.reflyGroup != undefined) {
               let filteredScore = [];

               for (let score of result) {
                  if (params.reflyRound != score.round_id || params.reflyGroup != score.group_id)
                     continue;
                  filteredScore.push(score);
               }

               scores = params.cache.qualif.scores = filteredScore;
            }
            else
               scores = params.cache.qualif.scores = result;

         })
         .catch(function (error) {
            displayErrorMsg(error);
         });
      /*} else {
         scores = params.cache.qualif.scores;
      }*/
   }

   let contest;
   if (params.cache.contest == undefined) {
      await fetch(`/api/v1/getContest?contest_id=${params.contestID}`)
         .then(handleErrors)
         .then(result => {
            contest = params.cache.contest = result;
         })
         .catch(function (error) {
            displayErrorMsg(error);
         });
   } else {
      contest = params.cache.contest;
   }

   let entrantList;
   if (params.cache.entrants == undefined) {
      await fetch(`/api/v1/getEntrantList?contest_id=${params.contestID}`)
         .then(handleErrors)
         .then(result => {
            entrantList = params.cache.entrants = result;
         })
         .catch(function (error) {
            displayErrorMsg(error);
         });
   } else {
      entrantList = params.cache.entrants;
   }

   let pilotMatrix;
   if (params.list.includes("draw")) {
      await fetch(`/api/v1/getDrawContest?contest_id=${params.contestID}&is_flyoff=${params.isFlyoff}&is_junior=${params.isJunior}`)
         .then(handleErrors)
         .then(result => {
            pilotMatrix = result;
         })
         .catch(function (error) {
            displayErrorMsg(error);
         });
   }

   let bestPilotList;
   if (params.list.includes("inputs") || params.list.includes("results") || params.list.includes("cover") || params.list.includes("csv")) {
      let roundID = -1;
      if (!params.isFlyoff)
         roundID = params.afterRound != undefined ? params.afterRound : -1;
      await fetch(`/api/v1/getBestPilotList?contest_id=${params.contestID}&is_flyoff=${params.isFlyoff}&is_junior=${params.isJunior}&round_id=${roundID}`)
         .then(handleErrors)
         .then(data => {
            bestPilotList = data;
         })
         .catch(function (error) {
            displayErrorMsg(error);
         });
      await fetch(`/api/v1/getNotValidatedPilotList?contest_id=${params.contestID}&is_flyoff=${params.isFlyoff}&is_junior=${params.isJunior}&round_id=${roundID}`)
         .then(handleErrors)
         .then(data => {
            for (const pilotID of data)
               bestPilotList.push(pilotID);
         })
         .catch(function (error) {
            displayErrorMsg(error);
         });
   }

   try {
      if (params.list.includes("draw"))
         generateDrawMatrix(params, contest, pilotMatrix);
      if (params.list.includes("leaflet"))
         generateLeaflet(params, scores, entrantList);
      if (params.list.includes("cover"))
         generateLeafletCover(params, contest, scores, entrantList);
      if (params.list.includes("inputs"))
         generateResult(params, contest, bestPilotList, entrantList, scores, true);
      if (params.list.includes("csv"))
         return generateResult(params, contest, bestPilotList, entrantList, scores, false, true);
      if (params.list.includes("results"))
         generateResult(params, contest, bestPilotList, entrantList, scores);
   } catch (error) {
      displayErrorMsg(error);
   }
}
