/*
RC-Contest (c) 2019-2024 Thierry Wilmot - All rights reserved
*/

var useOpenAPI = false;
var tippyElementMap = [];
var elementMap = [];
var fullElementMap = [];

async function fillDropdowCategory() {
   let organizations;
   await getOrganizationList()
      .then(function (response) {
         organizations = response;
      })
      .catch(function (error) {
         displayErrorMsg(error);
      });

   await getCategoryList('all')
      .then(function (response) {
         let optGroup = $('<optgroup>', { label: "{{ lang en='General' fr='Général' de='Allgemeines' es='General'}}" });
         optGroup.append($('<option>', { value: "no_cat", title: "{{ lang en='categoryless' fr='Sans catégorie' de='kategorienlos' es='Sin categoría' }}", "data-content": '<span class="text-monospace"><b>{{ lang en="categoryless" fr="Sans catégorie" de="kategorienlos" es="Sin categoría" }}</b></span> {{ lang en="Meeting, training..." fr="Rencontre, entrainement..." de="Treffen, Training ..." es="Reunión, formación ..." }}' }));
         $("#category-select").append(optGroup);

         for (let organization of organizations) {
            optGroup = $('<optgroup>', { label: organization.short_name });
            jQuery.each(response, function (key, value) {
               if (value.organizations.includes(organization.short_name))
                  optGroup.append($('<option>', { value: value.code, title: value.code, "data-content": '<span class="text-monospace"><b>' + value.code + '</b></span> ' + value.name }));
            });
            $("#category-select").append(optGroup);
         }

         $('#category-select').selectpicker('refresh');

         currentCategory = getUserPrefsValue('idxClassFilt', ['all']);
         $('#category-select').selectpicker('val', currentCategory);
         if (currentCategory.length === 1 && currentCategory[0] == 'all')
            $('#category-select').selectpicker('selectAll');

         $('#category-select').on('changed.bs.select', function (event, clickedIndex, isSelected, previousValue) {
            currentCategory = $('#category-select').selectpicker('val');
            setUserPrefsValue('idxClassFilt', currentCategory);
            markers.clearLayers();
            mymap.setView([0, 0], 2);
            calendar.setDataSource(function () { return getCalendarData(); });
         });

      })
      .catch(function (error) {
         displayErrorMsg(error);
      });
}

async function fillDropdowCountry() {
   await fetch('/api/v1/op/getCountryList?scope=contest')
      .then(handleErrors)
      .then(async function (result) {
         for (const value of result) {
            var img = `<span><img src='json/countries/data/${value.toLowerCase()}.svg' style='width:25px;height:18px;'> ${value}</span>`;
            $('#country-select').append($(`<option value="${value}" data-content="${img}"></option>`));
         }
         $('#country-select').selectpicker('refresh');
         let cca3;
         if (getUserPrefsValue('idxCntFilt') == "") {
            let cca2 = currentUser.locale.split("-")[1];

            await fetch("/api/v1/op/getCountryList")
               .then(handleErrors)
               .then(function (response) {
                  cca3 = response.cca2_cca3[cca2];
               })
               .catch(function (error) {
                  displayErrorMsg(error);
               });
         }
         currentCountry = getUserPrefsValue('idxCntFilt', [cca3]);
         $('#country-select').selectpicker('val', currentCountry);
         if (currentCountry.length === 1 && currentCountry[0] == 'all')
            $('#country-select').selectpicker('selectAll');

         $('#country-select').on('changed.bs.select', function (event, clickedIndex, isSelected, previousValue) {
            currentCountry = $('#country-select').selectpicker('val');
            setUserPrefsValue("idxCntFilt", currentCountry);
            markers.clearLayers();
            mymap.setView([0, 0], 2);
            calendar.setDataSource(function () { return getCalendarData(); });
         });

      })
      .catch(function (error) {
         displayErrorMsg(error);
      });
}

async function fillDropdowEventTypes() {
   currentEventTypes = getUserPrefsValue('idxEvtFilt', ['all']);
   $('#event-types-select').selectpicker('val', currentEventTypes);
   if (currentEventTypes.length === 1 && currentEventTypes[0] == 'all')
      $('#event-types-select').selectpicker('selectAll');

   $('#event-types-select').on('changed.bs.select', function (event, clickedIndex, isSelected, previousValue) {
      currentEventTypes = $('#event-types-select').selectpicker('val');
      setUserPrefsValue("idxEvtFilt", currentEventTypes);
      markers.clearLayers();
      mymap.setView([0, 0], 2);
      calendar.setDataSource(function () { return getCalendarData(); });
   });

}

function updateLastUpdate() {
   fetch('/api/v1/op/getLastUpdate?entity=contest_list')
      .then(handleErrors)
      .then(function (result) {
         if (result.last_update != undefined) {
            let dt = result.last_update.split(" ");
            $('#lastUpdateDate').text(getDatetimeFormatted(new Date(dt[0]), "date-only"));
            $('#lastUpdateTime').text(dt[1]);
         }
      })
      .catch(function (error) {
         displayErrorMsg(error);
      });
}

function operateFormatter(value, row) {
   return '<button class="btn-circle btn-secondary btn-sm" style="border:0;" onclick="toggleWishList(' + row.id + ', true)" ><i class="fas fa-trash"></i></button>';
}

function nameFormatter(value, row) {
   return `<a href="contest.html?contest_id=${row.id}">${stringTruncate(value, 10)}</a>`;
}

function categoriesFormatter(value) {
   return formatCategories(value);
}

function showFormatter(value, row) {
   return [
      //'<a class="edit" href="javascript:void(0)" title="Edit">',
      '<a href="contest.html?contest_id=' + row.id + '">',
      '<i class="far fa-eye"></i>',
      '</a>',
   ].join('');
}

function countryFormatter(value) {
   return `<img src="json/countries/data/${value.toLowerCase()}.svg" style="width:32px;height:16px;">`;
}

function localeFormatter(value) {
   return `<img src="json/countries/data/${value.split("_")[1].toLowerCase()}.svg" style="width:32px;height:16px;">`;
}

function contestFormatter(value) {
   return `<a href="${value}"><i class="fas fa-link"></i></a>`;
}

function dateFormatter(value) {
   if (value)
      return value.split(' ')[0];
   else
      return 'N/A';
}

function emailFormatter(value) {
   return `<a href="mailto:${value}">${value}</a>`;
}

function remainFormatter(value, row) {
   if (row.status == 'A')
      return row.remain_day;
   else
      return "";
}

function cellStyle(value, row) {
   if (row.status != "A")
      return {
         classes: 'remain-nadays'
      };

   if (row.remain_day == 0) {
      return {
         classes: 'remain-0days'
      };
   }
   if (row.remain_day < 2) {
      return {
         classes: 'remain-2days'
      };
   }
   if (row.remain_day < 5) {
      return {
         classes: 'remain-4days'
      };
   }

   return {
   };
}

// Calendar
var tooltip = null;

function dateNotInPast(date) {
   return (new Date() < date);
}

function createCalendar(useOpen, language) {

   useOpenAPI = useOpen;

   let options = {
      minDate: new Date(2016, 0, 1),
      maxDate: new Date(2024, 11, 31),
      style: "custom",
      clickDay: function (param) {
         // jump to contest page when only 1 contest for this days
         // bug : event click on wish list + is not consume by tippy
         // following code is comment as long as I dont handle that properlly
         window.event.stopPropagation();

         if ((param.events.length === 1) && param.events[0]) {
            let event = param.events[0];
            if (canAccesToContest(event, currentUser)) {
               let item ={categories: event.details.categories};
               let path = makePathFromModule(item, "contest", "contest_id=" + event.contest_id, currentUser);
               window.open(path, "_self");
            }
         } else {
            displayAlert({
               type: 'error',
               msg: "{{ lang en='Multiple events for this date, please select event in tooltip' fr='Plusieurs événements pour cette date, veuillez sélectionner l\'événement dans l\'info-bulle' de='Mehrere Ereignisse für dieses Datum, bitte Ereignis im Tooltip auswählen' es='Múltiples eventos para esta fecha, seleccione el evento en la información sobre herramientas' }}",
               divElt: $('#successUpdateAlert'),
               spanElt: $('#alertMsg')
            });
      }
      },
      customDayRenderer: function (HTMLElement, currentDate) {
         if (currentDate.setHours(0, 0, 0, 0) == new Date().setHours(0, 0, 0, 0))
            HTMLElement.style = 'border-radius:50%;border: 2px solid #b3b3b3;';
      },
      customDataSourceRenderer: function (HTMLElement, currentDate, events) {
         HTMLElement.style.backgroundColor = events[events.length - 1].backgroundColor;

         let tippyElement = fillContestList(HTMLElement, events, true);
         tippyElement.addEventListener('click', function (/*event*/) {
            window.event.stopPropagation();
            //console.log("stop progatate");
         });

         tippy(HTMLElement, {
            //placement: "right",
            content: tippyElement,
            zIndex: 99,
            allowHTML: true,
            animateFill: false,
            //animation: "scale-subtle",
            //animation: "shift-away",
            theme: "rcs",
            interactive: true,
            //delay: [100, 0],
            arrow: true,
            onShown: function (instance) {
               // in case of tooltips is raised over tippy
               $('[data-toggle="tooltip"]').tooltip({ offset: "0,10", zindex: 99999 });
            }
         });
      }
   };

   if (language != undefined)
      options.language = language;

   if (useOpenAPI) {
      let curYear = new Date().getFullYear();
      let curMonth = new Date().getMonth();
      let daysInMonth = new Date(curYear, curMonth, 0).getDate();

      options = {
         ...options,
         startDate: new Date(curYear, curMonth - 1),
         endDate: new Date(curYear, curMonth, daysInMonth),
         numberMonthsDisplayed: 2,
         minDate: new Date(curYear, curMonth - 1),
         maxDate: new Date(curYear, curMonth, daysInMonth)
      };
   }

   calendar = new Calendar('#calendar', options);
   calendar.setYear(currentYear);

   document.querySelector('.calendar').addEventListener('yearChanged', function (event) {
      currentYear = event.currentYear;
      setUserPrefsValue('curCalYear', currentYear);
      markers.clearLayers();
      mymap.setView([0, 0], 2);
   });

   document.querySelector('.calendar').addEventListener('renderEnd', function (/*event*/) {
      if (currentYear == 2022)
         $(".months-container").css("background-color", "#fff");
      else
         $(".months-container").css("background-color", "#F7F7F7");
   });
}

function fillContestList(HTMLElement, events, forTooltip) {
   let rootElement = createElementRCS({ tagName: "div" });
   let fullRootElement = createElementRCS({ tagName: "div" });
   let countItem = 0;
   let ellipsys = false;
   events.reverse().forEach(function (item) {
      {
         if (countItem++ < 5) {
            let eventElement = createElementRCS({ tagName: "div", parent: rootElement, className: "event-tooltip-content" });
            let rowElement = createElementRCS({ tagName: "div", parent: eventElement, className: "row" });
            if (dateNotInPast(item.endDate)) {
               let labelElement = createElementRCS({ tagName: "label", parent: rowElement, className: "custom-checkbox a-add-wishlist", style: "margin-right:5px;margin-top:1px;margin-bottom:0px;" });
               let inputElement = createElementRCS({ tagName: "input", parent: labelElement, type: "checkbox", checked: item.details.wish, onchange: function () { toggleWishList(item.contest_id, false); } });
               createElementRCS({ tagName: "i", className: "fas fa-heart checked", parent: labelElement });
               createElementRCS({ tagName: "i", className: "far fa-heart unchecked", parent: labelElement });

               if (tippyElementMap[item.contest_id] == undefined)
                  tippyElementMap[item.contest_id] = [];
               tippyElementMap[item.contest_id].push(inputElement);

               if (elementMap[item.contest_id] == undefined)
                  elementMap[item.contest_id] = [];
               elementMap[item.contest_id].push(HTMLElement);

               if (item.details.wish)
                  HTMLElement.style.border = "1px solid #A0A0A0";
            }
            else
               rowElement.innerHTML = '<i class="fas fa-heart a-add-wishlist-disabled" style="margin-right:5px;margin-top:3px;"></i> ';

            let eventName = createElementRCS({ tagName: "div", className: "event-name", parent: rowElement });
            eventName.innerHTML = `<span class="badge ${contestStatusClass(item.details.status)}" data-toggle="tooltip" data-placement="top" title="${contestStatusText(item.details.status)}">${item.details.text}</span>&nbsp;` + item.name;
         } else {
            if (!ellipsys) {
               ellipsys = true;
               let eventElement = createElementRCS({ tagName: "div", parent: rootElement, className: "event-tooltip-content" });
               let rowElement = createElementRCS({ tagName: "div", parent: eventElement, className: "row" });
               let eventName = createElementRCS({ tagName: "div", className: "event-name", parent: rowElement });
               eventName.innerHTML = "<span class='text-xl-left'><a href='javascript:openAllContestModal(" + item.contest_id + ")'>{{ lang en='More' fr='Plus' de='Mehr' es='Más'}}</a> ...</span>";
               //eventName.innerHTML = "<button type='button' class='btn btn-outline-secondary btn-sm' data-toggle='modal' data-target='#allContestModal'>{{ lang en='More' fr='Plus' de='Mehr' es='Más'}}</button>";
            }
         }
      }
      {
         let eventElement = createElementRCS({ tagName: "div", parent: fullRootElement, className: "event-tooltip-content" });
         let rowElement = createElementRCS({ tagName: "div", parent: eventElement, className: "row" });
         if (dateNotInPast(item.endDate)) {
            let labelElement = createElementRCS({ tagName: "label", parent: rowElement, className: "custom-checkbox a-add-wishlist", style: "margin-right:5px;margin-top:1px;margin-bottom:0px;" });
            let inputElement = createElementRCS({ tagName: "input", parent: labelElement, type: "checkbox", checked: item.details.wish, onchange: function () { toggleWishList(item.contest_id, false); } });
            createElementRCS({ tagName: "i", className: "fas fa-heart checked", parent: labelElement });
            createElementRCS({ tagName: "i", className: "far fa-heart unchecked", parent: labelElement });

            if (item.details.wish)
               HTMLElement.style.border = "1px solid #A0A0A0";
         }
         else
            rowElement.innerHTML = '<i class="fas fa-heart a-add-wishlist-disabled" style="margin-right:5px;margin-top:3px;"></i> ';

         let eventName = createElementRCS({ tagName: "div", className: "event-name", parent: rowElement });
         eventName.innerHTML = `<span class="badge ${contestStatusClass(item.details.status)}" data-toggle="tooltip" data-placement="top" title="${contestStatusText(item.details.status)}">${item.details.text}</span>&nbsp;` + item.name;

         if (fullElementMap[item.contest_id] == undefined)
            fullElementMap[item.contest_id] = [];
         fullElementMap[item.contest_id].push(HTMLElement);

      }
   });

   return rootElement;
}

var global_wcs_color = getComputedStyle(document.documentElement).getPropertyValue('--wcs-color');
var global_ccs_color = getComputedStyle(document.documentElement).getPropertyValue('--ccs-color');
var global_wcc_color = getComputedStyle(document.documentElement).getPropertyValue('--wcc-color');
var global_ccc_color = getComputedStyle(document.documentElement).getPropertyValue('--ccc-color');
var global_nat_color = getComputedStyle(document.documentElement).getPropertyValue('--nat-color');
var global_loc_color = getComputedStyle(document.documentElement).getPropertyValue('--loc-color');
var global_met_color = getComputedStyle(document.documentElement).getPropertyValue('--met-color');
var global_exa_color = getComputedStyle(document.documentElement).getPropertyValue('--exa-color');
var global_tra_color = getComputedStyle(document.documentElement).getPropertyValue('--tra-color');
var global_unk_color = getComputedStyle(document.documentElement).getPropertyValue('--unk-color');

function getBgColorCell(item) {
   switch (item.selection_type) {
      case 'wcs': return global_wcs_color;
      case 'ccs': return global_ccs_color;
      case 'wcc': return global_wcc_color;
      case 'ccc': return global_ccc_color;
      case 'nat': return global_nat_color;
      case 'loc': return global_loc_color;
      case 'met': return global_met_color;
      case 'exa': return global_exa_color;
      case 'tra': return global_tra_color;
      default: return global_unk_color;
   }
}

function openAllContestModal(contestID) {
   $('#allContestModal').modal('show');
   $("#allContestModal").on('shown.bs.modal', function () {
      let allElement = fullElementMap[contestID];
      let elt = document.getElementById("AllContestBodyModal");
      elt.innerHTML = allElement;
   });
}

function makePathFromModule(item, type, params, user) {
   let path = null;
   let defaultPath = null;

   for (const module of user.modules) {
      const categories = item.categories ? item.categories.split(",") : [item.category];
      for (const category of categories) {
         if (module.categories.includes("*")) {
            defaultPath = `module/${module.url_path}/${type}.html?${params}`;
         }
         else if (module.categories.includes(category)) {
            path = `module/${module.url_path}/${type}.html?${params}`;
            break;
         }
      }
   }
   if (defaultPath && !path)
      path = defaultPath;

   return path;
}

function gotoContest(path) {
   window.event.stopPropagation();
   window.open(path, "_self");
}

function getEventDecoration(item) {
   let content = `<span class='text-monospace text-xs'>${formatCategories(item.categories)}</span> <img src="/json/countries/data/${item.country.toLowerCase()}.svg" class="country-flag-sm"> `;
   if (item.club_unknown == true)
      content += '<i class="fas fa-shield-alt" style="color:#bbbbbb;"></i> ';
   else
      content += '<a href="club-mg.html?club_id=' + item.club_id + '"><i class="fas fa-shield-alt" data-toggle="tooltip" data-placement="top" title="club"></i></a> ';
      //content += `<span onclick="gotoContest('club-mg.html?club_id=${item.club_id}')"><i class="fas fa-shield-alt" data-toggle="tooltip" data-placement="top" title="club"></i></span> `;

   content += `<span class="badge ${contestTypeClass(item.selection_type)}">`;

   if (canAccesToContest(item, currentUser)) {
      let path = makePathFromModule(item, "contest", "contest_id=" + item.id, currentUser);
      if (path)
         content += `<a href="${path}">${stringTruncate(item.name, 20)}</a>`;
         //content += `<div class="event-name-text" onclick="gotoContest('${path}')">${stringTruncate(item.name, 20)}</div>`;
      else
         content += stringTruncate(item.name, 20);
   }
   else
      content += stringTruncate(item.name, 20);

   content += "</span>";

   return content;
}

function getCalendarData() {
   let data = fetch(`api/v1/` + (useOpenAPI ? 'op/' : '') + `getContestList?sort=${sortDir}&fields=full&categories=${currentCategory.join(",")}&countries=${currentCountry.join(",")}&scope=year_${currentYear}&event_types=${currentEventTypes}`)
      .then(handleErrors)
      .then(result => {
         result.forEach(function (item) {
            addMarkerMap({ data: item, duplicate: 'merge', content: 'both' });
         });

         mymap.addLayer(markers);
         if (markers.getLayers().length != 0) {
            mymap.fitBounds(markers.getBounds(), { maxZoom: 17, padding: [4, 4] });
         }

         return result.map(item => ({
            startDate: new Date(item.start_datetime.split(" ")[0]),
            endDate: new Date(item.end_datetime.split(" ")[0]),
            name: getEventDecoration(item),
            details: {
               status: item.status,
               text: contestStatusChar(item.status),
               wish: (item.wish) ? true : false,
               categories: item.categories
            },
            contest_id: item.id,
            status: item.status,
            backgroundColor: getBgColorCell(item)
         }));
      })
      .catch(function (error) {
         displayErrorMsg(error);
      });

   return data;
}

var searchTooltip = null;
function createSearchPopper() {
   tippy('#generalSearch', {
      placement: 'bottom',
      content: '{{ lang en="Empty result" fr="Résultat vide" de="Leeres Ergebnis" es="Resultado vacío" }}',
      allowHTML: true,
      animateFill: false,
      hideOnClick: false,
      animation: 'shift-away',
      arrow: false,
      interactive: true,
      //offset: [0,4],    // v 6.x
      theme: 'light',
      onShow(instance) {
         searchTooltip = instance;
      }
   });
}

var searchTooltipContent = '';
function onSearchChanged() {
   var element = document.getElementById('generalSearch');

   fetch('/api/v1/smartSearch?txt=' + element.value)
      .then(handleErrors)
      .then(result => {
         searchTooltipContent = '';
         result.forEach(function (item) {
            searchTooltipContent += '<div>';
            searchTooltipContent += "<span class='text-monospace text-xs'>" + item.year + "</span> ";
            searchTooltipContent += `<span class="badge text-xs ${contestStatusClass(item.status)}">${contestStatusChar(item.status)}</span>&nbsp;`;
            searchTooltipContent += "<span class='text-monospace text-xs'>" + item.category + "</span> <img class='country-flag-sm' src='json/countries/data/" + item.country.toLowerCase() + ".svg'> ";
            if (item.club_unknown == false)
               searchTooltipContent += " <a href='club-mg.html?club_id=" + item.club_id + "'><i class='fas fa-shield-alt'></i></a> ";
            else
               searchTooltipContent += '<span><i class="fas fa-shield-alt" style="color:#bbbbbb;"></i></span>';

            let path = makePathFromModule(item, "contest", "contest_id=" + item.id, currentUser);
            searchTooltipContent += `<a href="${path}">${stringTruncate(item.name, 22)}</a></div>`;
         });

         if (searchTooltip !== null) {
            if (searchTooltipContent === '')
               searchTooltip.setContent('{{ lang en="Empty result" fr="Résultat vide" de="Leeres Ergebnis" es="Resultado vacío" }}');
            else
               searchTooltip.setContent(searchTooltipContent);
         }
      })
      .catch(function (error) {
         displayErrorMsg(error);
      });
}
