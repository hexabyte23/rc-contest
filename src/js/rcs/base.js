/*
 * Copyright (c) 2019-2024 Thierry Wilmot
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

class RCSChronographState {
   static RCS_CHRONOGRAPH_STATE_STOPPED = 0;
   static RCS_CHRONOGRAPH_STATE_RUNNING = 1;
   static RCS_CHRONOGRAPH_STATE_IN_PAUSE = 2;
}

class RCSRegExp extends RegExp {
   [Symbol.matchAll](str) {
      const result = RegExp.prototype[Symbol.matchAll].call(this, str);
      if (!result)
         return null;
      return Array.from(result);
   }
}

class RCSElementBase {
   constructor(params) {
      this.parentRef_ = params.element;
      if ((params.autoCallInit === true) || (params.autoCallInit === undefined))
         this.init(params);
   }

   // assign values to element innerText
   render() {
      if (this.active_) {
         for (let [key, item] of this.elementList_)
            if (item.element.innerText != item.value)
               item.element.innerText = item.value;
      }
   }

   init(param) { }

   setValue(id, value) {
      for (const [key, element] of this.elementList_) {
         if (key === id) {
            if (element.value != value) {
               element.value = value;
            }
         }
      }
   }

   setProperty(id, property, value) {
      for (const [key, element] of this.elementList_) {
         if (key === id) {
            element.element.style.setProperty(property, value);
         }
      }
   }

   getValue(id) {
      return this.elementList_.get(id).value;
   }

   addElement(id, initialValue, onValueChange) {
      let elt = document.getElementById(id);
      let pthis = this;
      elt.onchange = function (/*event*/) { 
         const item = pthis.elementList_.get(id);
         const currentValue = item.value;
         const newValue = this.value;//event.target.value;
         item.value = newValue; // assign before jump to callback
         onValueChange(currentValue, newValue);         
      };
      const item = {
         element: elt,
         value: initialValue,
         onValueChange: onValueChange
      };
      this.elementList_.set(id, item);
   }

   parentRef_ = null;   // reference to DOM parent element
   elementList_ = new Map();
   active_ = false;
   isInitialized_ = false;
}

