/*
 * Copyright (c) 2019-2024 Thierry Wilmot
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

class RCSModal extends RCSElementBase {

   constructor(params) {
      params.element = document.createElement("div");
      super(params);
   }

   init(params) {
      this.parentRef_.classList.add("modal");
      this.parentRef_.setAttribute("tabindex", "-1");
      this.parentRef_.setAttribute("role", "dialog");

      if (params.html)
         this.parentRef_.innerHTML = params.html;
      else {
         this.parentRef_.innerHTML = `
            <div class="modal-dialog" role="document">
               <div class="modal-content">
                  <div class="modal-header rcs-modal-header">
                     <h5 id="msgModalLabel" class="modal-title">
                     </h5>
                     <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  </div>
                  <div class="modal-body">
                     <span id="msgModalMsg"></span>
                  </div>
                  <div class="modal-footer">
                     <button id="msgModalCancelButton" type="button" class="btn btn-secondary" data-dismiss="modal" hidden>{{ lang en="Cancel" fr="Annuler" de="Stornieren" es="Cancelar" }}</button>
                     <button id="msgModalOkButton"  type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                  </div>
               </div>
            </div>
               `;
      }
   }

   open() {
      this.parentRef_.classList.add("show");
   }

   close() {
      this.parentRef_.classList.remove("show");
   }
}