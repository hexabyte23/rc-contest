/*
 * Copyright (c) 2019-2024 Thierry Wilmot
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

class RCSDrawPilotMatrix extends RCSElementBase {

   constructor(params) {
      super(params);

      this.#contestID_ = params.contestID;
      this.#tabContentParent_ = params.tabContent;
   }

   init(params) {

      this.#categories_ = params.categories;
      this.#pilotTab_ = params.pilotTab;
      if (this.#categories_.length == 2) {
         //if (categoryNames.includes("f3l") && categoryNames.includes("f5l"))
         this.currentCategoryName_ = "f35l";
      }

      for (const category of this.#categories_) {
         let tab = document.createElement("a");
         tab.setAttribute("class", "nav-link");
         tab.setAttribute("id", "nav-params-draw-tab-f3l");
         tab.setAttribute("data-toggle", "tab");
         tab.setAttribute("href", `#nav-draw-${category.name}`);
         tab.setAttribute("role", "tab");
         tab.setAttribute("aria-controls", `nav-draw-${category.name}`);
         tab.setAttribute("aria-selected", "false");
         tab.innerHTML = `<i class="fa-solid fa-dice"></i>&nbsp;{{ lang en="Draw rounds" fr="Tirage manches" de="Zulosung der Runden" es="Rondas de sorteos" }} ${category.name.toUpperCase()}`;
         this.parentRef_.appendChild(tab);

         let tabContent = document.createElement("div");
         tabContent.setAttribute("class", "tab-pane fade");
         tabContent.setAttribute("id", `nav-draw-${category.name}`);
         tabContent.setAttribute("role", "tabpanel");
         tabContent.setAttribute("aria-labelledby", `nav-params-draw-tab-${category.name}`);
         tabContent.innerHTML = `
            <div class="row">
               <div class="col input-group">
                  <div class="input-group-prepend">
                     <label class="input-group-text" for="rule-type-${category.name}" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang 
                        en='Rules applied during the contest. Warning, FAI rules will restrict number of round, lane...'
                        fr='Règles appliquées pendant le concours. Attention, les règles FAI limitent le nombre de manches, de cibles, groupes...'
                        de='Während des Wettbewerbs geltende Regeln. Achtung, die FAI-Regeln beschränken die Anzahl der Runden,...'
                        es='Reglas aplicadas durante el concurso. Atención, las reglas FAI restringirán el número de rondas, carriles...' }}"><i class="fa-solid fa-scale-balanced"></i></label>
                  </div>
                  <select class="custom-select form-control form-control-sm edit-if-cd" id="rule-type-${category.name}">${this.formatOptionRuleType(category)}</select>
               </div>
               <div class="col input-group">
                  <div class="input-group-prepend">
                     <label class="input-group-text" for="draw-type-${category.name}"" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang en='Strategy of draw' fr='Strategie de tirage' de='Strategie der Auslosung' es='Estrategia de sorteo'}}"><i class="fa-solid fa-chess-knight"></i></label>
                  </div>
                  <select class="custom-select form-control form-control-sm edit-if-cd" id="draw-type-${category.name}">${this.formatOptionDrawType(category)}</select>
               </div>
               <div class="col input-group">
                  <div class="input-group-prepend">
                     <label class="input-group-text" for="js-distribution-${category.name}" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang en='Team composing' fr='Composition de l\'équipe' de='Teamzusammenstellung' es='Composición en equipo'}}"><i class="fa-solid fa-user-group"></i></label>
                  </div>
                  <select class="custom-select form-control form-control-sm edit-if-cd" id="js-distribution-${category.name}">${this.formatOptionDistributionType(category)}</select>
               </div>
               <div class="col input-group">
                  <div class="input-group-prepend">
                     <label class="input-group-text" for="protection-type-${category.name}" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang en='Protection type' fr='Type de protection' de='Schutzart' es='Tipo de protección'}}"><i class="fa-solid fa-user-shield"></i></label>
                  </div>
                  <select class="custom-select form-control form-control-sm edit-if-cd" id="protection-type-${category.name}">${this.formatOptionProtectionType(category)}</select>
               </div>
            </div>

            <div class="row mt-3">
               <div style="margin-left:10px;">
                  <nav>
                     <div class="nav nav-tabs" role="tablist">
                        <a class="nav-link active" id="nav-qualification-tab-${category.name}" data-toggle="tab" data-target="#nav-qualification-${category.name}" type="button" role="tab" aria-controls="nav-qualification-${category.name}" aria-selected="true"><i class="fa-solid fa-user-large"></i> {{ lang en="Qualification" fr="Qualification" de="Qualifikation" es="Calificación" }}</a>
                        <a class="nav-link" id="nav-flyoff-tab-${category.name}" data-toggle="tab" data-target="#nav-flyoff-${category.name}" type="button" role="tab" aria-controls="nav-flyoff-${category.name}" aria-selected="false"><i class="fa-solid fa-user-graduate"></i> Flyoff</a>
                     </div>
                  </nav>
                  <div class="tab-content">
                     <div class="tab-pane fade show active" id="nav-qualification-${category.name}" role="tabpanel" aria-labelledby="nav-qualification-tab-${category.name}">
                        <div class="container border rounded" style="padding-top:1rem;margin-top: 1rem;">
                           <div class="row">
                              <div class="col">
                                 <button ${category.is_qualif_draw_set ? "disabled" : ""} id="new-qualif-draw-but-${category.name}" style="width:30;height:30;" class="btn btn-primary mr-1 mb-1" data-toggle="tooltip" data-placement="top" title="{{ lang en='Generate new draw' fr='Générer un nouveau tirage' de='Neue Auslosung generieren' es='Generar nuevo sorteo' }}">
                                    <i class="fas fa-sync-alt"></i>
                                 </button>
                                 <button ${category.is_qualif_draw_set ? "disabled" : ""} id="set-qualif-draw-but-${category.name}" style="width:30;height:30;" class="btn btn-primary mr-1 mb-1" data-toggle="tooltip" data-placement="top" title="{{ lang en='Save current draw' fr='Enregistrer le tirage actuel' de='Aktuelle Ziehung speichern' es='Guardar el sorteo actual' }}">
                                    <i class="far fa-save"></i>
                                 </button>
                                 <button ${category.is_qualif_draw_set ? "disabled" : ""} id="delete-qualif-draw-but-${category.name}" style="width:30;height:30;" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="{{ lang en='Delete current draw' fr='Détruire le tirage actuel' de='Aktuelle Ziehung Löschen ' es='Borrar el sorteo actual' }}">
                                    <i class="fa-solid fa-trash"></i>
                                 </button>
                              </div>
                              <div class="col">
                                 <table class="table table-borderless">
                                    <thead>
                                       <tr>
                                          <th scope="col" style="white-space: nowrap" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang 
                                             en='<u>CIAM 5.5.11.5.1</u><br> a) The competitor will be allowed a minimum of four (4) flights in the qualifying rounds.'
                                             fr='<u>CIAM 5.5.11.5.1</u><br> a) Le compétiteur aura droit à un minimum de quatre (4) vols dans les tours de qualification.'
                                             de='<u>CIAM 5.5.11.5.1</u><br> a) Dem Teilnehmer werden in den Qualifikationsrunden mindestens vier (4) Flüge gestattet.'
                                             es='<u>CIAM 5.5.11.5.1</u><br> a) Al competidor se le permitirá un mínimo de cuatro (4) vuelos en las Mangas clasificatorias.' }}">
                                             <i class="fas fa-circle-notch"></i>&nbsp;{{ lang en="Round" fr="Manche" de="Runde" es="Redondo" }}
                                          </th>
                                          <th scope="col" style="white-space: nowrap">
                                             <i class="fas fa-user-friends"></i>&nbsp;{{ lang en="Group" fr="Groupe" de="Gruppe" es="grupo" }}
                                          </th>
                                          <th scope="col" style="white-space: nowrap" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang 
                                             en='<u>CIAM 5.5.11.8.1</u><br> a) A minimum of 6 competitors should be scheduled for each Group'
                                             fr='<u>CIAM 5.5.11.8.1</u><br> a) Un minimum de 6 concurrents doit être programmé pour chaque groupe'
                                             de='<u>CIAM 5.5.11.8.1</u><br> a) Für jede Gruppe sollten mindestens 6 Teilnehmer geplant sein'
                                             es='<u>CIAM 5.5.11.8.1</u><br> a) Se debe programar un mínimo de 6 competidores para cada grupo.' }}">
                                             <i class="fas fa-bullseye"></i>&nbsp;{{ lang en="Lane" fr="Cible" de="Bahn" es="carril" }}
                                          </th>
                                          <th scope="col" style="white-space: nowrap">
                                             {{ lang en="Free lane" fr="Cible libre" de="Freie Bahn" es="Carril libre" }}
                                          </th>
                                          <th scope="col" style="white-space: nowrap" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang 
                                                en='The highest number, the better distribution of incidence matrix.'
                                                fr='Plus le nombre est élevé, meilleure est la distribution de la matrice d\'incidence.'
                                                de='Je höher die Zahl, desto besser die Verteilung der Inzidenzmatrix.'
                                                es='Cuanto mayor sea el número, mejor será la distribución de la matriz de incidencia.' }}">
                                             <i class="fa-solid fa-rotate-left"></i>&nbsp;{{ lang en="Iteration" fr="Itération" de="Durchlauf" es="Iteración" }}
                                          </th>
                                          <th scope="col" style="white-space: nowrap" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang 
                                                en='Distribution of free lanes per group<br><table><tr><td><b>#</b></td><td>&#8594;</td><td>One pilot</td></tr><tr><td><b>-</b></td><td> &#8594;</td><td>One free lane<td></table>'
                                                fr='Répartition des cibles libres par groupe<br><table><tr><td><b>#</b></td><td>&#8594;</td><td>Un pilote</td></tr><tr><td><b>-</b></td><td> &#8594;</td><td>Une cible libre<td></table>'
                                                de='Verteilung der freien Fahrspuren pro Gruppe<br><table><tr><td><b>#</b></td><td>&#8594;</td><td>One pilot</td></tr><tr><td><b>-</b></td><td> &#8594;</td><td>One free lane<td></table>'
                                                es='Distribución de carriles libres por grupo<br><table><tr><td><b>#</b></td><td>&#8594;</td><td>One pilot</td></tr><tr><td><b>-</b></td><td> &#8594;</td><td>One free lane<td></table>' }}">
                                             {{ lang en="Lane distributing template" fr="Modèle de distrib. de cible" de="Fahrspurverteiler-Vorlage" es="Plantilla de distrib. de carriles" }}
                                          </th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                          <td>
                                             <select ${category.is_qualif_draw_set ? "disabled" : ""} class="form-control form-control-sm edit-if-cd" id="contest-qualif-round-${category.name}">${this.formatOptionQualifRound(category)}</select>
                                          </td>
                                          <td>
                                             <select ${category.is_qualif_draw_set ? "disabled" : ""} class="form-control form-control-sm edit-if-cd" id="contest-qualif-group-${category.name}">${this.formatOptionQualifGroup(category)}</select>
                                          </td>
                                          <td>
                                             <select ${category.is_qualif_draw_set ? "disabled" : ""} class="form-control form-control-sm edit-if-cd" id="contest-qualif-slot-${category.name}">${this.formatOptionQualifSlot(category)}</select>
                                          </td>
                                          <td>
                                             <div style="text-align:center;"><span class="badge" id="contest-qualif-pilot-${category.name}"></span></div>
                                          </td>
                                          <td>
                                             <select ${category.is_qualif_draw_set ? "disabled" : ""} class="form-control form-control-sm edit-if-cd" name="loop" id="contest-qualif-loop-${category.name}">
                                                <option value="1">1</option>
                                                <option value="5">5</option>
                                                <option value="10">10</option>
                                                <option value="15">15</option>
                                                <option value="20">20</option>
                                             </select>
                                          </td>
                                          <td>
                                             <input ${category.is_qualif_draw_set ? "disabled" : ""} class="form-control input-mask" id="free-profile-input-${category.name}" type="text" style="text-align: center; width: 100%;"></input>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>

                              </div>
                           </div>

                           <div id="qualifFadeMsg" class="alert alert-success alert-dismissible fade show" style="display:none;margin-top:1em;">
                              <span id="qualifMsg"></span>
                           </div>

                           <div class="form-check" style="margin-top:1rem;">               
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input edit-if-cd" type="checkbox" id="merge-qualif-js-${category.name}" value="js" disabled>
                                 <label class="form-check-label" for="merge-qualif-js-${category.name}">{{ lang en="Separate senior and junior" fr="Séparer junior et senior" de="Trennen Sie Junior und Senior" es="Junior y senior separados" }}</label>
                              </div>

                              <div class="form-check form-check-inline">
                                 <input class="form-check-input edit-if-cd" type="checkbox" id="alt-qualif-js-${category.name}" value="js" disabled>
                                 <label class="form-check-label" for="alt-qualif-js-${category.name}">{{ lang en="Alternate junior senior" fr="Alterner junior et senior" de="alternativer Junior Senior" es="suplente junior senior" }}</label>
                              </div>               
                           </div>
                           <hr>
                           <nav>
                              <div class="nav nav-tabs" id="nav-qualif-tab" role="tablist">
                                 <a class="nav-link active" id="nav-qualif-draw-tab-${category.name}" data-toggle="tab" href="#nav-qualif-draw-${category.name}" role="tab" aria-controls="nav-qualif-draw" aria-selected="true"><i class="fa-solid fa-table-cells"></i>&nbsp;{{ lang en='Draw' fr='Tirage' de='Ziehung' es='Sorteo' }}</a>
                                 <a class="nav-link" id="nav-qualif-profile-tab-${category.name}" data-toggle="tab" href="#nav-qualif-profile-${category.name}" role="tab" aria-controls="nav-qualif-profile" aria-selected="false"><i class="fa-solid fa-handshake-simple"></i>&nbsp;{{ lang en="Meeting" fr="Rencontre" de="Treffen" es="Reuniones"}}</a>
                                 <a class="nav-link" id="nav-qualif-stats-tab-${category.name}" data-toggle="tab" href="#nav-qualif-stats-${category.name}" role="tab" aria-controls="nav-qualif-stats" aria-selected="false"><i class="fa-solid fa-chart-area"></i>&nbsp;{{ lang en="Statistics" fr="Statistiques" de="Statistik" es="Estadísticas"}}</a>
                              </div>
                           </nav>
                           <div class="tab-content">
                              <div class="tab-pane fade show active" id="nav-qualif-draw-${category.name}" role="tabpanel" aria-labelledby="nav-qualif-draw-tab-${category.name}">
                                 <div id="draw-qualif-table-${category.name}" style="overflow-y: scroll;height: 30rem;width: 100%;margin: 1rem;"></div>
                              </div>

                              <div class="tab-pane fade" id="nav-qualif-profile-${category.name}" role="tabpanel" aria-labelledby="nav-qualif-profile-tab-${category.name}">
                                 <div style="overflow-y: scroll;height: 30rem;width: 100%;margin: 1rem;">
                                    <table id="meetings-pilot-table-${category.name}" data-sortable="true" data-sort-name="meetings" data-sort-order="desc" data-pagination="true">
                                       <thead>
                                          <tr>
                                             <th data-field="pilot1" data-sortable="true">{{ lang en="Pilot 1" fr="Pilote 1" de="Pilot 1" es="piloto 1" }}</th>
                                             <th data-field="meetings" data-sortable="true">{{ lang en="Meets N times" fr="Rencontre N fois" de="Trifft N mal" es="Se reúne N veces" }}</th>
                                             <th data-field="pilot2" data-sortable="true">{{ lang en="Pilot 2" fr="Pilote 2" de="Pilot 2" es="piloto 2" }}</th>
                                          </tr>
                                       </thead>
                                    </table>
                                 </div>
                              </div>

                              <div class="tab-pane fade" id="nav-qualif-stats-${category.name}" role="tabpanel" aria-labelledby="nav-qualif-stats-tab-${category.name}">
                                 <div style="overflow-y: scroll;height: 30rem;width: 100%;margin: 1rem;">
                                    <table class="table table-striped">
                                       <thead>
                                          <tr>
                                             <td class="font-weight-bold">{{ lang en="Name" fr="Nom" de="Name" es="Nombre"}}</td>
                                             <td class="font-weight-bold">{{ lang en="Value" fr="Valeur" de="Wert" es="Valor"}}</td>
                                          </tr>
                                       </thead>
                                       <tbody id="draw-stats-table-${category.name}">
                                       </tbody>
                                    </table>
                                 </div>
                              </div>

                           </div>
                        </div>               
                     </div>
                     <div class="tab-pane fade" id="nav-flyoff-${category.name}" role="tabpanel" aria-labelledby="nav-flyoff-tab-${category.name}">                     
                        <div class="container border rounded" style="padding-top:1rem;margin-top: 1rem;">
                           <div class="row">
                              <div class="col">
                                 <button ${category.is_flyoff_draw_set ? "disabled" : ""} id="new-flyoff-draw-but-${category.name}" style="width:30;height:30;" class="btn btn-primary mr-1 mb-1" data-toggle="tooltip" data-placement="top" title="{{ lang en='Generate new draw' fr='Générer un nouveau tirage' de='Neue Auslosung generieren' es='Generar nuevo sorteo' }}">
                                    <i class="fas fa-sync-alt"></i>
                                 </button>
                                 <button ${category.is_flyoff_draw_set ? "disabled" : ""} id="set-flyoff-draw-but-${category.name}" style="width:30;height:30;" class="btn btn-primary mr-1 mb-1" data-toggle="tooltip" data-placement="top" title="{{ lang en='Save current draw' fr='Enregistrer le tirage actuel' de='Aktuelle Ziehung speichern' es='Guardar el sorteo actual' }}">
                                    <i class="far fa-save"></i>
                                 </button>
                                 <button ${category.is_flyoff_draw_set ? "disabled" : ""} id="delete-flyoff-draw-but-${category.name}" style="width:30;height:30;" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="{{ lang en='Delete current draw' fr='Détruire le tirage actuel' de='Aktuelle Ziehung Löschen ' es='Borrar el sorteo actual' }}">
                                    <i class="fa-solid fa-trash"></i>
                                 </button>
                              </div>
                              <div class="col-10">
                                 <table class="table table-borderless">
                                    <thead>
                                       <tr>
                                          <th scope="col" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang en='<u>CIAM rule 5.5.11.13.d</u>: A minimum of 3 or maximum of 4 fly-off rounds should be flown. Exceptionally the CD may reduce to 2 in the case of bad weather or poor visibility'
                                          fr='<u>Règle CIAM 5.5.11.13.d</u>: Un minimum de 3 ou un maximum de 4 manches doivent être effectués. Exceptionnellement, le DC peut réduire à 2 manches en cas de mauvais temps ou de mauvaise visibilité'
                                          de='<u>CIAM Regel 5.5.11.13.d</u>: Es sollten mindestens 3 oder maximal 4 Abflugrunden geflogen werden. Ausnahmsweise kann sich die CD bei schlechtem Wetter oder schlechter Sicht auf 2 reduzieren'
                                          es='<u>Norma CIAM 5.5.11.13.d</u>: Se debe volar un mínimo de 3 o un máximo de 4 Mangas de vuelo. Excepcionalmente, el CD puede reducirse a 2 en caso de mal tiempo o mala visibilidad.' }}"><i class="fas fa-circle-notch"></i>&nbsp;{{ lang en="Round" fr="Manche" de="Runde" es="Redondo" }}
                                          </th>
                                          <th scope="col" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang en='<u>CIAM rule 5.5.11.13.c</u>: At the end of the qualifying rounds 30% of competitors with the highest aggregate scores will be placed together in a single Group comprising a minimum of 6 and maximum of 14 for the fly-off rounds. For operational reasons the CD may set a lower maximum'
                                          fr='<u>Règle CIAM 5.5.11.13.c</u>: À la fin des manches de qualification, 30% des concurrents avec les scores cumulés les plus élevés seront placés ensemble dans un seul groupe comprenant un minimum de 6 et un maximum de 14 pour les manches de fly-off. Pour des raisons opérationnelles, le CD peut fixer un maximum plus petit'
                                          de='<u>CIAM Regel 5.5.11.13.c</u>: Am Ende der Qualifikationsrunden werden 30% der Teilnehmer mit den höchsten Gesamtpunktzahlen in einer einzigen Gruppe zusammengefasst, die mindestens 6 und höchstens 14 für die Fly-Off-Runden umfasst. Aus betrieblichen Gründen kann die CD ein niedrigeres Maximum einstellen'
                                          es='<u>Norma CIAM 5.5.11.13.c</u>: Al final de las Mangas de clasificación, el 30% de los competidores con las puntuaciones totales más altas se colocarán juntos en un solo grupo que comprende un mínimo de 6 y un máximo de 14 para las Mangas de salida. Por razones operativas, el CD puede establecer un máximo más bajo' }}"><i class="fas fa-bullseye"></i>&nbsp;{{ lang en="Lane" fr="Cible" de="Bahn" es="carril" }}
                                          </th>
                                          <th scope="col" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang en='<u>CIAM rule 5.5.11.13.c</u>: Competitors will be placed together in a single Group'
                                          fr='<u>Règle CIAM 5.5.11.13.c</u>: Les concurrents seront placés ensemble dans un seul groupe '
                                          de='<u>CIAM Regel 5.5.11.13.c</u>: Die Teilnehmer werden in einer einzigen Gruppe zusammengefasst'
                                          es='<u>Norma CIAM 5.5.11.13.c</u>: Los competidores se colocarán juntos en un solo grupo' }}"><i class="fas fa-user-friends"></i>&nbsp;{{ lang en="Group" fr="Groupe" de="Gruppe" es="grupo" }}
                                             </span>
                                          </th>
                                          <th>
                                             <div style="text-align:center;" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang en='<u>CIAM rule 5.5.11.13.c</u>: 30% of competitors with the highest aggregate scores'
                                          fr='<u>Règle CIAM 5.5.11.13.c</u>: 30% des concurrents avec les scores cumulés les plus élevés '
                                          de='<u>CIAM Regel 5.5.11.13.c</u>: 30% der Wettbewerber mit den höchsten Gesamtwerten'
                                          es='<u>Norma CIAM 5.5.11.13.c</u>: 30% de competidores con las puntuaciones totales más altas' }}">&nbsp;&nbsp;&nbsp;{{ lang en="Competitors ratio" fr="Ratio concurrents" de="Wettbewerbsverhältnis" es="Razón de competidores" }}
                                             </div>
                                          </th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                          <td>
                                             <select ${category.is_flyoff_draw_set ? "disabled" : ""} class="form-control form-control-sm edit-if-cd" id="contest-flyoff-round-${category.name}">${this.formatOptionFlyoffRound(category)}</select>
                                          </td>
                                          <td>
                                             <select ${category.is_flyoff_draw_set ? "disabled" : ""} class="form-control form-control-sm edit-if-cd" id="contest-flyoff-slot-${category.name}">${this.formatOptionFlyoffSlot(category)}</select>
                                          </td>
                                          <td>
                                             <select ${category.is_flyoff_draw_set ? "disabled" : ""} class="form-control form-control-sm edit-if-cd" id="contest-flyoff-group-${category.name}">${this.formatOptionFlyoffGroup(category)}</select>
                                          </td>
                                          <td>
                                             <div style="text-align:center;"><span class="badge" id="contest-pilot-ratio-${category.name}"></span></div>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
            
                           <div id="flyoffFadeMsg" class="alert alert-success alert-dismissible fade show" style="display:none;margin-top:1em;">
                              <span id="flyoffMsg"></span>
                           </div>
               
                           <hr>
                           <nav>
                              <div class="nav nav-tabs" role="tablist">
                                 <a class="nav-link active" id="nav-flyoff-topqual-tab" data-toggle="tab" href="#nav-flyoff-topqual-${category.name}" role="tab" aria-controls="nav-flyoff-topqual" aria-selected="false"><i class="fa-solid fa-ranking-star"></i>&nbsp;{{ lang en="Qualification ranking" fr="Classement des qualifications" de="Rangfolge der Qualifikationen" es="Clasificación de las calificaciones"}}</a>
                                 <a class="nav-link" id="nav-flyoff-draw-tab" data-toggle="tab" href="#nav-flyoff-draw-${category.name}" role="tab" aria-controls="nav-flyoff-draw" aria-selected="true"><i class="fa-solid fa-table-cells"></i>&nbsp;{{ lang en='Draw' fr='Tirage' de='Ziehung' es='Sorteo' }}</a>
                              </div>
                           </nav>
                           <div class="tab-content">
                              <div class="tab-pane fade show active" id="nav-flyoff-topqual-${category.name}" role="tabpanel" aria-labelledby="nav-flyoff-topqual-tab">
                                 <div style="overflow-y: scroll;height: 30rem;width: 100%;margin: 1rem;">
                                    <table class="table table-borderless">
                                       <thead>
                                          <tr>
                                             <th scope="col">#</th>
                                             <th scope="col">{{ lang en="First name" fr="Prénom" de="Vornamen" es="Nombre"}}</th>
                                             <th scope="col">{{ lang en="Last name" fr="Nom" de="Name" es="Apellido"}}</th>
                                             <th scope="col">{{ lang en="Score" fr="Score" de="Ergebnis" es="Puntuación"}}</th>
                                          </tr>
                                       </thead>
                                       <tbody id="draw-flyoff-top-table">
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              <div class="tab-pane fade show" id="nav-flyoff-draw-${category.name}" role="tabpanel" aria-labelledby="nav-flyoff-draw-tab">
                                 <div id="draw-flyoff-table-${category.name}" style="overflow-y: scroll;height: 30rem;width: 100%;margin: 1rem;">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>


               <!--
               <div class="col">
                  <div class="form-check" style="margin-left:10px;">
                     <input class="form-check-input" type="radio" name="qualifFlyoffActive" id="qualifActive" value="qa" onchange="onQualifFlyoffActiveChange(false)">
                     <label class="form-check-label" for="qualifActive">{{ lang en="Active" fr="Active" de="Aktiv" es="Active"}}</label>
                  </div>

                  <div class="input-group">
                     <button id="qualifCollapseButton" class="col btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseQualif" aria-expanded="false" aria-controls="collapseQualif">
                        {{ lang en="Qualification" fr="Qualification" de="Qualifikation" es="Calificación" }} <i class="fas fa-caret-down"></i>
                     </button>
                  </div>
               </div>

               <div class="col">
                  <div class="form-check" style="margin-left:10px;">
                     <input class="form-check-input" type="radio" name="qualifFlyoffActive" id="flyoffActive" value="fa" onchange="onQualifFlyoffActiveChange(true)">
                     <label class="form-check-label" for="flyoffActive">{{ lang en="Active" fr="Active" de="Aktiv" es="Active"}}</label>
                  </div>
                  <div class="col input-group">
                     <button id="flyoffCollapseBut" class="col btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseFlyoff" aria-expanded="false" aria-controls="collapseFlyoff">
                        Fly-off <i class="fas fa-caret-down"></i>
                     </button>
                  </div>
               </div>-->
            </div>

            <div class="collapse" id="collapseQualif">
            </div>
         
            <div class="collapse" id="collapseFlyoff">
            </div>`;

         this.#tabContentParent_.appendChild(tabContent);
      }

      let pthis = this; // parent this
      for (let category of this.#categories_) {
         this.addElement(`draw-type-${category.name}`, category.draw_type, function () { pthis.onSelectChange(category, "dt", this.value); });
         this.addElement(`protection-type-${category.name}`, category.protection_type, function () { pthis.onSelectChange(category, "pt", this.value); });
         this.addElement(`contest-qualif-loop-${category.name}`, category.draw_loop, function () { pthis.onSelectChange(category, "lo", this.value); });


         document.getElementById(`new-qualif-draw-but-${category.name}`).onclick = function () { pthis.generateDrawContest(category, false, false); };
         document.getElementById(`set-qualif-draw-but-${category.name}`).onclick = function () { pthis.saveDrawContest(category, false, false); };
         document.getElementById(`delete-qualif-draw-but-${category.name}`).onclick = function () { pthis.askDeleteDrawContest(category, false, false); };

         document.getElementById(`new-flyoff-draw-but-${category.name}`).onclick = function () { pthis.generateDrawContest(category, true, false); };
         document.getElementById(`set-flyoff-draw-but-${category.name}`).onclick = function () { pthis.saveDrawContest(category, true, false); };
         document.getElementById(`delete-flyoff-draw-but-${category.name}`).onclick = function () { pthis.askDeleteDrawContest(category, true, false); };

         document.getElementById(`contest-qualif-round-${category.name}`).onchange = function () { pthis.onSelectChange(category, "qr", this.value); };
         document.getElementById(`contest-qualif-group-${category.name}`).onchange = function () { pthis.onSelectChange(category, "qg", this.value); };
         document.getElementById(`contest-qualif-slot-${category.name}`).onchange = function () { pthis.onSelectChange(category, "qs", this.value); };
         document.getElementById(`contest-qualif-pilot-${category.name}`).onchange = function () { pthis.onSelectChange(category, "pi", this.value); };

         this.updateUI(category);
      }

      this.active_ = true;
   }

   onSelectChange(category, select, value) {
      if ((select == "qr")) {
         category.qualif_round = value;   
         this.modifyContest(category.id, {qualif_round: value}, () => this.updateAllDrawContest(category.is_flyoff, category.is_junior));
         //this.updateUI(category);
      } else if ((select == "qg")) {
         category.qualif_group = value;
         this.updateUI(category);
      } else if ((select == "dt") || (select == "pt")) {
         this.updateUI(category);
      }
   }

   formatOptionDrawType(category) {
      let html = "";
      html += `<option value="random" ${(category.draw_type == "random") ? "selected" : ""}>{{ lang en="Random" fr="Aléatoire" de="Zufällig" es="Aleatorio"}}</option>`;
      html += `<option value="mpilot" ${(category.draw_type == "mpilot") ? "selected" : ""}>{{ lang en="Minimize same pilot" fr="Minimiser même pilote" de="Minimieren Sie denselben Piloten" es="Minimizar el mismo piloto"}}</option>`;
      html += `<option value="morlot" ${(category.draw_type == "morlot") ? "selected" : ""}>Algo Michel Morlot</option>`;
      html += `<option value="mslot" disabled>{{ lang en="Minimize same lane" fr="Minimiser la même cible" de="Gleiche Fahrspur minimieren" es="Minimizar mismo carril"}}</option>`;
      return html;
   }

   formatOptionRuleType(category) {
      let html = "";
      html += `<option value="fai" ${(category.rule_type == "fai") ? "selected" : ""}>FAI</option>`;
      html += `<option value="free" ${(category.rule_type == "free") ? "selected" : ""}>{{ lang en="Free" fr="Libre" de="Frei" es="Libre"}}</option>`;
      return html;
   }

   formatOptionDistributionType(category) {
      let html = "";
      html += `<option value="sjall" ${(category.is_junior_senior_merged) ? "selected" : ""}>{{ lang en="Senior & Junior together" fr="Senior & Junior ensemble" de="Senior & Junior zusammen" es="Senior y Junior juntos"}}</option>`;
      html += `<option value="sjsepalt" disabled>{{ lang en="Senior & Junior separated" fr="Senior & Junior séparés" de="Senior & Junior getrennt" es="Sénior y Júnior separados"}}</option>`;
      return html;
   }

   formatOptionProtectionType(category) {
      let html = "";
      html += `<option value="none" ${(category.protection_type == "none") ? "selected" : ""}>{{ lang en="None" fr="Aucune" de="Keiner" es="Ninguno"}}</option>`;
      html += `<option value="team" ${(category.protection_type == "team") ? "selected" : ""}>{{ lang en="Team (1 Senior &#8594; N Junior)" fr="Par équipe (1 Sénior &#8594; N Junior)" de="Nach Mannschaft (1 Senior &#8594; N Junior)" es="Por equipo (1 Sénior &#8594; N Júnior)"}}</option>`;
      html += `<option value="country" disabled>{{ lang en="by country" fr="par pays" de="nach Land" es="por país"}}</option>`;
      return html;
   }

   formatOptionQualifRound(category) {
      let html = "";
      let isFaiRules = (category.rule_type == "fai");
      for (let i = (isFaiRules ? category.official_qualif_min_round : 1); i <= category.official_qualif_max_round; i++)
         html += `<option value="${i}" ${(category.qualif_round == i) ? "selected" : ""}>${i}</option>`;
      return html;
   }

   formatOptionQualifSlot(category) {
      let html = "";
      let isFaiRules = (category.rule_type == "fai");
      for (let i = (isFaiRules ? category.official_qualif_min_slot : 1); i <= category.official_qualif_max_slot; i++)
         html += `<option value="${i}" ${(category.qualif_slot == i) ? "selected" : ""}>${i}</option>`;
      return html;
   }

   formatOptionQualifGroup(category) {
      let html = "";
      let isFaiRules = (category.rule_type == "fai");
      for (let i = (isFaiRules ? category.official_qualif_min_group : 1); i <= category.official_qualif_max_group; i++)
         html += `<option value="${i}" ${(category.qualif_group == i) ? "selected" : ""}>${i}</option>`;
      return html;
   }

   formatOptionFlyoffRound(category) {
      let html = "";
      let isFaiRules = (category.rule_type == "fai");
      for (let i = (isFaiRules ? category.official_flyoff_min_round : 1); i <= category.official_flyoff_max_round; i++)
         html += `<option value="${i}" ${(category.flyoff_round == i) ? "selected" : ""}>${i}</option>`;
      return html;
   }

   formatOptionFlyoffSlot(category) {
      let html = "";
      let isFaiRules = (category.rule_type == "fai");
      for (let i = (isFaiRules ? category.official_flyoff_min_slot : 1); i <= category.official_flyoff_max_slot; i++)
         html += `<option value="${i}" ${(category.flyoff_slot == i) ? "selected" : ""}>${i}</option>`;
      return html;
   }

   formatOptionFlyoffGroup(category) {
      let html = "";
      let isFaiRules = (category.rule_type == "fai");
      for (let i = (isFaiRules ? category.official_flyoff_min_group : 1); i <= category.official_flyoff_max_group; i++)
         html += `<option value="${i}" ${(category.flyoff_group == i) ? "selected" : ""}>${i}</option>`;
      return html;
   }

   async generateDrawContest(category, isFlyoff, isJunior) {
      await this.updateUI(category);
   }

   async saveDrawContest(category, isFlyoff, isJunior) {
      console.log(isFlyoff, isJunior, category);
      await fetch(`/api/v1/setDrawContest?contest_id=${this.#contestID_}&is_flyoff=${isFlyoff}&is_junior=${isJunior}`, {
         headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
         },
         method: "POST",
         body: JSON.stringify(curPilotMatrix)
      })
         .then(handleErrors)
         .then(function (response) {
            displayAlert({
               type: "success",
               msg: '{{ lang en="Round draw is saved for qualification" fr="Le tirage au sort est enregistré pour la qualification" de="Die Auslosung wird für die Qualifikation gespeichert" es="El sorteo de la Manga se guarda para la clasificación" }}',
               divElt: (!isFlyoff) ? $("#qualifFadeMsg") : $("#flyoffFadeMsg"),
               spanElt: (!isFlyoff) ? $("#qualifMsg") : $("#flyoffMsg"),
               callbackOnHide: function () {
                  $("#newQualifDrawBut").prop("disabled", true);
                  $("#setQualifDrawBut").prop("disabled", true);
                  $("deleteQualifDrawBuf").prop("disabled", true);
                  $("#contestQualifSlot").prop("disabled", true);
                  $("#contestQualifGroup").prop("disabled", true);
                  $("#contestQualifRound").prop("disabled", true);
               },
               autoHide: true
            });
         })
         .catch(function (error) {
            displayAlert({
               type: "error",
               msg: `(${error.type}) ${error.message}`,
               divElt: (!isFlyoff) ? $("#qualifFadeMsg") : $("#flyoffFadeMsg"),
               spanElt: (!isFlyoff) ? $("#qualifMsg") : $("#flyoffMsg"),
               autoHide: true,
               timeout: 4000
            });
         });
   }

   async askDeleteDrawContest(category, isFlyoff, isJunior) {
      console.log(isFlyoff, isJunior, category);
   }

   #dmVisitor(dm, func) {
      for (let i = 0; i < dm.length; i++) {
         for (let j = (i + 1); j < dm.length; j++) {
            func(i, j, dm[i][j]);
         }
      }
   }

   #getPilot(pilotID) {
      for (const pilot of this.#pilotTab_) {
         if (pilot.id === pilotID)
            return pilot;
      }
      return undefined;
   }

   #getPilotIndex(pilotID) {
      for (let idx = 0; idx < this.#pilotTab_.length; idx++) {
         if (this.#pilotTab_[idx].id === pilotID)
            return idx;
      }
      return undefined;
   }

   updateMeetingsPilotTableUI(category, data) {
      let incidenceMatrix = [];
      const size = this.#pilotTab_.length;
      for (let i = 0; i < size; i++) {
         let row = [];
         for (let j = 0; j < size; j++)
            row.push(0);
         incidenceMatrix.push(row);
      }

      for (const round of data.matrix) {
         for (const group of round) {
            for (const pilot1 of group) {
               for (const pilot2 of group) {
                  if (pilot1.id === pilot2.id)
                     continue;
                  if (!pilot1.is_free_type && !pilot2.is_free_type) {
                     let p1Idx = this.#getPilotIndex(pilot1.id);
                     let p2Idx = this.#getPilotIndex(pilot2.id);
                     if ((p1Idx != undefined) && (p2Idx != undefined)) // check if not remove pilot after draw matrix
                        incidenceMatrix[p1Idx][p2Idx]++;
                  }
               }
            }
         }
      }

      let min = 1000;
      this.#dmVisitor(incidenceMatrix, function (i, j, v) {
         if (min > v)
            min = v;
      });
      let max = -1;
      this.#dmVisitor(incidenceMatrix, function (i, j, v) {
         if (max < v)
            max = v;
      });

      let list = [];
      for (let level = min; level <= max; level++) {
         let pthis = this;
         this.#dmVisitor(incidenceMatrix, function (i, j, v) {
            if (level === v) {
               let p1 = pthis.#pilotTab_[i];
               let p2 = pthis.#pilotTab_[j];
               list.push({
                  pilot1: `${p1.first_name} ${p1.last_name}`,
                  meetings: v,
                  pilot2: `${p2.first_name} ${p2.last_name}`
               });
            }
         });
      }

      if (data.statistic) {
         let stats = "";
         for (const [key, value] of Object.entries(data.statistic)) {
            if (typeof value === 'object')
               stats += `<tr><td>${key}</td><td>${JSON.stringify(value)}</td></tr>`;
            else
               stats += `<tr><td>${key}</td><td>${value}</td></tr>`;
         }
         document.getElementById(`draw-stats-table-${category.name}`).innerHTML = stats;
      }

      let table = $(`#meetings-pilot-table-${category.name}`);
      table.bootstrapTable("destroy");
      table.bootstrapTable({ data: list });
   }

   updateAllDrawContest(isFlyoff, isJunior) {

   }
   updatePilotMatrixUI(category, isFlyoff, isJunior, result) {
      let content = "<div>";
      let roundNum = 1;
      let curPilotMatrix = result.matrix;

      if (curPilotMatrix.length != 0) {


         let teamColor = new Map();
         for (const round of curPilotMatrix) {
            for (const group of round) {
               for (const item of group) {
                  teamColor[item.team] = getNextColor();
               }
            }
         }

         for (const round of curPilotMatrix) {
            let groupNum = 1;
            content += '<div class="row">';
            for (const group of round) {
               let target = 1;
               content += `<div class="col" style="margin-top:1rem;${(groupNum % 2) ? "background-color:#f8f8f8" : ""}">
                        <span><b>{{ lang en="Ro" fr="Ma" de="Ru" es="Re" }}${roundNum}</b>&nbsp;<u>Gr${groupNum}</u></span>
                        <br><table style="margin-left:0.5rem;" border="0">`;

               for (const item of group) {
                  if (item.is_free_type == false) {
                     let teamContent = "";
                     if (item.team) {
                        teamContent = `&nbsp;<span style="color:${teamColor[item.team]};"><i class="fa-solid fa-shield-halved" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang en='Team' fr='Equipe' de='Team' es='Equipo' }} ${item.team}"></i></span>`;
                     }
                     content += `<tr>
                              <td class="draw-slot-td">${target}</td>
                              <td class="draw-pilot-td font-weight-light">${item.first_name[0]}.${item.last_name}${teamContent}</td>
                              </tr>`;
                  } else
                     content += `<tr><td class="draw-slot-td">${target}</td><td class="editable-cell draw-pilot-td"><i>{{ lang en="free" fr="Libre" de="Leer" es="Vacío" }}</i></td></tr>`;
                  target++;
               }
               content += '</table></div>';
               groupNum++;
            }
            content += '</div>';
            roundNum++;
         }
         content += '</div>';
      } else {
         content = '{{ lang en="Qualification ranking is empty" fr="Le classement des qualifications est vide" de="Qualifikationsranking ist leer" es="La clasificación está vacía"}}';
      }

      let id = `draw-${isFlyoff ? "flyoff" : "qualif"}-table-${category.name}`;
      //console.log(id, isFlyoff, isJunior);
      document.getElementById(id).innerHTML = content;
   }

   updateUI(category) {
      this.updateFreeProfileUI(category);
      this.updateDrawMatrixUI(category);
   }

   async updateDrawMatrixUI(category) {
      this.updateDrawUI(category, false, false, !category.is_qualif_draw_set);
      this.updateDrawUI(category, true, false, !category.is_flyoff_draw_set);
   }

   async haveEnoughBestQualifPilot(category, isJunior) {
      let bestPilotList = [];
      await fetch(`/api/v1/getBestPilotList?contest_id=${this.contestID_}&category_id=${category.id}&is_flyoff=false&is_junior=${isJunior}`)
         .then(handleErrors)
         .then(result => {
            bestPilotList = result;
         })
         .catch(function (error) {
            displayErrorMsg(error);
         });

      return (bestPilotList.length != 0);
   }

   async updateDrawUI(category, isFlyoff, isJunior, generate) {
      let url = `/api/v1/getDrawContest?contest_id=${this.#contestID_}&is_flyoff=${isFlyoff}&is_junior=${isJunior}&category_id=${category.id}`;
      if (generate) {
         const drawTypeID = `draw-type-${category.name}`;
         const protectionTypeID = `protection-type-${category.name}`;
         const drawLoop = `contest-qualif-loop-${category.name}`;
         url += "&generate";
         url += `&draw_type=${this.getValue(drawTypeID)}`;
         url += `&protection_type=${this.getValue(protectionTypeID)}`;
         url += `&loop=${this.getValue(drawLoop)}`;
         url += `&free_mask=${this.#freeProfileInputMask_.value.replace("#", "$")}`;

         if (isFlyoff) {
            if (!await this.haveEnoughBestQualifPilot(category, isJunior)) {
               let content = `<br><br><br><br><br><br><br><div style="transform: rotate(-45deg);text-align:center;color:#C0C0C0";height:100%;position:fixed;><h1>{{ lang en="No ranking yet" fr="Pas encore<br>de classement" de="Noch kein Ranking" es="Aún no hay clasificación"}}</h1></div>`;
               let elt = document.getElementById(`draw-flyoff-table-${category.name}`);
               elt.innerHTML = content;
               return;
            }
         }
      }

      await fetch(url)
         .then(handleErrors)
         .then(result => {
            this.updatePilotMatrixUI(category, isFlyoff, isJunior, result);
            this.updateMeetingsPilotTableUI(category, result);
         });
   }

   updateFreeProfileUI(category) {
      let maskStr = "";
      let first = true;
      for (let i = 0; i < category.qualif_group; i++) {
         if (first)
            first = false;
         else
            maskStr += "-";

         maskStr += "0";
      }

      let options = {
         mask: maskStr,
         lazy: false,
         placeholderChar: "#",
      };

      if (this.#freeProfileInputMask_ != undefined)
         this.#freeProfileInputMask_.destroy();

      let idx = `free-profile-input-${category.name}`;
      this.#freeProfileInputMask_ = IMask(document.getElementById(idx), options);
      this.#maskMap_.set(idx, this.#freeProfileInputMask_);
   }

   /*
   async onModifyContest(isFlyoff, isJunior) {
      let is_junior_senior_merged = true;
      let drawType = $("#jsDistribution").val();
      if (drawType !== "jsall")
         is_junior_senior_merged = false;

      let data = {
         qualif_slot: $("#contestQualifSlot").val(),
         qualif_round: $("#contestQualifRound").val(),
         qualif_group: $("#contestQualifGroup").val(),
         flyoff_slot: $("#contestFlyoffSlot").val(),
         flyoff_round: $("#contestFlyoffRound").val(),
         flyoff_group: $("#contestFlyoffGroup").val(),
         rule_type: $("#applicableRules").val(),
         draw_type: $("#drawType").val(),
         protection_type: $("#protectionType").val(),
         is_junior_senior_merged: is_junior_senior_merged
      };

      this.modifyContest(data, () => this.updateAllDrawContest(isFlyoff, isJunior));
   }*/

   async modifyContest(categoryID, data, callback) {
      await fetch(`/api/v1/modifyContest?contest_id=${this.#contestID_}&category_id=${categoryID}`, {
         headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
         },
         method: "POST",
         body: JSON.stringify(data)
      })
         .then(handleErrors)
         .then(function () {
            if (callback)
               callback();
         })
         .catch(function (error) {
            displayErrorMsg(error);
         });
   }

   currentCategoryName_;  // f5j,f3l....
   #contestID_;
   #tabContentParent_;
   #categories_ = [];
   #pilotTab_ = [];
   #maskMap_ = new Map();
   #freeProfileInputMask_;
}
