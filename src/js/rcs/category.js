/*
 * Copyright (c) 2019-2024 Thierry Wilmot
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

class RCSCategorySettings extends RCSElementBase {

    constructor(params) {
        super(params);

        this.#contestID_ = params.contestID;
    }

    init(params) {
        this.#categories_ = params.categories;
        if (this.#categories_.length == 2) {
            //if (categoryNames.includes("f3l") && categoryNames.includes("f5l"))
                this.currentCategoryName_ = "f35l";
        }

        let html = "";
        for (const category of this.#categories_) {
            html += `
                  <table class="table table-borderless" name="${category.name}" fai_cat="${category.name}"
                     <tr>
                        <th rowspan="2" style="padding:0;font-size:1.5rem;width:0rem;writing-mode:vertical-rl;text-orientation:mixed;vertical-align:middle;text-align:center;" data-toggle="tooltip" data-html="true" 
                            title="{{ lang en='Suggested setting to limit registration, may be reviewed at time of print run' 
                            fr='Proposition de réglage pour limiter l\'inscription, peut être revu lors du tirage' 
                            de='Vorschlag für eine Einstellung, um die Anmeldung zu begrenzen, kann bei der Ziehung überprüft werden' 
                            es='Ajuste sugerido para limitar el registro, puede revisarse durante la tirada.'}}">${category.name.toUpperCase()}</th>
                        <th scope="col" style="white-space: nowrap" data-toggle="tooltip" data-html="true" title="{{ lang 
                           en='Rules applied during the contest. Warning, FAI rules will restrict number of round, lane...'
                           fr='Règles appliquées pendant le concours. Attention, les règles FAI limitent le nombre de manches, de cibles, groupes...'
                           de='Während des Wettbewerbs geltende Regeln. Achtung, die FAI-Regeln beschränken die Anzahl der Runden,...'
                           es='Reglas aplicadas durante el concurso. Atención, las reglas FAI restringirán el número de rondas, carriles...' }}">
                           <i class="fa-solid fa-scale-balanced"></i>&nbsp;{{ lang en="Rules" fr="Règles" de="Regeln" es="Reglas" }}
                        </th>
                        <th scope="col" style="white-space: nowrap" data-toggle="tooltip" data-html="true" title="${category.official_qualif_round_rule}">
                           <i class="fas fa-circle-notch"></i>&nbsp;{{ lang en="Round" fr="Manche" de="Runde" es="Redondo" }}
                        </th>
                        <th scope="col" style="white-space: nowrap" data-toggle="tooltip" data-html="true" title="${category.official_qualif_group_rule}">
                           <i class="fas fa-user-friends"></i>&nbsp;{{ lang en="Group" fr="Groupe" de="Gruppe" es="grupo" }}
                        </th>
                        <th scope="col" style="white-space: nowrap" data-toggle="tooltip" data-html="true" title="${category.official_qualif_slot_rule}">
                            <i class="fas fa-bullseye"></i>&nbsp;{{ lang en="Lane" fr="Cible" de="Bahn" es="carril" }}
                        </th>
                        <th scope="col" style="white-space: nowrap;text-align: center;">
                           {{ lang en="Max pilot" fr="Max pilote" de="Max. Zahl Piloten" es="max piloto" }}
                        </th>
                        <th scope="col" style="white-space: nowrap" data-toggle="tooltip" data-html="true" title="${category.official_flyoff_slot_rule}">
                            <i class="fas fa-bullseye"></i>&nbsp;Flyoff
                        </th>
                     </tr>
                     <tr>
                        <td>
                           <select class="form-control form-control-sm edit-if-cd" id="contest-rule-type-${category.name}">
                              <option value="fai" ${this.optionIsRuleType(category, 'fai')}>FAI</option>
                              <option value="free" ${this.optionIsRuleType(category, 'free')}>Libre</option>
                           </select>
                        </td>
                        <td>
                           <select class="form-control form-control-sm edit-if-cd" id="contest-qualif-round-${category.name}">${this.formatOptionQualifRound(category)}</select>
                        </td>
                        <td>
                           <select class="form-control form-control-sm edit-if-cd" id="contest-qualif-group-${category.name}">${this.formatOptionQualifGroup(category)}</select>
                        </td>
                        <td>
                           <select class="form-control form-control-sm edit-if-cd" id="contest-qualif-slot-${category.name}">${this.formatOptionQualifSlot(category)}</select>
                        </td>
                        <td>
                           <div style="text-align:center;"><span id="contest-max-entrant-${category.name}">${category.qualif_slot * category.qualif_group}</span></div>
                        </td>
                        <td>
                           <select class="form-control form-control-sm edit-if-cd" id="contest-flyoff-slot-${category.name}">${this.formatOptionFlyoffSlot(category)}</select>
                        </td>
                     </tr>
                  </table>`;
        }

        this.parentRef_.innerHTML = html;

        let pthis = this; // parent this
        for (let category of this.#categories_) {
            document.getElementById(`contest-rule-type-${category.name}`).onchange = function () { pthis.onSelectChange(category, "rule", this.value); };
            document.getElementById(`contest-qualif-round-${category.name}`).onchange = function () { category.qualif_round = this.value; pthis.onSelectChange(category, "qr", this.value); };
            document.getElementById(`contest-qualif-group-${category.name}`).onchange = function () { category.qualif_group = this.value; pthis.onSelectChange(category, "qg", this.value); };
            document.getElementById(`contest-qualif-slot-${category.name}`).onchange = function () { category.qualif_slot = this.value; pthis.onSelectChange(category, "qs", this.value); };
            document.getElementById(`contest-flyoff-slot-${category.name}`).onchange = function () { pthis.onSelectChange(category, "fs", this.value); };

            this.addElement(`contest-max-entrant-${category.name}`, category.qualif_slot * category.qualif_group);
        }

        this.active_ = true;
    }

    onSelectChange(category, select, value) {
        if(select === "qr")
        {
            // do nothing
        }
        else if (select === "qs" || select === "qg") {
            let maxEntrant = category.qualif_slot * category.qualif_group;
            this.setValue(`contest-max-entrant-${category.name}`, maxEntrant);
            this.render();
        }
        else
            console.log(`Not yet implemented ${category} ${select} ${value}`);
    }

    formatOptionQualifRound(category) {
        let html = "";
        let isFaiRules = (category.rule_type == "fai");
        for (let i = (isFaiRules ? category.official_qualif_min_round : 1); i < category.official_qualif_max_round; i++)
            html += `<option value="${i}" ${(category.qualif_round == i) ? "selected" : ""}>${i}</option>`;
        return html;
    }

    formatOptionQualifSlot(category) {
        let html = "";
        let isFaiRules = (category.rule_type == "fai");
        for (let i = (isFaiRules ? category.official_qualif_min_slot : 1); i < category.official_qualif_max_slot; i++)
            html += `<option value="${i}" ${(category.qualif_slot == i) ? "selected" : ""}>${i}</option>`;
        return html;
    }

    formatOptionQualifGroup(category) {
        let html = "";
        let isFaiRules = (category.rule_type == "fai");
        for (let i = (isFaiRules ? category.official_qualif_min_group : 1); i < category.official_qualif_max_group; i++)
            html += `<option value="${i}" ${(category.qualif_group == i) ? "selected" : ""}>${i}</option>`;
        return html;
    }

    formatOptionFlyoffSlot(category) {
        let html = "";
        for (let i = 0; i < 7; i++)
            html += `<option value="${i}" ${(category.flyoff_slot == i) ? "selected" : ""}>${i}</option>`;
        return html;
    }

    optionIsRuleType(category, rules) {
        return category.rule_type == rules ? "selected" : "";
    }

    async onCategoryChange(newCategory) {
        let data = [];

        if (newCategory == "f35l") {
            await fetch(`/api/v1/op/getContestOfficial?name=f3l`)
                .then(handleErrors)
                .then(result => {
                    //console.log(result);
                    let cat = {
                        ...result,
                        name: "f3l",
                        rule_type: "fai",
                        qualif_round: 4,
                        qualif_group: 1,
                        qualif_slot: 6,
                        flyoff_slot: 6
            
                    };
                    data.push(cat);
                })
                .catch(function (error) {
                    displayErrorMsg(error);
                });


            await fetch(`/api/v1/op/getContestOfficial?name=f5l`)
                .then(handleErrors)
                .then(result => {
                    //console.log(result);
                    let cat = {
                        ...result,
                        name: "f5l",
                        rule_type: "fai",
                        qualif_round: 4,
                        qualif_group: 1,
                        qualif_slot: 6,
                        flyoff_slot: 6
            
                    };
                    data.push(cat);
                })
                .catch(function (error) {
                    displayErrorMsg(error);
                });
        }
        else {
            await fetch(`/api/v1/op/getContestOfficial?name=${newCategory}`)
                .then(handleErrors)
                .then(result => {
                    let cat = {
                        ...result,
                        name: newCategory,
                        rule_type: "fai",
                        qualif_round: 4,
                        qualif_group: 1,
                        qualif_slot: 6,
                        flyoff_slot: 6
            
                    };
                    data.push(cat);
                })
                .catch(function (error) {
                    displayErrorMsg(error);
                });
        }

        this.init({ categories: data });
        $("[data-toggle='tooltip']").tooltip(); // todo remove jQuery call
    }

    collect() {
        let data = [];
        let categories = document.querySelectorAll("table[fai_cat]");
        for (const category of categories) {
            let categoryName = category.getAttribute("fai_cat");

            let categoryData = {
                name: categoryName,
                rule_type: document.getElementById(`contest-rule-type-${categoryName}`).value,
                qualif_round: document.getElementById(`contest-qualif-round-${categoryName}`).value,
                qualif_group: document.getElementById(`contest-qualif-group-${categoryName}`).value,
                qualif_slot: document.getElementById(`contest-qualif-slot-${categoryName}`).value,
                flyoff_slot: document.getElementById(`contest-flyoff-slot-${categoryName}`).value
            };

            data.push(categoryData);
        }

        return data;
    }

    currentCategoryName_;  // f5j,f3l....
    #contestID_;
    #categories_ = [];
}