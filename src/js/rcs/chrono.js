/*
 * Copyright (c) 2019-2024 Thierry Wilmot
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

class RCSDigitalContestTimer extends RCSElementBase {

   constructor(params) {
      super(params);

      this.#contestID_ = params.contest_id;
      this.#busRef_ = params.bus;
      this.active_ = true;
      if (params.speech)
         this.#speechRef_ = params.speech;
      if (params.format_state)
         this.#formatState_ = params.format_state;
      if (params.msgOnChangeVoice)
         this.#msgOnChangeVoice_ = params.msgOnChangeVoice_;

      let myThis = this;
      this.#busRef_.state_change_cb = function (state) { myThis.onBusStateChanged(state); };
      this.#busRef_.callbackList_.push({ topic: RCSBus.RCS_WS_TOPIC_TIME_CHANGED, cb: function (e) { myThis.onTimeChanged(e); } });
      this.#busRef_.callbackList_.push({ topic: RCSBus.RCS_WS_TOPIC_ROUND_CHANGED, cb: function (e) { myThis.onContestParamsChanged(e); } });
      this.#busRef_.callbackList_.push({ topic: RCSBus.RCS_WS_TOPIC_DISPLAY_COLOR_CHANGED, cb: function (e) { myThis.onColorChanged(e); } });
      this.#busRef_.callbackList_.push({ topic: RCSBus.RCS_WS_TOPIC_SPEECH, cb: function (e) { myThis.onSpeech(e); } });
      this.#busRef_.callbackList_.push({ topic: RCSBus.RCS_WS_TOPIC_SOUND, cb: function (e) { myThis.onSound(e); } });

      if (params.defaultAudioMute == undefined)
         params.defaultAudioMute = false;

      let speakOnElt = document.getElementById("dct.label-speak-on")
      let speakOffElt = document.getElementById("dct.label-speak-off")

      if (getUserPrefsValue("dct.speak-on", !params.defaultAudioMute) == true) {
         if (speakOnElt)
            speakOnElt.classList.add("active");
         if (speakOffElt)
            speakOffElt.classList.remove("active");
         this.onAudioSwitchChanged(true);
      }
      else {
         if (speakOnElt)
            speakOnElt.classList.remove("active");
         if (speakOffElt)
            speakOffElt.classList.add("active");
         this.onAudioSwitchChanged(false);
      }

      if (this.#speechRef_) {
         this.populateVoiceList();
         if (params.speechDisable)
            this.#speechRef_.disable();
      }

      if (params.notifyEventCallback)
         this.#notifyEventCallback_ = params.notifyEventCallback;

      if (params.soundDisable)
         this.#soundEnable_ = false;
   }

   init(params) {
      this.parentRef_.classList.add("container")
      if (params.html)
         this.parentRef_.innerHTML = params.html;
      else {
         if (params.speech) {
            this.parentRef_.innerHTML =
               `<div class="dct-frame">
                  <div class="row">
                     <div class="col">
                        <span id="dct.refligth" class="dct-reflight">--</span>
                     </div>
                     <div class="col">
                        <span id="dct.state" class="dct-state">--</span>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col" style="text-align:center;">
                        <span id="dct.time" class="dct-time">--:--</span>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col">
                        <div><span>{{ lang en="Rnd" fr="Ma"}}&nbsp;</span><span id="dct.round" class="dct-round">--</span></div>
                     </div>
                     <div class="col">
                        <div style="float:right;"><span>Grp&nbsp;</span><span id="dct.group" class="dct-group">--</span></div>
                     </div>
                  </div>
                  <div class="dct-hr"></div>
                  <div class="row">
                     <div class="col">
                        <a class="btn btn-secondary" data-toggle="collapse" href="#collapseDCT" role="button" aria-expanded="false" aria-controls="collapseDCT"><i class="fas fa-language"></i></a>
                     </div>
                     <div class="col-3" style="color:#FFFFFF;font-size: 0.8rem;">
                        <div class="row flex-nowrap">                     
                           <i class="fa-solid fa-volume-high" data-toggle="tooltip" data-html="true" data-placement="top" title='{{ lang en="Volume" fr="Volume" de="Volumen" es="Volumen"}}'></i>&nbsp;
                           <span id="dct.speech-volume"></span>&nbsp;%
                        </div>
                        <div class="row flex-nowrap">
                           <i class="fa-solid fa-gauge-high" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ lang en='Elocution speed' fr='Vitesse d\'élocution' de='Sprechgeschwindigkeit' es='Velocidad de elocución'}}"></i>&nbsp;
                           <span id="dct.speech-rate"></span>&nbsp;%
                        </div>
                     </div>
                     <div class="col">
                        <div class="btn-group btn-group-toggle" data-toggle="buttons" style="float:right;">
                           <label class="btn btn-secondary active" id="dct.label-speak-on">
                              <input id="dct.speak-on" type="radio" name="options" id="option1" checked> <i class="fas fa-volume-up"></i>
                           </label>
                           <label class="btn btn-secondary" id="dct.label-speak-off">
                              <input id="dct.speak-off" type="radio" name="options" id="option2"> <i class="fas fa-volume-mute"></i>
                           </label>
                        </div>
                     </div>
                  </div>
                  <div class="row collapse dct-lang-collapse" id="collapseDCT">
                     <div class="card card-body bg-gradient-dark">
                        <table class="table-borderless">
                           <tbody>
                           <tr>
                              <td><img src="/json/countries/data/w40/de.png" style="width:25px;height:18px;margin-right:1rem;"></td>
                              <td><select id="dct.de" class="custom-select bg-gray-600 text-gray-900"></select></td>
                           </tr>
                           <tr>
                              <td><img src="/json/countries/data/w40/gb.png" style="width:25px;height:18px;margin-right:1rem;"></td>
                              <td><select id="dct.en" class="custom-select bg-gray-600 text-gray-900"></select></td>
                           </tr>
                           <tr>
                              <td><img src="/json/countries/data/w40/es.png" style="width:25px;height:18px;margin-right:1rem;"></td>
                              <td><select id="dct.es" class="custom-select bg-gray-600 text-gray-900"></select></td>
                           </tr>
                           <tr>
                              <td><img src="/json/countries/data/w40/fr.png" style="width:25px;height:18px;margin-right:1rem;"></td>
                              <td><select id="dct.fr" class="custom-select bg-gray-600 text-gray-900"></select></td>
                           </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>`;

            let mythis = this;
            document.getElementById("dct.speak-on").onclick = function () { mythis.onAudioSwitchChanged(true); };
            document.getElementById("dct.speak-off").onclick = function () { mythis.onAudioSwitchChanged(false); };
            document.getElementById("dct.en").onchange = function (e) { mythis.onLangSelectChange(e); };
            document.getElementById("dct.fr").onchange = function (e) { mythis.onLangSelectChange(e); };
            document.getElementById("dct.de").onchange = function (e) { mythis.onLangSelectChange(e); };
            document.getElementById("dct.es").onchange = function (e) { mythis.onLangSelectChange(e); };
            this.updateSpeechRate(params.speech.getRate());
            this.updateSpeechVolume(params.speech.getVolume());
         } else {
            this.parentRef_.innerHTML =
               `<div class="dct-frame">
                  <div class="row">
                     <div class="col">
                        <span id="dct.refligth" class="dct-reflight">--</span>
                     </div>
                     <div class="col">
                        <span id="dct.state" class="dct-state">--</span>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col" style="text-align:center;">
                        <span id="dct.time" class="dct-time">--:--</span>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col">
                        <div><span>{{ lang en="Rnd" fr="Ma"}}&nbsp;</span><span id="dct.round" class="dct-round">--</span></div>
                     </div>
                     <div class="col">
                        <div style="float:right;"><span>Grp&nbsp;</span><span id="dct.group" class="dct-group">--</span></div>
                     </div>
                  </div>
               </div>`;
         }
      }

      this.addElement("dct.refligth", "--");
      this.addElement("dct.state", "--");
      this.addElement("dct.time", "--:--");
      this.addElement("dct.round", "--");
      this.addElement("dct.group", "--");
   }

   async onAudioSwitchChanged(state) {
      setUserPrefsValue("dct.speak-on", state);

      // just allowed if admin
      await updateLocalServerInfo({ audio_volume: state })
         .then(function (response) {
         })
         .catch(function (error) {
            // dont notify error if granted as admin
         });
      
      if (state) {
         this.#audioEnable_ = true;
         if (this.#speechRef_)
            this.#speechRef_.setVolume(1);
      }
      else {
         this.#audioEnable_ = false;
         if (this.#speechRef_)
            this.#speechRef_.setVolume(0);
      }
   }

   onTimeChanged(timeInSec) {
      if (timeInSec >= 0)
         this.setValue("dct.time", this.timeFormat(timeInSec));
      else {
         this.setValue("dct.reflight", "--");
         this.setValue("dct.time", "--:--");
         this.setValue("dct.round", "--");
         this.setValue("dct.group", "--");
         this.setValue("dct.state", "--");
      }

      this.render();

      if (this.#notifyEventCallback_)
         this.#notifyEventCallback_(`<i class="fa-solid fa-clock"></i>&nbsp;${timeInSec}`);
   }

   onContestParamsChanged(data) {
      this.active_ = (this.#contestID_ == data.contest_id);
      //console.log(this.active_);
      //console.log(data);

      if (data.chrono_state == 0) { // stopped
         if (this.#speechRef_)
            this.#speechRef_.cancel();
      }
      else if (data.chrono_state == 3) { // pause
         if (this.#speechRef_)
            this.#speechRef_.togglePauseResume();
      }

      this.setValue("dct.round", this.formatDigit(data.round_id + 1));
      this.setValue("dct.group", this.formatDigit(data.group_id + 1));
      this.setValue("dct.state", this.#formatState_(data.state));

      this.render();
   }

   onColorChanged(data) {
      this.#currentBg_ = data.bg;
      this.setProperty("dct.time", "color", "#" + data.fg);
      this.setProperty("dct.time", "background-color", "#" + data.bg);
      let eltList = document.getElementsByClassName("dct-frame");
      if (eltList.length >= 1)
         eltList[0].style.setProperty("background-color", "#" + data.bg);
   }

   onBusStateChanged(state) {
      if (state == RCSBus.BUS_STATE_OPEN)
         this.parentRef_.style.setProperty("background-color", "#" + this.#currentBg_);
      else if (state == RCSBus.BUS_STATE_CLOSE)
         this.parentRef_.style.setProperty("background-color", "#e74a3b");
   }

   async onSpeech(data) {
      //console.log(data);
      if (this.#speechRef_ && this.#speechRef_.isEnabled()) {
         let dsr = document.getElementById("dct.speech-rate");
         if (dsr)
            dsr.innerHTML = this.#speechRef_.getRate() * 100;
         let dsv = document.getElementById("dct.speech-volume");
         if (dsv)
            dsv.innerHTML = this.#speechRef_.getVolume() * 100;

         let re = new RCSRegExp("\\((.*?)\\)\\{(.*?)\\}", "g");
         for (const message of data.m) {
            let result = message.matchAll(re);
            for (const phrase of result) {
               if (this.#speechRef_) {
                  await this.#speechRef_.say(phrase[1], phrase[2]);
                  if (this.#notifyEventCallback_)
                     this.#notifyEventCallback_(`<i class="fa-brands fa-speakap"></i> [${phrase[1]}] ${phrase[2]}`);
               }
            }
         }
      }
   }

   onSound(data) {
      if (this.#audioEnable_ && this.#soundEnable_) {
         let audio = new Audio("/" + data.m);
         audio.volume = this.#audioVolume_;
         audio.play();
         //console.log(data.m);
         if (this.#notifyEventCallback_)
            this.#notifyEventCallback_(`<i class="fa-solid fa-file-audio"></i> ${data.m}`);
      }
   }

   onLangSelectChange(event) {
      let elt = event.currentTarget.selectedOptions[0];
      let locale = elt.getAttribute("data-lang");
      let name = elt.getAttribute("data-name");

      this.#speechRef_.setVoicesForSay([{ locale: locale, name: name }]);

      let lang = getLocaleDetail(locale)[0];
      let testMsg = this.#msgOnChangeVoice_[lang];
      if (this.#speechRef_) {
         let ie = this.#speechRef_.isEnabled();
         this.#speechRef_.enable();
         this.#speechRef_.say(locale, testMsg);
         if (!ie)
            this.#speechRef_.disable();
      }
   }

   timerStateToString(value) {
      switch (value) {
         case 0: return "PT";
         case 1: return "WT";
         case 2: return "LT";
         case 3: return "ST";
         case 4: return "DT";
         default: return "--";
      }
   }

   formatDigit(v) {
      if (v < 10)
         return "0" + v;
      else
         return v;
   }

   timeFormat(time) {
      if ((time < 0) || (typeof time == "undefined"))
         return "-- : --";
      const sec = time % 60;
      const min = Math.floor(time / 60);
      return this.formatDigit(min) + ":" + this.formatDigit(sec);
   }

   async populateVoiceList() {
      let selectEn = document.getElementById("dct.en");
      let selectFr = document.getElementById("dct.fr");
      let selectDe = document.getElementById("dct.de");
      let selectEs = document.getElementById("dct.es");

      let enVoice = this.#speechRef_.getVoiceForSay("en");
      let frVoice = this.#speechRef_.getVoiceForSay("fr");
      let deVoice = this.#speechRef_.getVoiceForSay("de");
      let esVoice = this.#speechRef_.getVoiceForSay("es");

      let voices = await this.#speechRef_.getVoiceList();
      for (let i = 0; i < voices.length; i++) {
         const voice = voices[i];
         if (!voice.localService)
            continue;

         const lang = voice.lang;
         const name = voice.name;

         let option = document.createElement("option");
         option.textContent = `${name} (${lang})`;
         option.setAttribute("data-lang", lang);
         option.setAttribute("data-name", name);
         if (enVoice && enVoice.name === name)
            option.selected = true;
         else if (frVoice && frVoice.name === name)
            option.selected = true;
         else if (deVoice && deVoice.name === name)
            option.selected = true;
         else if (esVoice && esVoice.Name === name)
            option.selected = true;

         if (lang.startsWith("en"))
            selectEn.appendChild(option);
         else if (lang.startsWith("fr"))
            selectFr.appendChild(option);
         else if (lang.startsWith("de"))
            selectDe.appendChild(option);
         else if (lang.startsWith("es"))
            selectEs.appendChild(option);
      }
   }

   updateSpeechRate(value) {
      document.getElementById("dct.speech-rate").innerHTML = value * 100;
   }

   updateSpeechVolume(value) {
      document.getElementById("dct.speech-volume").innerHTML = value * 100;
   }

   setSpeechRate(value) {
      if (this.#speechRef_) {
         this.#speechRef_.setRate(value);
         this.updateSpeechRate(value);
      }
   }

   setSpeechVolume(value) {
      if (this.#speechRef_) {
         this.#speechRef_.setVolume(value);
         this.updateSpeechVolume(value);
      }
   }

   pauseSpeech() {
      if (this.#speechRef_)
         this.#speechRef_.pause();
   }

   resumeSpeech() {
      if (this.#speechRef_)
         this.#speechRef_.resume();
   }

   enableSound() {
      this.#soundEnable_ = true;
   }

   disableSound() {
      this.#soundEnable_ = false;
   }

   isSoundEnabled() {
      return this.#soundEnable_;
   }

   #contestID_;
   #currentBg_;
   #audioVolume_ = 1.0;
   #audioEnable_ = true;
   #soundEnable_ = true;
   #busRef_ = null;
   #speechRef_ = null;
   #notifyEventCallback_ = null;
   #formatState_ = function (state) { return state; };
   #msgOnChangeVoice_ = {
      en: "Speech try with this voice",
      fr: "Essai d'élocution avec cette voix",
      de: "Sprachversuche mit dieser Stimme",
      es: "Intenta hablar con esta voz"
   };
}