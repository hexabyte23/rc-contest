/*
 * Copyright (c) 2019-2024 Thierry Wilmot
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

class RCSQrCode extends RCSElementBase {
    constructor(params) {
        super(params);
    }

    init(params) {
        //this.parentRef_.classList.add("container")
        if (!params.qrcode_id)
            params.qrcode_id = "rcs-qrcode";

        if (params.html)
            this.parentRef_.innerHTML = params.html;
        else {
            let html = `<div class="d-flex justify-content-center" id="${params.qrcode_id}"></div>`
            if (params.display_url)
                html += `<br><div class="d-flex justify-content-center" id="${params.qrcode_id}-text"></div>`;

            this.parentRef_.innerHTML = html;
        }

        let port = ""
        if (window.location.port)
            port = ":" + window.location.port;
            
        let url = `${window.location.protocol}//${params.server_ip}${port}${params.url}`;

        this.qrcode_ = new QRCode(params.qrcode_id, {
            text: url,
            width: params.width,
            height: params.height,
            userSVG: true
        });

        if (params.display_url) {
            let elt = document.getElementById(`${params.qrcode_id}-text`);
            elt.innerHTML = url;
        }
    }

    // private
    #qrcode_ = 0;
}

