/*
 * Copyright (c) 2019-2024 Thierry Wilmot
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

class RCSBus {

    // Topics sent by the master server or by local servers
    static RCS_WS_TOPIC_ALL = "all";
    static RCS_WS_TOPIC_TIME_CHANGED = "srv.tim";
    static RCS_WS_TOPIC_ROUND_CHANGED = "srv.inf";
    static RCS_WS_TOPIC_PILOT_RESULT_CHANGED = "srv.pir";
    static RCS_WS_TOPIC_DISPLAY_COLOR_CHANGED = "srv.dco";
    static RCS_WS_TOPIC_RESULT_CONFIRM = "srv.rco";
    static RCS_WS_TOPIC_RESULT_CHANGED = "srv.rch";
    static RCS_WS_TOPIC_PHONE_IS_ALLOWED = "srv.pia";
    static RCS_WS_TOPIC_FREE_POS_CHANGED = "srv.fpc";
    static RCS_WS_TOPIC_CONTEST_DIR_IS_ALLOWED = "srv.cia";
    static RCS_WS_TOPIC_UPGRADE_LOCAL_SERVER_CHANGED = "srv.ugd";
    static RCS_WS_TOPIC_SPEECH = "srv.spe";
    static RCS_WS_TOPIC_SOUND = "srv.sou";
    static RCS_WS_TOPIC_DISPLAY_FEEDBACK = "dis.feb";

    static BUS_STATE_OPEN = "open";
    static BUS_STATE_CLOSE = "close";

    constructor(params) {
        this.state_change_cb_ = params.state_change_cb;
        this.error_cb_ = params.error_cb;
        this.callbackList_ = (params.callbackList) ? params.callbackList : [];

        if (this.#websocket_ && this.websocket_.readyState == 1)
            return;

        this.url_ = `${(params.isSSL ? "wss" : "ws")}://${window.location.hostname}:${params.port}`
        if (params.urlPrepend)
            this.url_ += params.urlPrepend;
    }

    async init() {
        try {
            this.#websocket_ = new WebSocket(this.url_);
            this.#websocket_.onopen = this.onOpen.bind(this);
            this.#websocket_.onclose = this.onClose.bind(this);
            this.#websocket_.onmessage = this.onMessage.bind(this);
            this.#websocket_.onerror = this.onError.bind(this);
        } catch (error) {
            displayErrorMsg(error);
        }
    }

    onOpen(event) {
        if (this.state_change_cb_)
            this.state_change_cb_(RCSBus.BUS_STATE_OPEN);
    }

    onClose(event) {
        if (this.state_change_cb_)
            this.state_change_cb_(RCSBus.BUS_STATE_CLOSE);
    }

    deltaArray(input, ref, output) {
        if (input.length != ref.length) {
            output = input;
            return;
        }

        for (let i = 0; i < input.length; i++) {
            let inp = input[i];
            let re = ref[i];
            let out;
            deltaObject(inp, re, out);

            output.push(out);
        }
    }

    mergeArray(input, ref, output) {
        if (input.length != ref.length)
            throw new Error(`Web socket error: Size mismatch (${input.length}/${ref.length}) when merging array`);

        for (let i = 0; i < ref.length; i++) {
            const iv = input[i];
            const rv = ref[i];

            if (iv == undefined) {
                output.push(rv);
                continue;
            }
            else if (rv == undefined) {
                output.push(iv);
                continue;
            }

            if (typeof iv == "object") {
                if (Array.isArray(iv)) {
                    let ov = [];
                    this.mergeArray(iv, rv, ov);
                    output.push(ov);
                } else {
                    let ov = {};
                    this.mergeObject(iv, rv, ov);
                    output.push(ov);
                }
            } else {
                if (iv == undefined) {
                    output.push(rv);
                } else {
                    output.push(iv);
                }
            }
        }
    }

    addKeys(i, r) {
        const ret = new Set();

        if (i) {
            for (let key of Object.keys(i))
                ret.add(key);
        }

        if (r) {
            for (let key of Object.keys(r))
                ret.add(key);
        }

        return ret;
    }

    mergeObject(input, ref, output) {
        for (let key of this.addKeys(input, ref)) {
            const iv = input[key];
            const rv = ref[key];
            if (typeof iv == "object") {
                if (Array.isArray(iv)) {
                    let ov = [];
                    this.mergeArray(iv, rv, ov);
                    output[key] = ov;
                } else {
                    let ov = {};
                    this.mergeObject(iv, rv, ov);
                    output[key] = ov;
                }
            } else {
                if (iv != undefined) {
                    output[key] = iv;
                } else {
                    output[key] = ref[key];
                }
            }
        }
    }

    onMessage(event) {
        try {
            if (event.data.length != 0) {
                const message = JSON.parse(event.data);
                for (const callback of this.callbackList_) {
                    if (callback.topic === message.s) {
                        if (message.i != undefined)
                            callback.cb(message.i);
                        else {
                            let mergedData = {};
                            if (callback.lastData == undefined)
                                callback.lastData = [];
                            let lastData = callback.lastData[message.k];
                            if (lastData)
                                this.mergeObject(message.d, lastData, mergedData);
                            else
                                mergedData = message.d;
                            callback.lastData[message.k] = mergedData;
                            callback.cb(mergedData);
                        }
                    }
                    else if (callback.topic === RCSBus.RCS_WS_TOPIC_ALL) {
                        if (message.i != undefined)
                            callback.cb(message.s, message.i);
                        else {
                            let mergedData = {};
                            if (callback.lastData == undefined)
                                callback.lastData = [];
                            let lastData = callback.lastData[callback.topic];
                            if (lastData)
                                this.mergeObject(message.d, lastData, mergedData);
                            else
                                mergedData = message.d;
                            callback.lastData[callback.topic] = mergedData;
                            callback.cb(mergedData);
                        }
                    }
                }
            }
        } catch (error) {
            console.error(error);
            displayErrorMsg(error);
        }
    }

    onError(event) {
        if (this.error_cb_)
            this.error_cb_(event);
        else
            displayErrorMsg(event);
    }

    // private
    #websocket_;
    url_;
    callbackList_ = [];
    state_change_cb_;
    error_cb_;
}

