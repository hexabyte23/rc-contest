/*
 * Copyright (c) 2019-2024 Thierry Wilmot
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

class RCSSpeech {
   constructor(params) {
      if (params) {
         this.#volume_ = (params.volume ? params.volume : 1.0);
         this.#rate_ = (params.rate ? params.rate : 1.0);
      }
   }

   async init() {
      this.getNavigatorLanguage_ = (navigator.languages && navigator.languages.length) ? navigator.languages[0] : navigator.userLanguage || navigator.language || navigator.browserLanguage || 'en';

      this.voices_ = await this.getVoiceList();
      let prefs = getUserPrefsValue("dct-default-voices", [])
      if (prefs.length != 0) {
         for (const pref of prefs) {
            for (const voice of this.voices_) {
               if ((voice.name === pref.name) && (voice.lang.startsWith(pref.lang))) {
                  this.#currentVoice_.set(pref.lang, voice);
               }
            }
         }
         if (this.#currentVoice_.size === 0) {
            for (const pref of prefs) {
               for (const voice of this.voices_) {
                  if (voice.lang.startsWith(pref.lang)) {
                     this.#currentVoice_.set(pref.lang, voice);
                  }
               }
            }
         }
      } else {
         for (const lang of ["de", "en", "es", "fr"]) {
            for (const voice of this.voices_) {
               if (voice.lang.startsWith(lang)) {
                  this.#currentVoice_.set(lang, voice);
                  break;
               }
            }
         }
      }
   }

   async getVoiceList() {
      if (this.voices_.length == 0) {
         const allVoicesObtained = new Promise(function (resolve, reject) {
            let voices = window.speechSynthesis.getVoices();
            if (voices.length !== 0)
               resolve(voices);
            else {
               window.speechSynthesis.addEventListener("voiceschanged", function () {
                  voices = window.speechSynthesis.getVoices();
                  resolve(voices);
               });
            }
         });

         let voices = await allVoicesObtained.then(function (voices) { return voices; });
         this.voices_ = [];
         for (const voice of voices) {
            // remove all voices that need internet connection (localService == false)
            if (voice.localService == true)
               this.voices_.push(voice);
         }
      }

      return this.voices_;
   }

   setVoicesForSay(langNameList) {
      for (const item of langNameList) {
         const locale = item.locale;
         const name = item.name;
         for (const voice of this.voices_) {
            if ((voice.lang === locale) && (voice.name === name)) {
               let lang = getLocaleDetail(locale)[0];
               this.#currentVoice_.set(lang, voice);
               break;
            }
         }
      }

      let prefs = [];
      this.#currentVoice_.forEach(function (voice, key) {
         prefs.push({ lang: key, name: voice.name });
      });

      setUserPrefsValue("dct-default-voices", prefs)
   }

   getVoiceForSay(lang) {
      return this.#currentVoice_.get(lang);
   }

   getAllVoiceForSay(lang) {
      for (const voice of this.voices_) {
         if (voice.lang.startsWith(lang)) {
            return voice;
         }
      }

      return null;
   }

   setRate(value) {
      if (value == undefined) {
         console.error("value is undefined");
         return;
      }
      //console.log("rate " + value);
      this.#rate_ = value;
      if (this.#utter_)
         this.#utter_.rate = value;
   }

   getRate() {
      return this.#rate_;
   }

   setVolume(value) {
      if (value == undefined) {
         console.error("value is undefined");
         return;
      }
      //console.log("vol " + value);
      this.#volume_ = value;
      if (this.#utter_)
         this.#utter_.volume = value;

      if (value > 0.0) {
         let isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
         if (isMobile && this.#volume_ > 0.0) {
            displayModalMsg({
               type: "info",
               title: '{{ lang en="Security policy" fr="Politique de sécurité" de="Sicherheitsrichtlinie" es="Política de seguridad" }}',
               message: '{{ lang en="Annonces will be sent to this device" fr="Des annonces audio seront envoyées à cet appareil" de="Audioansagen werden an dieses Gerät gesendet" es="Se enviarán anuncios de audio a este dispositivo" }}',
               ok_callback: function () {
                  let audio = new Audio("/audio/hbeep.wav");
                  audio.volume = 0;
                  audio.play();
                  const speech = new SpeechSynthesisUtterance("hi");
                  speech.volume = 0;
                  speechSynthesis.speak(speech);
               },
               showCancel: false
            });
         }
      }
   }

   getVolume() {
      return this.#volume_;
   }

   say(locale, text) {
      if (locale == "audio") {
         let audio = new Audio("/" + text);
         //audio.volume = this.#audioVolume_;
         audio.play();
         return new Promise(resolve => {
            audio.onended = resolve;
         });
      }
      if (this.#enable_ === false)
         return;
      let detail = getLocaleDetail(locale);
      if (!text || text == "" || detail.length === 0) {
         //console.error(`Empty string ${locale} ${text}`);
         return;
      }
      let lang = detail[0];
      let voice = this.#currentVoice_.get(lang);
      if (voice == null)
         voice = this.getAllVoiceForSay(lang);
      this.#utter_ = new SpeechSynthesisUtterance(text.toLowerCase());  // lowercase could remove some spelling bad effect ?
      this.#utter_.onerror = function (event) { console.error(event.error); };
      this.#utter_.volume = this.#volume_;
      this.#utter_.rate = this.#rate_;
      this.#utter_.voice = voice;
      //console.log(`${locale} ${text} ${this.#volume_} ${this.#rate_}`)
      window.speechSynthesis.speak(this.#utter_);
      return new Promise(resolve => {
         this.#utter_.onend = resolve;
      });
   }

   cancel() {
      window.speechSynthesis.cancel();
   }

   enable() {
      this.#enable_ = true;
   }

   disable() {
      this.#enable_ = false;
   }

   pause() {
      window.speechSynthesis.pause();
   }

   resume() {
      window.speechSynthesis.resume();
   }

   togglePauseResume() {
      togglePauseResume = !togglePauseResume;
      if (this.#togglePauseResume)
         this.pause();
      else
         this.resume();
   }

   isEnabled() {
      return this.#enable_;
   }

   getNavigatorLanguage_;
   voices_ = [];
   #currentVoice_ = new Map();
   #utter_;
   #volume_ = 1.0;
   #rate_ = 1.0;
   #enable_ = true;
   #togglePauseResume = false;
}
