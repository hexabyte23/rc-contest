#!/bin/zsh

############## prepare #################

export DOC_SOURCE=../src/docs
export DOC_TARGET=doc

rm -r ./$DOC_TARGET

mkdir $DOC_TARGET

############# make package ###############
cp    $DOC_SOURCE/main.html ./$DOC_TARGET
cp -R $DOC_SOURCE/image ./$DOC_TARGET
rm ./$DOC_TARGET/image/*.drawio
rm ./$DOC_TARGET/image/.*.bkp
rm -R ./$DOC_TARGET/image/rpi/Colour/Screen/EPS
rm -R ./$DOC_TARGET/image/rpi/Colour/Print/EPS
rm -R ./$DOC_TARGET/image/rpi/Mono


version=$(date "+%Y.%m.%d")

# for manual install
rm "doc.$version.zip"
zip -r --symlink -9 "doc.$version.zip" ./$DOC_TARGET

############## clean #################

rm -r ./$DOC_TARGET

echo "build done"
