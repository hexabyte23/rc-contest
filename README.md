# RC-Contest UI

This open-source project is JavaScript official front end of HTTP RCS server back end.

We already considered, as Mid-term objective, a full rewrite of UI with modern front end (React/VueJS/...), but we need volunteers to help that way.

## Getting started

### Installation

3 steps are mandatory:

- Download and install latest version of corresponding binary package for your platform (macOS, Windows or Raspberry PI) from https://rc-contest.com

- Download and install this project at your favorite dev location `<your dir location>`.

- Modify start.sh (macOS), start.bat(win10) or rcs.service (rpi) and replace cmdline `rcs -p 443 -w 8082 -c true` by `rcs -p 443 -w 8082 -c true -e <your dir location>`

By adding `-e` or `--dev-doc` option, RCS will switch into dev mode. All file changes in `<your dir location>` will raise an update process and make available the updated resource (html, json...) for all new incoming HTTP requests.

Root doc path by default is './site/'. You can add `-r` or `--root-doc` option to specify your own location. 

### Preprocessing directives

Before writing your first html page, RCS is able to preprocess some special directives. 

This ability is working in all text resources (html, json, xml...) of dev doc directory. General format of a directive is: `{{ command params }}`. Space between `{{` and `command` is mandatory, space between `params`and `}}` is optional.

- `{{ include "your file path" }}` as suggested, it insert content of "your file path" at this location
- `{{ lang en="my english text" fr="my french text" de="my german text" es="my spanish text" }}` insert the corresponding text at this location

## Support
On discord, server : RC-Contest

## Documentation
see doc [here](src/docs/main.html)

## License
GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 (see LICENSE file)
